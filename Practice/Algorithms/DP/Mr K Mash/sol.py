#!/bin/python3
import sys
fstd="t0"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')

class Marshes(object):
    def __init__(self,r,c):
        # Field
        self.X=[]
        #[[0]*N for _ in range(M) ]
        self.Rows=r
        self.Cols=c
        # free from left side
        self.FL=[]
        # free from top
        self.FT=[]
        #current result
        self.res=0

    def addRow(self,x):
        X =self.X
        R = self.Rows
        C = self.Cols
        FL=self.FL
        FT=self.FT
        idx=len(X)
        #add to input
        X.append( list(map(lambda v: (0,1)[v=='x'],x)) )
        #count from left
        FL.append([0]*C)
        FL[idx][0]=(0,-1)[X[idx][0]==1]
        for i in range(1,C):
            FL[idx][i]=(FL[idx][i-1]+1,-1)[X[idx][i]==1]
        #count from top
        FT.append([0]*C)
        if idx==0:
            for i in range(C):
                FT[idx][i]=(0,-1)[X[idx][i]==1]
        else:
            for i in range(C):
                FT[idx][i]=(FT[idx-1][i]+1,-1)[X[idx][i]==1]
        #check for current res
        res=self.res
        if idx>0:
            r=idx
            for c in range(C-1,0,-1):
                if c+r<=res: #we can't exceed current res - go away
                    break
                t=FL[r][c]+FT[r][c]
                if t<2 or t<=res: #we have masrh or can't exceed current res for this cell - go to next 
                    continue
                for dc in range(FL[r][c],0,-1):
                    if dc+FT[r][c]<=res:  #we can't exceed current res using this left side - go away
                        break
                    dr=min(FT[r][c] , FT[r][c-dc]) #get maximum heigh for chosen left and right sides
                    if dr+dc<=res: #and go away if can't exceed current res
                        continue
                    for ddr in range(dr,0,-1):
                        if FL[r-ddr][c]>=dc and ddr+dc>res:
                            res=ddr+dc
                            break #we found value and it is maximal for this dc - break
        self.res=res
        return res

if __name__ == "__main__":
    rows, cols = map(int, input().strip().split(' '))
    M=Marshes(rows,cols)
    res=0
    for _ in range(rows):
        res=M.addRow(input().strip())    
    print(("impossible",res*2)[res>0])