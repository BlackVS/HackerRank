import sys
fstd="t11"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')

import sys
input = sys.stdin.readline

CBASE=ord('a')
MOD=1000000007

def solve():
    #S=input()
    #
    FRQx  =[0]*26
    #counts all xy masks
    FRQxy =[[0]*26 for _ in range(26)]
    #counts XYY masks 
    FRQxyy=[0]*26

    res=0
    for c in input():
        v=ord(c)-CBASE
        res+=FRQxyy[v]
        if res>=MOD: res%=MOD
        for i in range(26):
            FRQxyy[i]+=FRQxy[i][v]
            FRQxy[i][v]+=FRQx[i]
        FRQx[v]+=1
    return res

print(solve())