import sys
MOD=1000000007
S=tuple(map(int,input().strip()))
N=len(S)
m=1
res=0
for i in range(N-1,-1,-1):
    res+=S[i]*m*(i+1)
    if res>=MOD: res%=MOD
    m=m*10+1
    if m>MOD: m%=MOD
print(res)