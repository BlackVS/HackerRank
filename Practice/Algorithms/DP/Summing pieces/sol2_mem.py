import sys
N=int(input())
MOD=1000000007
N2M=[1]*(N+1)
for i in range(N):
    N2M[i+1]=N2M[i]<<1
    if N2M[i+1]>=MOD:
        N2M[i+1]%=MOD
#
K=[0]*N
i0=0
i1=N-1
#t=pow(2,N,MOD)-1
t=N2M[N]-1
while i0<=i1:
    K[i0]=K[i1]=t
    #t+=pow(2,N-2-i0,MOD)-(1<<i0)
    t+=N2M[N-2-i0]-(1<<i0)
    i0+=1
    i1-=1
#
#print(K)
S=0
for i,x in enumerate(map(int,input().split())):
    S=(S+x*K[i])%MOD
print(S)