import sys
#https://github.com/andreimaximov/algorithms/blob/master/algorithms/dynamic-programming/summing-pieces/solution.py

fstd="t2"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')



MOD=1000000007

def mulmod(a, b, m=MOD):
    r=a*b
    if r>=m: r%=m
    return r

def addmod(a, b, m=MOD):
    r=a+b
    if r>=m: r%=m
    return r

N =int(input())


dp = 0,0,0,0
for i,a in enumerate(map(int,input().split())):
    if i==0:
        dp = a, 1, 1, aZZ
        print(dp)
        continue

    px, py, pk, pm = dp

    # Case 1 - Append a piece (A[i])
    exc_x = addmod(px, mulmod(a, py))

    # Case 2 - The essence here is that existing suffix pieces increase
    # in length by one which affects by how much A[i] is factored in and
    # that the value of each piece increases by m(i).
    inc_x = addmod(px,
                addmod(pm,  # Prefix piece increases in value my m(i + 1)
                    mulmod(a, addmod(pk, py))))  # Factor in A[i]
    x = addmod(exc_x, inc_x)

    # Double combinations
    y = mulmod(py, 2)

    # k(i) = k(i + 1) + y(i + 1) * 2 = k(i + 1) + y(i)
                           k = addmod(pk, y)

    # m(i) = A[i] * y(i) + m(i + 1)
    m = addmod(mulmod(a, y), pm)

    dp = x, y, k, m
    print(dp)

print(dp[0])


