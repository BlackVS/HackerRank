// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include<iostream>
#include<string.h>
#include <vector>
#include <map>
using namespace std;

typedef long long ll;
std::map<int, std::vector<int>> edges;
std::vector< std::vector<ll> > DP;
int N, K;


std::vector<int> V;

//counts subtrees with k=1..K edges to rest for each vertex and store as DP[ vertex ] [ k ] 
void DFS(int cur, int root=-1)
{
	//already visited
	if (V[cur])
		return;

	for (auto v: edges[cur])
	{
		if (v == root)
			continue;
		//dive to bottom
		DFS(v, cur);
		//starting from bottom
		//
		for (int k = K; k > 0; k--)
		{
			for (int x = k; x > 0; x--)
				// v - already visted/calulated i.e. DP[v][] has actual data
				// k-x = 0..k-1 i.e. DP[cur][k-x] already calculated
				//   x = k..1, i.e. DP[v] also calculated
				// res: we have DP[cur][k - x] at current node and DP[v][x] in children
				// combine them to get k-x+x=k result subtrees
				DP[cur][k] += DP[cur][k - x] * DP[v][x];
			//also just take all children k-1 and +1 to get k for parent
			DP[cur][k] += DP[cur][k - 1];
		}
	}
	return;
}


int main()
{
	ios_base::sync_with_stdio(0);
	cin >> N >> K;

	V.resize(N+1, false);
	DP.resize(N + 1, std::vector<ll>(K + 1));

	int a, b;
	for (int i = 1; i < N; ++i)
	{
		cin >> a >> b;
		edges[a].push_back(b);
		edges[b].push_back(a);
	}


	//for K==0 at each vertex we have only one tree with 0 edges to rest - vertex itself
	//for leaves of tree we have DP[leaf][0]=1 and DP[leaf][ !=0 ]=0 due to leaf hasn';t subroots i.e no edges. Zeroed already above in resize
	for (int i = 1; i <= N; ++i)
		DP[i][0] = 1;

	//do job
	DFS(1);

	ll ans = 0;
	for (int i = 1; i <= N; ++i)
		for (int j = 0; j < K; ++j)
			ans += DP[i][j];
	ans += DP[1][K]+1;
	cout << ans << endl;
}
