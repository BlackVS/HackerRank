#!/bin/python3
import sys, copy
from collections import *

input = sys.stdin.readline
#sys.stdin = open('t0.in', 'r')
#sys.stdout= open('t0.res', 'w+')

# vertices have nums 0..N-1
class Graph(object):
    def __init__(self,n):
        self.VN=n
        self.G = [[] for _ in range(n)]
        self.VDegree = [0]*n
        # DFS
        self.DFS_NR  = None
        self.DFS_PR  = None
        #SubGraphs
        self.SB_P=None
        self.SB_R=None
        self.SB_L=None
        self.SB_N=None

    def __str__(self,shortform=True):
        if shortform:
            return str(self.G)
        res = "vertices: " + str(self.VN)
        res += "\nedges:\n"
        for i,x in enumerate(self.G):
            for y in x:
                res += str(i) + " -> " + str(y) + "\n"
        return res

    def merge(self,v1,v2):
        N=self.VN
        #init
        if not self.SB_P:
            self.SB_P=[None]*N
            self.SB_R=list(range(N))
            self.SB_L=list(range(N))
        P=self.SB_P
        R=self.SB_R
        L=self.SB_L
        r1 = R[v1]
        r2 = R[v2]
        if r1 == r2:
            return
        r1, r2=min(r1,r2),max(r1,r2)
        P[r2]=L[r1]
        L[r1]=L[r2]
        v=L[r1]
        while R[v]==r2:
            R[v]=r1
            v=P[v]

    def add_edge(self,x,y,directed=False,fmerge=True):
        self.G[x].append(y)
        self.VDegree[x]+=1
        if not directed:
            self.G[y].append(x)
            self.VDegree[y]+=1
        if fmerge:
            self.merge(x,y)

          
s2i=lambda i:int(i)-1

Q = int(input().strip())

for _ in range(Q):
    N, M, Cl, Cr = map(int, input().strip().split())
    G = Graph(N)
    res=0
    if Cl<=Cr or not M:
        res=N*Cl
    for _ in range(M):
        u,v=map(s2i, input().strip().split())
        if res: continue
        G.add_edge(u,v)
    if res:
        print(res)
    else:    
        cnt=Counter(G.SB_R)
        for c in cnt.values():
            res+=Cl+(c-1)*Cr
        print(res)