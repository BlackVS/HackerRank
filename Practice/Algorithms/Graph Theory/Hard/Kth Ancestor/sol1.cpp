#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#define SEARCH_STEPS  8
#define MAX(a,b) ((a)>(b)?(a):(b))
#define scanf_s scanf

typedef struct { 
	int knext; //next branch. -1 if not found yet
	int dnext; //distance to next branch. -1 if not found yet
	int dmax;  //distance to the root. -1 if not calculated yet
	int dksize;
	int dk[SEARCH_STEPS];// next neigbours at 1, 2, 4, 8 .... positions. -1 if not found yet
} ELINFO;

ELINFO edges[100001];
int	   temp_path[100001];

int main() {
	int T, P, Q;
	//fill predefined steps
	int    STEPS[SEARCH_STEPS];
	for (int i = 0, d = 1; i<SEARCH_STEPS; i++, d <<= 1) 
		STEPS[i] = d;
    //
    scanf_s("%d\n", &T);
	for (int t = 0; t<T; t++)
	{
		scanf_s("%d\n", &P);
		memset(edges, -1, sizeof(edges));
		edges[0].dmax  = 0;
		edges[0].knext = 0;
		edges[0].dnext = 0;
		int p1, p2;
		for (int p = 0; p<P; p++)
		{
			scanf_s("%d %d\n", &p1, &p2);
			edges[p1].dk[0] = p2;
			if (p2 == 0)
			{
				edges[p1].dmax  = 0;
				edges[p1].dnext = 0;
				edges[p1].knext = 0;
			}
		}
		scanf_s("%d\n", &Q);
		for (int q = 0; q<Q; q++)
		{
			int q0 = 0, q1 = 0, q2 = 0;
			scanf_s("%d", &q0);
			switch (q0)
			{
				case 0:
					scanf_s("%d %d\n", &q1, &q2);
					memset(&edges[q2], -1, sizeof(edges[q2]));
					edges[q2].dk[0] = q1;
					break;
				case 1:
					scanf_s("%d\n", &q1);
					memset(&edges[q1], -1, sizeof(edges[q1]));
					break;
				case 2:
					scanf_s("%d %d\n", &q1, &q2);
					//check&fill
					int tres = -1;
					if (edges[q1].dmax<0)
					{
						int d = 0, p=q1;
						while(p>0 && edges[p].dmax<0)
						{
							if (d == q2) 
								tres = p;
							temp_path[d] = p;
							p = edges[p].dk[0];
							d++;
						}
						if (p == -1)
						{//not a node of the tree
							printf("0\n");
							break;
						}
						//
						for (int id = 0; id<d; id++)
						{
							int el = temp_path[id];
							edges[el].dmax = d - id + edges[p].dmax;
							edges[el].dnext = d - id;
							edges[el].knext = p;
							for (int i = 1, base = 2; base + id< d && i < SEARCH_STEPS; i++, base <<= 1)
							{
								edges[el].dk[i] = temp_path[base+id];
								edges[el].dksize = i + 1;
							}

						}
					}
					int res = q1;
					if(tres>=0)
					{ 
						res = tres;
						q2 = 0;
					}
					else if (q2 > edges[res].dmax)
						res = q2 =0;
					else if (q2 >= edges[res].dnext)
					{
						q2 -= edges[res].dnext;
						res = edges[res].knext;
					}
					while (q2 && res)
					{
						if (q2 > 1 && edges[res].dksize>1)
						{
							int n = 1;
							while (n < edges[res].dksize && q2 >= STEPS[n])
								n++;
							q2 -= STEPS[n-1];
							res = edges[res].dk[n-1];
						}
						else
						{
							res = edges[res].dk[0];
							q2--;
						}
					}
					printf("%d\n", MAX(0, res));
				}
		}
	}
	return 0;
}