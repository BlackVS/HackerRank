#!/bin/python3
import sys
from random import *

fstd="t0"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

modInv=lambda x,m:pow(x, m-2, m)

def imod(a, n):
 c = 1
 while (c % a > 0):
     c += n
 return c // a


def solve0(A,M,K):
    AN=len(A)
    res=0
    for i in range(AN):
        S=1
        for j in range(i,AN):
            S=(S*A[j])%M
            res+=S==K
            #if S==K:
            #    print(i,j)
    return res

def solveK0(A,M):
    L0=None
    res=0
    for i,a in enumerate(A):
        if a==0: 
            res+=i+1
            L0=i
        elif L0!=None:
            res+=(L0+1)

    return res

def solve1(A,M,K):
    if(K==0):
        return solveK0(A,M)

    N=len(A)
    res=0
    F=[0]*M
    ACC=1
    Kinv=modInv(K,M)
    
    C1S=[0]*N
    for i,a in enumerate(A):
        ACC=(ACC*a)%M
        if i==0:
            C1S[i]=(0,1)[a==1]
        else:
            C1S[i]= (C1S[i-1]+1) if a==1 else 0

        #print(ACC,end=' ')
        #due to K>0 we must reinit F,ACC etc
        if ACC==0:
            ACC=1
            F=[0]*M
            continue
        #add if single value ==K
        res+=ACC==K
        #get subarrs
        rinv=(ACC*Kinv)%M
        if F[rinv]:
            res+=F[rinv]
        F[ACC]+=1
    #print("  ".join(map(str,C1S)))
    return res


def solve(A,M,K):
    if(K==0):
        return solveK0(A,M)

    N=len(A)
    res=0
    F=defaultdict(int)
    
    ACC=1
    Kinv=modInv(K,M)
    
    for i,a in enumerate(A):
        ACC=(ACC*a)%M
        #due to K>0 we must reinit F,ACC etc
        if ACC==0:
            ACC=1
            F=defaultdict(int)
            continue
        #add if single value ==K
        res+=ACC==K
        #get subarrs
        rinv=(ACC*Kinv)%M
        if F[rinv]:
            res+=F[rinv]
        F[ACC]+=1
    return res

if __name__ == "__main__":
    #Q = int(input().strip())
    #for _ in range(Q):
    #    N, M, K=map(int,input().strip().split())
    #    A=tuple(map(lambda s: int(s)%M, input().strip().split()))
    #    print(solve0(A,M,K))
    #    print(solve1(A,M,K))
    
    #print("inv mod ",M)
    #for i in range(M):
    #    print(modInv(i,M))

    N=30
    M=17
    K=randint(0,M-1)
    A=[randint(0,100)%M for _ in range(N)]
    print(N,M,K)
    print(" ".join(map(str,A)))
    print(solve0(A,M,K))
    print(solve1(A,M,K))
