#!/bin/python3
import sys

fstd="t1"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)


def solve(A,M,K):
    AN=len(A)
    res=0
    for i in range(AN):
        S=1
        for j in range(i,AN):
            S=(S*A[j])%M
            res+=S==K
    return res


if __name__ == "__main__":
    Q = int(input().strip())
    for _ in range(Q):
        N, M, K=map(int,input().strip().split())
        A=tuple(map(int,input().strip().split()))
        print(solve(A,M,K))