#include <bits/stdc++.h>
using namespace std;
const int MAXN = 100005;
using pi = pair<int, int>;
using lint = long long;

int main(){
	int n;
	string str;
	cin >> n >> str;
	int cnt = 0;
	int mincnt = 0;
	int maxcnt = 0;
	for(auto &i : str){
		if(i == '(') cnt++;
		else cnt--;
		mincnt = min(cnt, mincnt);
		maxcnt = max(cnt, maxcnt);
	}
	if(cnt == 0){
		if(mincnt == 0) cout << 0 << endl;
		else cout << 2 << endl;
	}
	else{
		if(cnt > 0){
			if(mincnt < 0) cout << 2 << endl;
			else cout << 1 << endl;
		}
		else{
			if(mincnt < cnt) cout << 2 << endl;
			else cout << 1 << endl;
		}
	}
}