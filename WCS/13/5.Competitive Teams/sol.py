#!/bin/python3
import sys,bisect
from collections import *
from random import *

fstd="t3"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline


class Teams(object):
    def __init__(self,N):
        self.NP=N
        #groups size, first each equal 1
        self.G_SZ    =[1]*self.NP
        self.G_SZcnt =Counter(self.G_SZ)
        #SubGraphs
        self.SB_P=None # prev group memebr
        self.SB_R=None # root, group id
        self.SB_L=None # last in group

    def join(self,v1,v2):
        NP=self.NP
        if not self.SB_P:
            self.SB_P=[None]*NP
            self.SB_R=list(range(NP))
            self.SB_L=list(range(NP))
        P=self.SB_P #prev
        R=self.SB_R #root
        L=self.SB_L #end
        G_SZ=self.G_SZ
        G_SR=self.G_SZcnt

        # get root of each
        r1 = R[v1]
        r2 = R[v2]
        if r1 == r2:
            #already in one subgraph
            return None

        # join
        r1, r2=min(r1,r2),max(r1,r2)
        P[r2]=L[r1] # link 2nd group to the end of first
        L[r1]=L[r2] # update L for first group
        L[r2]=None #just clean, not needed
        #update roots/group id for 2nd group
        v=L[r1]
        while R[v]==r2:
            R[v]=r1
            v=P[v]
        # count final groups
        # self.NG-=1
        #
        G_SR[G_SZ[r1]]-=1
        G_SR[G_SZ[r2]]-=1
        #
        G_SZ[r1]+=G_SZ[r2]
        G_SZ[r2]=0 #need zero elemnts here
        #
        G_SR[G_SZ[r1]]+=1
        #check statistics, k - mumber of members,
        s=sum( k*v for k,v in G_SR.items())
        assert(s==NP)
        return True

    def query0(self,c):
        A=self.G_SZ
        NG=len(A)
        res=0
        for i in range(NG):
            if A[i]==0: continue
            for j in range(i+1, NG):
                if A[j]>0 and abs(A[j] - A[i])>=c:
                    res+=1
        return res

    def query1(self,c):
        # c==0
        if c==0: #just count all non zero teams
            cnt=sum(map(lambda x:x>0,self.G_SZ))
            return cnt*(cnt-1)//2

        A=sorted(filter(lambda x:x>0,self.G_SZ))
        N=len(A)
        res = 0
        j = 0
        for i in range(N):
            while j < N and A[j] < A[i]+c: #skip lowest
                j+=1
            #first proper element, rest good too
            res += N-j
        return res
    
    def query2(self,c):
        # c==0
        if c==0: #just count all non zero teams
            cnt=sum(v for k,v in self.G_SZcnt.items() if k>0)
            return cnt*(cnt-1)//2
        if c>=self.NP:
            return 0
        A=sorted( (k,v) for (k,v) in self.G_SZcnt.items() if v>0)
        N=len(A)

        RA=[0]*N
        RA[-1]=A[-1][1]
        for i in range(N-2,-1,-1):
            RA[i]=RA[i+1]+A[i][1]
        
        res = 0
        j = 0
        for i in range(N):
            while j < N and A[j][0] < A[i][0]+c: #skip lowest
                j+=1
            #first proper element, rest good too
            if j<N:
                res += A[i][1]*RA[j]
        return res

if __name__ == "__main__":
    #N, Q = map(int,input().strip().split())
    #T=Teams(N)
    #for _ in range(Q):
    #    q=tuple(map(int,input().strip().split()))
    #    if q[0]==1:
    #        T.join(q[1]-1,q[2]-1)
    #    if q[0]==2:
    #        print(T.query0(q[1]), T.query1(q[1]),T.query2(q[1]),)
    N=20
    T=Teams(N)
    Q=100
    print(N,Q)
    for _ in range(Q):
        q=randint(1,10)
        if q>2:
            p1=randint(0,N-1)
            p2=randint(0,N-1)
            #print(1,p1+1,p2+1)
            T.join(p1,p2)
        else:
            q=randint(0,N+2)
            print(2,q)
            print("> ",T.query0(q), T.query1(q),T.query2(q))
            