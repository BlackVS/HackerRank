// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <cstdio>
#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <utility>
using namespace std;


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

typedef vector<int>  VI;
typedef vector<bool> VB;
typedef vector<VI>   VII;
typedef vector<VB>   VVB;

//maxN =200000 -> ln2=18
#define LN 18

class CRoads {
	VII adj;//N
	VII parent;//[LN][N]
	VI  depth;// [N];
	VVB	removed;
	int m_N;
public:
	CRoads(int N) :
		m_N(N),
		parent(LN,VI(N)),
		adj(N),
		depth(N),
		removed(N,VB(N))
	{
	}

	void add(int u, int v) 
	{
		adj[u].push_back(v);
		adj[v].push_back(u);
	}

	void init() {
		dfs();
		for (int j = 1; j < LN; j++) {
			for (int i = 0; i < m_N; i++) {
				if (parent[j - 1][i] != -1) {
					parent[j][i] = parent[j - 1][parent[j - 1][i]];
				}
			}
		}
	}

	void dfs(int u = 0, int prev = -1, int d = 0) {
		depth[u] = d;
		parent[0][u] = prev;

		for (int v : adj[u]) {
			if (v == prev) continue;
			dfs(v, u, d + 1);
		}
	}

	int lca(int u, int v) {
		if (depth[u] < depth[v]) swap(u, v);

		int diff = depth[u] - depth[v];
		for (int i = 0; i < LN; i++) {
			if ((diff >> i) & 1) {
				u = parent[i][u];
			}
		}

		if (u == v) return u;

		for (int i = LN - 1; i >= 0; i--) {
			if (parent[i][u] != parent[i][v]) {
				u = parent[i][u];
				v = parent[i][v];
			}
		}

		return parent[0][u];
	}

	void remove(int u, int v) 
	{
		if (u > v) swap(u, v);
		removed[u][v] = true;
	}

	void clear(int u, int v)
	{
		if (u > v) swap(u, v);
		removed[u][v] = false;
	}

	bool check(int u, int v)
	{
		if (u > v) swap(u, v);
		return removed[u][v];
	}

	int query(int u, int v)
	{
		int p = lca(u,v);//LCA

		int res = (depth[u] - depth[p]) + (depth[v] - depth[p]);
		//now check for failed roads
		
		int pp = u;
		while (pp != p) {
			//get parent
			int pr = parent[0][pp];
			//check if failed
			if (check(pp,pr)) {
				return -1;
			}
			pp = pr;
		}
		pp = v;
		while (pp != p) {
			//get parent
			int pr = parent[0][pp];
			//check if failed
			if (check(pp, pr)) {
				return -1;
			}
			pp = pr;
		}
		return res;
	}

};


int main() {
	int N,u,v,Q;
	boost;

	cin >> N;
	CRoads R(N);
	//read tree, N-1
	for (int i = 0; i < N - 1; i++) {
		cin >> u >> v;
		R.add(u-1,v-1);
	}
	R.init();
	cin >> Q;
	for (int q = 0; q < Q; q++) {
		char c;
		int res;
		cin >> c >> u >> v;
		u--;
		v--;
		switch (c) {
			case 'd':
				R.remove(u, v);
				break;
			case 'c':
				R.clear(u, v);
				break;
			case 'q':
				res = R.query(u, v);
				if (res < 0)
					cout << "Impossible" << endl;
				else
					cout << res << endl;
				break;
		}
	}

}