#!/bin/python3
import os
import sys

fstd="t1"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

# Complete the findTheAbsentStudents function below.
def solve(N,A):
    all = set(range(1,N+1))
    res=all-set(A)
    return " ".join(map(str,sorted(res)))

if __name__ == '__main__':
    #fptr = open(os.environ['OUTPUT_PATH'], 'w')

    N = int(input())
    A = list(map(int, input().rstrip().split()))

    res = solve(N,A)

    #fptr.write(' '.join(map(str, result)))
    #fptr.write('\n')

    #fptr.close()
    print(res)
