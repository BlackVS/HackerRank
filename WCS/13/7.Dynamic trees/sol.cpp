// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <cstdio>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>
#include <utility>
using namespace std;


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

typedef vector<int>  VI;
typedef vector<bool> VB;
typedef vector<VI>   VII;
typedef vector<VB>   VVB;

//maxN =200000 -> ln2=18
#define LN 18

class CGraph {
	VI  m_parent;
	VI  m_colors;
	int m_N;
public:
	CGraph(int N) :
		m_N(N),
		m_parent(N),
		m_colors(N)
	{
		m_parent[0] = -1;
	}

	void setColor(int idx, int clr) {
		m_colors[idx] = clr;
	}

	void toggleColor(int idx) {
		m_colors[idx] ^= 1;
	}

	void setParent(int idx, int p) {
		m_parent[idx] = p;
	}

	int query(int u, int v, int k)
	{

		VI u2r;
		VI uc2r;

		int clr = 0;
		int p = u;
		while (p) {
			u2r.push_back(p);
			clr += m_colors[p] == 1;
			uc2r.push_back(clr);
			p = m_parent[p];
		}
		u2r.push_back(0);
		clr += m_colors[0] == 1;
		uc2r.push_back(clr);

		VI v2r;
		VI vc2r;
		clr = 0;
		p = v;
		while (p) {
			v2r.push_back(p);
			clr += m_colors[p] == 1;
			vc2r.push_back(clr);
			p = m_parent[p];
		}
		v2r.push_back(0);
		clr += m_colors[0] == 1;
		vc2r.push_back(clr);

		int iu = u2r.size() - 1;
		int iv = v2r.size() - 1;
		while (u2r[iu] == v2r[iv] && iv>0 && iu>0) {
			iu--;
			iv--;
		}
		if (u2r[iu] != v2r[iv]) {
			iu += 1; //points to lca
			iv += 1; //points to lca
		}

		// [u..lca]
		if(k <= uc2r[iu]){
			for (int i = 0; i <= iu; i++) {
				if (m_colors[u2r[i]] == 1)//black
					k--;
				if (k == 0)
					return u2r[i];
			}
		}
		else
			k -= uc2r[iu];
		// (lca..v]
		for (int i = iv - 1; i >= 0; i--) {
			if (m_colors[v2r[i]] == 1)//black
				k--;
			if (k == 0)
				return v2r[i];
		}
		return -2;
	}
};


int main() {
	int N, Q;
	boost;

	cin >> N >> Q;
	CGraph G(N);

	//read tree, N-1
	int clr;
	for (int i = 0; i < N; i++) {
		cin >> clr;
		G.setColor(i, clr);
	}

	int p;
	for (int i = 0; i < N - 1; i++) {
		cin >> p;
		G.setParent(i + 1, p - 1);
	}

	for (int q = 0; q < Q; q++) {
		char c;
		int x, u, v, k;
		int res;
		//string s;
		cin >> c;
		switch (c) {
		case 'T':
			//togle color
			cin >> x;
			x--;
			G.toggleColor(x);
			break;
		case 'C':
			cin >> u >> v;
			u--;
			v--;
			G.setParent(u, v);
			break;
		case 'K':
			cin >> u >> v >> k;
			u--;
			v--;
			res = G.query(u, v, k);
			cout << res + 1 << "\n";
			break;
		}
	}

}