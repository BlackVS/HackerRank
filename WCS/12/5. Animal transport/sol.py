#!/bin/python3
import sys
from collections import *
fstd="t0"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')

ANTYPES = {"E":1, "C":1, "D":-1, "M":-1}

class Graph(object):
    def __init__(self, n):
        self.VN = n
        self.GW = [Counter() for _ in range(n)]
        self.GB = [Counter() for _ in range(n)]

    def add_edge(self,src,dst,w):
        #self.G[x].append(y)
        if w>0:
            self.GW[dst][src]+=1
        else:
            self.GB[dst][src]+=1

    def DP(self):
        N =self.VN
        GW = self.GW
        GB = self.GB

        DP =[0]*N
        WDP=[0]*N
        BDP=[0]*N

        for i in range(1,N):
            #if not GW[i] and not GB[i]:
            #    continue 
            
            #all Whites
            s=0
            for v,c in GW[i].items():
                s+=WDP[v]+c
            WDP[i]=s+WDP[i-1]

            #all Blacks
            s=0
            for v,c in GB[i].items():
                s+=BDP[v]+c
            BDP[i]=s+BDP[i-1]
           
            #flipping

        return 

s2i=lambda i:int(i)-1


if __name__ == "__main__":
    Q = int(input().strip())
    for _ in range(Q):
        M, N = map(int, input().strip().split())
        # M zoos, N animals
        G=Graph(M)
        T = list(map(lambda t:ANTYPES[t], input().strip().split(' ')))
        S = list(map(s2i, input().strip().split(' ')))
        D = list(map(s2i, input().strip().split(' ')))
        for i in range(N):
            G.add_edge(S[i],D[i],T[i])
        G.DP()
        print(T,S,D)