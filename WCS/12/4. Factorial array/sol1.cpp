// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <cstdio>
#include <iostream>
#include <map>
#include <vector>
#include <utility>
using namespace std;


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

typedef unsigned long long ull;
typedef long long ll;
typedef vector<int> VI;
typedef vector<ll> VLL;

#define MOD 1000000000
// Returns value of n! % p
/*ll modFact(int n, int p)
{
	if (n >= p)
		return 0;

	ll result = 1;
	for (ll i = 1; i <= n; i++)
		result = (result * i) % p;

	return result;
}*/

template<ull mod>
struct fast_fact {
	ull m(ull a, ull b) const {
		ull r = (a*b) % mod;
		return r;
	}
	template<class...Ts>
	ull m(ull a, ull b, Ts...ts) const {
		return m(m(a, b), ts...);
	}
	// calculates x!!, ie 1*3*5*7*...
	ull double_fact(ull x) const {
		ull ret = 1;
		for (ull i = 3; i < x; i += 2) {
			ret = m(i, ret);
		}
		return ret;
	}
	// calculate 2^2^n for n=0...bits in ull
	// a pointer to this is stored statically to make calculating
	// 2^k faster:
	ull const* get_pows() const {
		static ull retval[sizeof(ull) * 8] = { 2 % mod };
		for (int i = 1; i < sizeof(ull) * 8; ++i) {
			retval[i] = m(retval[i - 1], retval[i - 1]);
		}
		return retval;
	}
	// calculate 2^x.  We decompose x into bits
	// and multiply together the 2^2^i for each bit i
	// that is set in x:
	ull pow_2(ull x) const {
		static ull const* pows = get_pows();
		ull retval = 1;
		for (int i = 0; x; ++i, (x = x / 2)) {
			if (x & 1) retval = m(retval, pows[i]);
		}
		return retval;
	}
	// the actual calculation:
	ull operator()(ull x) const {
		x = x%mod;
		if (x == 0) return 1;
		ull result = 1;
		// odd case:
		if (x & 1) result = m((*this)(x - 1), x);
		else result = m(double_fact(x), pow_2(x / 2), (*this)(x / 2));
		return result;
	}
};
template<ull mod>
ull factorial_mod(ull x) {
	return fast_fact<mod>()(x);
}

ll getSum(ll BITree[], int index)
{
	ll sum = 0; 
	index = index + 1;

	while (index>0)
	{
		sum += BITree[index];
		if (sum >= MOD) sum %= MOD;
		index -= index & (-index);
	}
	return sum;
}

void updateBIT(ll BITree[], int n, int index, ll val)
{
	// index in BITree[] is 1 more than the index in arr[]
	index = index + 1;
	while (index <= n)
	{
		BITree[index] += val;
		index += index & (-index);
	}
}

ll *constructBITree(VLL& arr)
{
	int n = (int)arr.size();
	// Create and initialize BITree[] as 0
	ll *BITree = new ll[n + 1];
	for (int i = 1; i <= n; i++)
		BITree[i] = 0;


	// Store the actual values in BITree[] using update()
	for (int i = 0; i<n; i++)
		updateBIT(BITree, n, i, arr[i]);

	// Uncomment below lines to see contents of BITree[]
	//for (int i=1; i<=n; i++)
	//      cout << BITree[i] << " ";

	return BITree;
}

int main()
{
	int n, m;
	int x;
	cin >> n >> m;
	VLL A(n);
	VLL F(n);
	for (int i = 0; i < n; i++) {
		cin >> x;
		A[i] = x;
		//F[i] = modFact(x,MOD);
		F[i] = factorial_mod<MOD>(x);
	}
	ll *BITree = constructBITree(A);

	
	for (int a0 = 0; a0 < m; a0++) {
		int c, a, b;
		cin >> c >> a >> b;
		switch (c) {
			case 1:
				for (int i = a-1; i <= b-1; i++) {
					int delta = (F[i] * A[i]) % MOD;
					updateBIT(BITree, n, i, delta);
					A[i] = A[i] + 1;
					F[i] = (F[i] + delta) % MOD;
				}
				break;
			case 2:
				a--;
				b--;
				if(a==0)
					cout << getSum(BITree, b) << endl;
				else 
					cout << getSum(BITree, b) - getSum(BITree, a - 1) << endl;
				break;
			case 3:
				int i = a - 1;
				ll fact = factorial_mod<MOD>(b);//modFact(b,MOD);
				ll delta = fact - F[i];
				updateBIT(BITree, n, i, delta);
				A[i] = b;
				F[i] = fact;
				break;
		}
		// Write Your Code Here
	}
	return 0;
}

