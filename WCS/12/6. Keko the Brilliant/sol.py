#!/bin/python3
import sys
from bisect import *
fstd="t0"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')

# vertices have nums 0..N-1
class Graph(object):
    def __init__(self, n, r):
        self.VN=n
        self.G = [[] for _ in range(n)]
        self.W = r
        # DFS
        self.DFS_NR  = None
        self.DFS_PR  = None

    def add_edge(self,x,y):
        self.G[x].append(y)
        self.G[y].append(x)

    def DFS(self,root=0):
        N =self.VN
        W =self.W
        NR  = self.NR  = [-1]*N
        P   = self.PR  = [None]*N
        FL  = [False]*N

        GU=self.G #no need to save G
        idx=0
        v=root
        NR[v]=idx
        idx+=1
        while True:
            while GU[v]:
                w=GU[v].pop()
                if NR[w]==-1:
                    P[w]=v
                    NR[w]=idx
                    idx+=1
                    v=w
            #we back to v
            #update parent
            if v!=root:
                p=P[v]
                if W[p]>W[v]:
                    W[p]=W[v]
                    FL[p]=True
            if v==root and not GU[v]:
                break;
            v=P[v]
            idx+=1
        return sum(FL)



          
s2i=lambda i:int(i)-1

if __name__ == "__main__":
    N = int(input())
    R = list(map(int, input().strip().split()))
    G=Graph(N,R)
    for _ in range(N-1):
        u,v = map(s2i, input().split())
        G.add_edge(u,v)
        #print(res)
    res=G.DFS(0)
    print(res)