#!/bin/python3
import sys
from collections import *
#fstd="t2"
#sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')


def primes(n):
    ps, sieve = [], [True] * (n+1)
    for p in range(2, n):
        if sieve[p]:
            ps.append(p)
            for i in range(p*p, n, p):
                sieve[i] = False
    return ps

PRIMES=tuple(primes(500000))

def factorize(n):
    res = defaultdict(lambda: 0)
    for p in PRIMES:
        while n % p == 0:
            n = n // p
            res[p] += 1
        if n == 1:
            return dict(res)

MAXN=10000

def prepare(F,FS):
    for i in range(2,MAXN):
        for f in range(2, MAXN//i):
            ni=f*i
            nv=1+f*F[i]
            if nv>F[ni]:
                F[ni] =nv
                FS[ni]="{} / {}".format(ni,f)
    F[1]=1
    
def gets1(x, F):
    if x<len(F):
        return F[x]
    return x+1


def facts(n):
    d    = 2
    step = 1
    res  = n
    while d*d<=n:
        while n%d == 0:
            n = n // d
            if n>1:
                res+=n
        d+=step
        step=2
    return res+1
    
if __name__ == "__main__":
    #N = int(input().strip())
    F = [i+1 for i in range(MAXN+1)]
    FS= [str(i) for i in range(MAXN+1)]
    prepare(F,FS)
    #s = 0
    #for x in map(int, input().strip().split(' ')):
    #    s+=gets(x,F)
    #print(s)
    T=[1024]
    for t in range(100,125):
        print(t, gets1(t,F),facts(t))
