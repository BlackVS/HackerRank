// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <cstdio>
#include <iostream>
#include <map>
#include <vector>
#include <utility>
#include <algorithm>
#include <cassert>
using namespace std;


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

typedef vector<int> VI;
typedef unsigned long long ull;
#define MOD 1000000007l

void transform1(int* A, int N, int AN) {
	int idx = N;
	int v, prev;
	for (int i = 0; i < N - 1; i++, idx++) {
		v = MAX(A[i], A[i + 1]);
		assert(idx < AN);
		A[idx] = v;
	}

	for (int d = 2; d < N; d++) {
		for (int j = 0; j < N - d; j++, idx++) {
			prev = idx - (N - d + 1);
			v = MAX(A[prev], A[j + d]);
			assert(idx < AN);
			A[idx] = v;
		}
	}
}

int getX(int* A, int N, int i) 
{
	int res = 0;

	return res;
}

ull transform(int* A, int N, int maxV) {
	ull res = 0;
	int XN = (N*(N + 1)) >> 1;

	bool f = false;
	for (int ws = 0; ws < XN; ws++) {
		if (f) {
			int l = XN - ws;
			ull cnt = (l*(l + 1)) / 2;
			res = (res + cnt*maxV) % MOD;
			break;
		}
		f = true;
		for (int a = 0; a < XN - ws; a++) {
			int mx = 0;
			for (int i = a; i < a + ws + 1; i++) {
				assert(i < XN);
				mx = max(mx, getX(A,N,i));
			}
			f = f && (mx == maxV);
			res = (res + mx) % MOD;
		}
	}
	return res;
}

int main() {
	int N;
	cin >> N;

	int* A = (int*)malloc(N * sizeof(A[0]));

	ull res = 0;
	int maxV = 0;
	for (int i = 0; i < N; i++) {
		cin >> A[i];
		res += A[i];
		maxV = MAX(maxV, A[i]);
		if (res >= MOD) res %= MOD;
	}

	res = transform(A, N, maxV);

	cout << res;
	return 0;
}