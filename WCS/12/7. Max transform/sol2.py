#!/bin/python3
import sys
from bisect import *
from random import *
fstd="t2"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')

MOD = 1000000007

class RMQ(object):
    def __init__(self, a):
        self._a=a
        self._n=n=len(a)
        self._logtable=_logtable=[0]*(n+1)
        for i in range(2,n+1):
            _logtable[i]=_logtable[i>>1]+1
        
        self._rmq = rmq = [ [0]*n for _ in range(_logtable[n] + 1)]

        for i in range(n):
            rmq[0][i] = i

        k=1
        while (1 << k) < n:
            i=0
            while i + (1 << k) <= n:
                x = rmq[k - 1][i];
                y = rmq[k - 1][i + (1 << k - 1)];
                rmq[k][i] = x if a[x] >= a[y] else y
                i+=1
            k+=1

    def query(self, i, j):
        k = self._logtable[j - i]
        x = self._rmq[k][i]
        y = self._rmq[k][j - (1 << k) + 1]
        return x if self._a[x] >= self._a[y] else y

def transform1(x):
    rmq=RMQ(x)
    res=[]
    for ws in range(0,len(x)):
        for a in range(0,len(x)-ws):
            v=max(x[a:a+ws+1])
            res.append(v)
    return res

def transform0(x):
    N=len(x)
    idx=N
    maxV=0
    for i in range(N-1):
        v=max(x[i],x[i+1])
        x.append( v )
        maxV=max(maxV,v)
        idx+=1

    for d in range(2,N):
        for j in range(0,N-d):
            prev = idx - (N - d + 1)
            v = max(x[prev], x[j + d])
            x.append(v)
            idx+=1
    return v


def transform2(x,maxV):
    res=0
    fEqual=False
    for ws in range(0,len(x)):
        if fEqual:
            #res=(res+(len(x)-ws)*maxV)%MOD
            #continue
            l=len(x)-ws
            cnt= (l*(l+1))//2
            res=(res+cnt*maxV)%MOD
            break
        fEqual=True
        for a in range(0,len(x)-ws):
            mx=max(x[a:a+ws+1])
            fEqual=fEqual and (mx==maxV)
            res=(res+mx)%MOD
    return res

def solve(A):
    maxV=transform1(A)
    return transform2(A,maxV)

if __name__ == "__main__":
    N = int(input().strip())
    A = list(map(int, input().strip().split()))
    result = solve(A)
    print(result)
