#!/bin/python3
import sys
from bisect import *
fstd="t0"
#sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')

def transform1(x):
    res=[]
    for ws in range(0,len(x)):
        for a in range(0,len(x)-ws):
            res.append(set(x[a:a+ws+1]))
    return res

def transform2(x):
    res=[]
    for ws in range(0,len(x)):
        for a in range(0,len(x)-ws):
            s=set()
            for ss in x[a:a+ws+1]:
                s|=ss
            res.append(s)
    return res

if __name__ == "__main__":
    X=list(range(10))
    Y=transform1(X)
    Z=transform2(Y)
    for i,z in enumerate(Z):
        print(i,z)
