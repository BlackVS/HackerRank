// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <cstdio>
#include <iostream>
#include <map>
#include <vector>
#include <utility>
#include <algorithm>
#include <cassert>
using namespace std;


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

typedef vector<int> VI;
typedef unsigned long long ull;
#define MOD 1000000007l

ull transform1(int* A, int N, int AN) {
	int idx = N;
	ull res = 0;
	int v, prev;
	for (int i = 0; i < N - 1; i++, idx++) {
		v = MAX(A[i], A[i + 1]);
		res += v;
		if (res >= MOD) res %= MOD;
		A[idx] = v;
	}

	for (int d = 2; d < N; d++) {
		for (int j = 0; j < N - d; j++, idx++) {
			prev = idx - (N - d + 1);
			v = MAX(A[prev], A[j + d]);
			A[idx] = v;
			res += v;
			if (res >= MOD) res %= MOD;
		}
	}
	return res;
}

ull transform2(int* X, int N, int XN, int maxV) {
	ull res = 0;
	int v;
	int idx = N;
	for (int i = 0; i < N - 1; i++, idx++) {
		v = MAX(X[i], X[i + 1]);
		res += v;
		if (res >= MOD) res %= MOD;
		X[idx] = v;
	}

	bool fAllMax = false;
	for (int d = 2; d < N; d++) {
		int idx = N;
		if (fAllMax) {
			int nr = N - d;
			//ull s = ((N - d)*(N - d + 1)) / 2;
			//res = (res + (s%MOD)*maxV) % MOD;
			//break;
			res = (res + ((N - d)*maxV)%MOD ) % MOD;
			continue;
		}
		fAllMax = true;
		for (int j = 0; j < N - d; j++, idx++) {
			v = MAX(X[idx], X[j + d]);
			X[idx] = v;
			res += v;
			if (res >= MOD) res %= MOD;
			fAllMax = fAllMax && (v == maxV);
		}
	}

	return res;
}

int main() {
	ull N, AN;
	cin >> N;
	AN = N*(N + 1);

	int* A = (int*)malloc(AN * sizeof(A[0]));

	ull res = 0;
	int maxV = 0;
	for (int i = 0; i < N; i++) {
		cin >> A[i];
		res += A[i];
		maxV = MAX(maxV, A[i]);
		if (res >= MOD) res %= MOD;
	}
	res += transform1(A, N, AN);
	if (res >= MOD) res %= MOD;

	res += transform2(A, AN >> 1, AN, maxV);
	if (res >= MOD) res %= MOD;

	cout << res;
	return 0;
}