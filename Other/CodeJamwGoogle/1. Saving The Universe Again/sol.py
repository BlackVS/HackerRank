#!/bin/python3
import sys

fstd="t1"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

maxC = 32

if __name__ == "__main__":
    T = int(input().strip())
    for t in range(T):
        sD, P = input().split()
        D=int(sD)
        C = [0]*maxC
        idx=0
        S = 0
        w = 1
        for p in P:
            if p=='C':
                idx+=1
                w<<=1
            if p=='S':
                C[idx]+=1
                S+=w
        #print(S,C)
        res=0
        while S>D and idx>0:
            if C[idx]>0:
                C[idx]-=1
                C[idx-1]+=1
                S-=(w>>1)
                res+=1
                continue
            idx-=1
            w>>=1
        print("Case #{}: {}".format(t+1,res if S<=D else "IMPOSSIBLE" ))
