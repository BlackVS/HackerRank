#!/bin/python3
import sys

#fstd="t1"
#sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

from timeit import default_timer as timer
def timefunc(func, *args, **kwargs):
    """Time a function. 
    args:
        iterations=1
    Usage example:
        timeit(myfunc, 1, b=2)
    """
    try:
        iterations = kwargs.pop('iterations')
    except KeyError:
        iterations = 1
    elapsed = sys.maxsize
    start = timer()
    for _ in range(iterations):
        result = func(*args, **kwargs)
    elapsed = (timer() - start)/iterations

    print(('{}() : {:.9f}'.format(func.__name__, elapsed)))
    return result
#res=timefunc( searchKMP, S, P, iterations=rep)
#if(res!=res0): print("Wrong")

def troubleSort0(V):
    f=True
    while f:
      f=False
      for i in range(0,len(V)-2):
        if V[i] > V[i+2]:
          f = True
          V[i],V[i+2]=V[i+2],V[i]
    for i in range(0,len(V)-1):
        if V[i]>V[i+1]:
            return i
    return None

if __name__ == "__main__":
    T = int(input().strip())
    print(T)
    for t in range(T):
        N=int(input())
        V=list(map(int,input().split()))
        res=troubleSort0(V)
        print("Case #{}: {}".format(t+1,"OK" if res==None else res))
