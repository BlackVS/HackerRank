#!/bin/python3
import sys
from random import *

#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

from timeit import default_timer as timer
def timefunc(func, *args, **kwargs):
    """Time a function. 
    args:
        iterations=1
    Usage example:
        timeit(myfunc, 1, b=2)
    """
    try:
        iterations = kwargs.pop('iterations')
    except KeyError:
        iterations = 1
    elapsed = sys.maxsize
    start = timer()
    for _ in range(iterations):
        result = func(*args, **kwargs)
    elapsed = (timer() - start)/iterations

    print(('{}() : {:.9f}'.format(func.__name__, elapsed)))
    return result
#res=timefunc( searchKMP, S, P, iterations=rep)
#if(res!=res0): print("Wrong")

def troubleSort0(V):
    f=True
    while f:
      f=False
      for i in range(0,len(V)-2):
        if V[i] > V[i+2]:
          f = True
          V[i],V[i+2]=V[i+2],V[i]
    for i in range(0,len(V)-1):
        if V[i]>V[i+1]:
            return i
    return None

def troubleSort1(V):
    f=True
    icnt=0
    while f:
      f=False
      for i in range(len(V)-1,1+icnt,-1):
        if V[i-2] > V[i]:
          f = True
          V[i-2],V[i]=V[i],V[i-2]
      if icnt>0 and V[icnt-1]>V[icnt]:
          return icnt-1
      icnt+=1
    for i in range(icnt-1,len(V)-1):
        if V[i]>V[i+1]:
            return i
    return None

#[5, 2, 0, 4, 4, 8, 3, 4, 8, 4]
#6 [0, 2, 3, 4, 4, 4, 5, 4, 8, 8]
#None [0, 2, 3, 4, 4, 4, 5, 4, 8, 8]

#[5, 4, 1, 4, 1, 7, 8, 1, 9, 5]
#4 [1, 1, 1, 4, 5, 4, 8, 5, 9, 7]
#None [1, 1, 1, 4, 5, 4, 8, 5, 9, 7]


if __name__ == "__main__":
    #V=[5, 4, 1, 4, 1, 7, 8, 1, 9, 5]
    #res=troubleSort1(V)
    #print(res,V)
    
    
    T = 1000
    N = 100
    Vmax = 10
    for t in range(T):
        V=[ randint(0,Vmax-1) for _ in range(N)]
        #print(" >: ", V0)

        V0=V.copy()
        res0=troubleSort0(V0)
        #print(res0,V0)

        V1=V.copy()
        res1=troubleSort1(V1)
        #print(res1,V1)

        if res1!=res0:
            print(">>>>: ")
            print(V)
            print(res0, V0)
            print(res1, V1)

