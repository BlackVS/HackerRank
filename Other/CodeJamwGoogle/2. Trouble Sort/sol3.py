#!/bin/python3
import sys

fstd="t1"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

from timeit import default_timer as timer
def timefunc(func, *args, **kwargs):
    """Time a function. 
    args:
        iterations=1
    Usage example:
        timeit(myfunc, 1, b=2)
    """
    try:
        iterations = kwargs.pop('iterations')
    except KeyError:
        iterations = 1
    elapsed = sys.maxsize
    start = timer()
    for _ in range(iterations):
        result = func(*args, **kwargs)
    elapsed = (timer() - start)/iterations

    print(('{}() : {:.9f}'.format(func.__name__, elapsed)))
    return result
#res=timefunc( searchKMP, S, P, iterations=rep)
#if(res!=res0): print("Wrong")

def troubleSort1(V):
    f=True
    icnt=0
    while f:
      f=False
      for i in range(len(V)-1,1+icnt,-1):
        if V[i-2] > V[i]:
          f = True
          V[i-2],V[i]=V[i],V[i-2]
      if icnt>0 and V[icnt-1]>V[icnt]:
          return icnt-1
      icnt+=1
    for i in range(icnt-1,len(V)-1):
        if V[i]>V[i+1]:
            return i
    return None

if __name__ == "__main__":
    T = int(input().strip())
    for t in range(T):
        N=int(input())
        V=list(map(int,input().split()))
        res=troubleSort1(V)
        print("Case #{}: {}".format(t+1,"OK" if res==None else res))
