#!/bin/python3

import sys

def add_word(word, w_ind, trie, l):
    
    lw = len(word)
    curr_ind = 0
    i = 0
    while i < lw:
        find = 0
        for child in trie[curr_ind][1]:
            if trie[child][0] == word[i]:
                curr_ind = child
                find = 1
                break
        if find == 0:
            break
        i += 1
    
    while i < lw:    
        l += 1
        trie[curr_ind][1].append(l)
        trie.append([word[i], [], -1, 1])
        curr_ind = l
        i += 1
    trie[curr_ind][2] = w_ind
    return l

l = 0
#letter, children by ind, word index in the original list, is included
trie = [['', [], -1, 1]]
n = int(input().strip())
s = input().strip().split(' ')
s = sorted(s)
#print(*s)
for ind in range(n):
    l = add_word(s[ind], ind, trie, l)
#print(*trie)
lst = [1 for _ in range(n)]
for ind in range(l - 1, 0, -1):
    elem = trie[ind]
    if elem[2] != -1:
        for child in elem[1]:
            if trie[child][3] == 1:
                elem[3] = 0
                lst[elem[2]] = 0
                break
    else:
        elem[3] = 0
        for child in elem[1]:
            if trie[child][3] == 1:
                elem[3] = 1
                break
sm = 0
for i in range(n):
    if lst[i] == 1:
        #print(s[i])
        for l in s[i]:
            sm += ord(l)
print(sm)