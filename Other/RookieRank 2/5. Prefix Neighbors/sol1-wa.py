#!/bin/python3

import sys
from itertools import*

n = int(input().strip())
X = sorted(input().strip().split())
#print(X)
# your code goes here
PW = [(False,0)]*n
Total = 0
for i,s in enumerate(X):
    t=sum(map(ord,s))
    Total+=t
    if i<n-1:
        PW[i]=(X[i+1].startswith(X[i]),t)
#print(PW)

resmin=0
for k,g in groupby(PW,key=lambda x:x[0]):
    if not k: continue
    f=[False,False]
    sg=list(map(lambda y:y[1],g))
    mm=sum(sg[-1::-2])
    resmin+=mm
print(Total-resmin)
