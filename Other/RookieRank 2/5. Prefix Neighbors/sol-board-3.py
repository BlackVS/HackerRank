def f(s):
    return sum(list(map(ord, list(s))))


if __name__ == '__main__':
    n = int(input().strip())
    A = input().strip().split(' ')

    S = set(A)
    Z = S.copy()
    A.sort(key = f, reverse = True)

    for s in A:
        if s not in Z:
            continue
        s = s[:-1]
        while s != '' and s not in S:
            s = s[:-1]
        if s != '' and s in Z:
            Z.remove(s)

    print(sum(f(x) for x in Z))
