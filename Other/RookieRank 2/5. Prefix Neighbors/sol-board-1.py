class Node:
    def __init__(self,string,parent=None):
        self.data = string
        self.parent = parent
        self.children = set()
        self.subtree_max_benefit = -1
        
def insert_node(string,roots,parent=None):
    for r in roots:
        if string.startswith(r.data):
            insert_node(string,r.children,r)
            return
    roots.add(Node(string,parent))
    return

def benefit(string):
    return sum(ord(x) for x in string)        

def max_benefit(tree):
    if tree.subtree_max_benefit!=-1:
        return tree.subtree_max_benefit
    if not tree.children:
        tree.subtree_max_benefit = benefit(tree.data)
        return tree.subtree_max_benefit
    benefit_without_root = sum(max_benefit(child) for child in tree.children)
    benefit_with_root = benefit(tree.data)
    for child in tree.children:
        benefit_with_root += sum(max_benefit(grandchild) for grandchild in child.children)
    tree.subtree_max_benefit = max(benefit_with_root,benefit_without_root)
    return tree.subtree_max_benefit
    
n = int(input().strip())
s = input().strip().split(' ')
s.sort(key=lambda x: (len(x),x))
roots = set()
for x in s:
    insert_node(x,roots)
print(sum(max_benefit(r) for r in roots))