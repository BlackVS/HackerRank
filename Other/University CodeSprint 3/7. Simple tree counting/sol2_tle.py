#!/bin/python3
import sys
from functools import reduce
from itertools import *
input = sys.stdin.readline
#sys.stdin = open('..\\..\\t2.txt', 'r')


sign = lambda a: (a>0) - (a<0)

class Graph(object):
    def __init__(self,n):
        self.VN=n
        self.G = [ [] for _ in range(n)]
        self.EDGES = list()
        self.EDGES_CLR = [-1]*(n-1)
        self.COMP_P = None
        #parent in sense of sub-graphs
        self.SB_P=None
        #root of current subgraph i.e. subgraph id
        self.SB_R=None
        #end of current subgraph
        self.SB_L=None
        self.Flag = False

    def add_edge(self,idx,x,y,color):
        self.G[x].append( idx )
        self.G[y].append( idx )
        self.EDGES.append( {x,y} )
        self.EDGES_CLR[idx]=color

    def mergeEdges(self,e1,e2):
        N=self.VN
        #parent
        P=self.SB_P
        #root
        R=self.SB_R
        #end
        L=self.SB_L
        r1 = R[e1]
        r2 = R[e2]
        if r1 == r2:
            return
        r1, r2=min(r1,r2),max(r1,r2)
        P[r2]=L[r1]
        L[r1]=L[r2]
        e=L[r1]
        while R[e]==r2:
            R[e]=r1
            e=P[e]

    def compress(self):
        N = self.VN
        G = self.G
        E = self.EDGES
        E_CLR = self.EDGES_CLR

        self.SB_P=[None]*(N-1)
        self.SB_R=list(range(N-1))
        self.SB_L=list(range(N-1))
        
        for v in range(N):
            #for each vertex check and combine edges
            if len(G[v])>1:
                for ie0,ie1 in combinations(G[v],2):
                    if E_CLR[ie0]==E_CLR[ie1]:
                        #join
                        self.mergeEdges(ie0,ie1)
        self.COMP_P = None
        self.Flag = True

    def edgeChangeColor(self, idx, clr):
        self.EDGES_CLR[idx]=clr
        self.Flag = False

    def calcP(self,comp_id):
        if not self.Flag:
            self.compress()
        if not self.COMP_P:
            self.COMP_P = [0]*(self.VN-1)
        COMP_P = self.COMP_P
        if COMP_P[comp_id]:
            return COMP_P[comp_id]
        res=0
        R=self.SB_R
        E=self.EDGES
        V=set()
        for ie in range(len(E)):
            if R[ie]==comp_id:
                V|=E[ie]
        n=len(V)
        return n*(n-1)//2

    def countSumT(self, l, r):
        if not self.Flag:
            self.compress()
        R=self.SB_R
        E=self.EDGES
        E_CLR=self.EDGES_CLR
        CMPS={e for e in R if E_CLR[e]>=l and E_CLR[e]<=r}
        res=0
        for c in CMPS:
            res+=self.calcP(c)
        return res

    def countP(self, eidx):
        if not self.Flag:
            self.compress()
        cmpid=self.SB_R[eidx]
        return self.calcP(cmpid)

if __name__ == "__main__":
    N = int(input().strip())
    G = Graph(N)
    for i in range(N-1):
        u, v, c = map(int, input().strip().split(' '))
        G.add_edge(i,u-1,v-1,c)
    #G.compress()
    Q = int(input().strip())
    for _ in range(Q):
        s=input().strip()

        if s[0]=='1':
            _,i,c = map(int,s.split())
            G.edgeChangeColor(i-1,c)
        elif s[0]=='2':
            _,l,r = map(int,s.split())
            print(G.countSumT(l,r))
        else:
            _,i = map(int,s.split())
            print(G.countP(i-1))
