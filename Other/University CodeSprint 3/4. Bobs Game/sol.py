#!/bin/python3
import sys
input = sys.stdin.readline
#sys.stdin = open('..\\..\\t1.txt', 'r')

DIRS = [ (0,-1),(-1,-1),(-1,0) ]
SEMPTY = 0
SBROKEN= -1
SLAT = { '.':SEMPTY, 'X':SBROKEN }

def printBoard(brd):
    print("Board:")
    for bl in brd:
        for b in bl:
            print("%3i" % (b),end='')
        print()

def getMex(brd,r,c):
    res=[0,0,0,0]
    for dr,dc in DIRS:
        rn,cn=r+dr,c+dc
        if rn>=0 and cn>=0:
            v=brd[rn][cn]
            if v>=0:
                res[v]=1
    return res.index(0)

def prepareBoard(brd):
    N=len(brd)
    for r in range(N):
        for c in range(N):
            if brd[r][c]==0:
                brd[r][c]=getMex(brd,r,c)

def countWins(brd,kings):
    #get G for all kings
    GS=0
    for r,c in KINGS:
        GS^=BOARD[r][c]
    #print("G all = ",GS)
    #no win moves
    if GS==0: return 0
    res=0
    #check each king win moves
    for r,c in KINGS:
        GK=GS^BOARD[r][c]
        for dr,dc in DIRS:
            rn,cn=r+dr,c+dc
            if rn>=0 and cn>=0:
                v=brd[rn][cn]
                res+=v==GK
    return res

if __name__ == "__main__":
    Q = int(input().strip())
    for _ in range(Q):
        N = int(input().strip())
        BOARD = []
        KINGS = []
        for r in range(N):
            s = list(input().strip())
            for c in range(N):
                if s[c]=='K':
                    s[c]=SEMPTY
                    KINGS.append( (r,c) )
                else:
                    s[c]=SLAT[s[c]]
            BOARD.append(s)
        #printBoard(BOARD)
        prepareBoard(BOARD)
        #print(KINGS)
        #printBoard(BOARD)
        r=countWins(BOARD,KINGS)
        if r:
            print("WIN",r)
        else:
            print("LOSE")
