#!/bin/python3

import sys

#dr,dc
DIRS={'s':(1,0), 'n':(-1,0), 'e':(0,+1), 'w':(0,-1) }

if __name__ == "__main__":
    N = int(input().strip())
    dr,dc = DIRS[input().strip()]
    #print(dr,dc)
    r0, c0 = map(int, input().strip().split(' '))
    res=[ [0]*N for _ in range(N) ]
    for i in range(1,N*N+1):
        res[r0][c0]=i
        #try by wind
        rn,cn=r0+dr,c0+dc
        if rn>=0 and rn<N and cn>=0 and cn<N and not res[rn][cn]:
            r0,c0=rn,cn
            continue
        #check 90 degree
        rn,cn=r0+dc,c0+dr
        if rn>=0 and rn<N and cn>=0 and cn<N and not res[rn][cn]:
            r0,c0=rn,cn
            continue
        rn,cn=r0-dc,c0-dr
        if rn>=0 and rn<N and cn>=0 and cn<N and not res[rn][cn]:
            r0,c0=rn,cn
            continue
        #finally against 
        rn,cn=r0-dr,c0-dc
        if rn>=0 and rn<N and cn>=0 and cn<N and not res[rn][cn]:
            r0,c0=rn,cn
            continue
        #print("WTF")
        break
    for r in res:
        print(" ".join(map(str,r)))