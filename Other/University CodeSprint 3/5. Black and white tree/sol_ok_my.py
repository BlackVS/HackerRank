#!/bin/python3
import sys, copy
from functools import reduce
from collections import deque
#input = stdin.readline
sys.stdin = open('..\\..\\t4.txt', 'r')


sign = lambda a: (a>0) - (a<0)

class Graph(object):
    def __init__(self,n):
        self.VN=n
        self.G = [set() for _ in range(n)]
        # DFS
        self.DFS_NR  = None
        self.DFS_PR  = None
        self.DFS_MXW = None
        self.DFS_MXB = None
        #self.W = [1]*n

    def setWeights(self,itw):
        self.W=list(itw)

    def add_edge(self,x,y):
        self.G[x].add( y )
        self.G[y].add( x )


    def DFS(self,root=0):
        N  = self.VN
        W  = self.W
        NR = self.DFS_NR = [-1]*N
        P  = self.DFS_PR = [None]*N
        MXW= self.DFS_MXW= [0]*N
        MXB= self.DFS_MXB= [0]*N
        #
        GU=copy.deepcopy(self.G)
        i=0
        v=root
        NR[v]=i
        MXW[v] = -W[v]
        MXB[v] = +W[v]
        #
        res_ind=v
        res_max=max(MXW[v],MXB[v])
        res_whites=MXW[v]>MXB[v]
        while True:
            while GU[v]:
                w=GU[v].pop()
                if w==P[v]: continue
                if NR[w]==-1:
                    #first visit - init sum
                    # White=-1 Black=1, whites increase whites, decrease blacks and vice versa
                    MXW[w] = -W[w]
                    MXB[w] = +W[w]
                    #go to child
                    P[w]=v
                    i+=1
                    NR[w]=i
                    v=w
            #all child processed, correct parent result 
            p=P[v]
            if v!=root:
                #parent initially +1(black) or -1(White). Correct sum if it increased only
                MXW[p] += max(0, MXW[v]);
                MXB[p] += max(0, MXB[v]);
                r=max(MXW[p],MXB[p])
                if r>res_max:
                    res_ind=p
                    res_max=r
                    res_whites=MXW[p]>MXB[p]
            if v==root and not GU[v]:
                break;
            v=P[v]
        #get path, again via BFS
        res_path=[]
        stack=deque([res_ind])
        while stack:
            v=stack.pop()
            res_path.append(v)
            for w in self.G[v]:
                if P[v]==w: continue
                r=(MXB[w],MXW[w])[res_whites]
                if r>0:
                    stack.append(w)

        return (res_max,res_path)

if __name__ == "__main__":
    N   = int(input().strip())
    G = Graph(N)
    G.setWeights(map(lambda c:(-1,1)[c=='1'], input().strip().split(' ')))
    for _ in range(N-1):
        u, v = map(int, input().strip().split(' '))
        G.add_edge(u-1,v-1)
    res=G.DFS()
    print(res[0])
    print(len(res[1]))
    print(" ".join(map(lambda i:str(i+1),res[1])))