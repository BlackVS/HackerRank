#!/bin/python3
import sys
from functools import reduce
from functools import lru_cache

#input = stdin.readline
sys.stdin = open('..\\..\\t2.txt', 'r')

DR = 8+1
DIRS8 = ( -DR-1, -DR, -DR+1, -1, 1, DR-1, DR, DR+1 )
      

#@lru_cache(None)
def count1(brd,scrt,idx,res=0):
    s=brd[idx]
    brd[idx]=0
    if len(scrt)==1:
        res+=1
    else:
        for dd in DIRS8:
            nidx=idx+dd
            if nidx>=0 and brd[nidx]==scrt[1]:
                res=count1(brd,scrt[1:],nidx,res)
    brd[idx]=s
    return res

def count2(brd,scrt,idx):
    if len(scrt)==1:
        return 1
    res=0
    s=brd[idx]
    brd[idx]=0
    for dd in DIRS8:
        nidx=idx+dd
        if nidx>=0 and brd[nidx]==scrt[1]:
            res+=count1(brd,scrt[1:],nidx)
    brd[idx]=s
    return res

def solve():
    secret = list(map(ord,input().strip()))
    board = [0]*DR*DR
    for r in range(8):
        for c,sym in enumerate(input().strip()):
            board[r*DR+c]=ord(sym)
    res=0
    for r in range(8):
        for c in range(8):
            idx=r*DR+c
            if board[idx]==secret[0]:
                res+=count2(board,secret,idx)
    return res

if __name__ == "__main__":
    k = int(input().strip())
    print(solve())