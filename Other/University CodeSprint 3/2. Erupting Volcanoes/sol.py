#!/bin/python3

import sys
from sys import stdin
input = stdin.readline

if __name__ == "__main__":
    N = int(input().strip())
    M = int(input().strip())
    Ws= list()
    xmin=ymin=N-1
    xmax=ymax=0
    
    for _ in range(M):
        x, y, w = map(int, input().strip().split(' '))
        Ws.append( (x,y,w) )
        xmin=min(x, xmin)
        xmax=max(x, xmax)
        ymin=min(y, ymin)
        ymax=max(y, ymax)
    # print(xmin,xmax,ymin,ymax)
    res=0
    for i in range(xmin,xmax+1):
        for j in range(ymin,ymax+1):
            curw=0
            for x,y,w in Ws:
                d=max( abs(i-x), abs(j-y) )
                dw = max(0, w-d) 
                curw+=dw
            # print(curw,end='')
            res=max(res,curw)
        # print()
    print(res)