#!/bin/python3

import sys


g = int(input().strip())
for a0 in range(g):
    n,m,x = input().strip().split(' ')
    n,m,x = [int(n),int(m),int(x)]
    a = list(map(int, input().strip().split(' ')))
    b = list(map(int, input().strip().split(' ')))
    
    i = 0
    while i < len(a) and x >= a[i]:
        x -= a[i]
        i += 1
    
    ans = i
    j = 0
    for p in b:
        j += 1
        x -= p
        
        while x < 0 and i > 0:
            i -= 1
            x += a[i]
        
        if x >= 0: ans = max(ans, j + i)
    
    print(ans)