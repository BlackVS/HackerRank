#!/bin/python3
import sys
from collections import *

fstd="t2"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

def solve():
    N = int(input().strip())
    R = []
    XS = defaultdict(set)
    YS = defaultdict(set)
    
    for _ in range(N):
        x1,y1,x2,y2=map(int,input().split())
        for x in range(x1,x2+1):
            for y in range(y1,y2+1):
                XS[x].add(y)
                YS[y].add(x)
    X2DEL=[]
    Y2DEL=[]
    while XS or YS:
        #check X to delete
        x2cut=None
        ymax =0
        for x,ys in XS.items():
            if x2cut==None or len(ys)>ymax:
                x2cut=x
                ymax=len(ys)
        #print(x2cut,ymax)
        #check Y to delete
        y2cut = None
        xmax  = 0
        for y,xs in YS.items():
            if y2cut==None or len(xs)>xmax:
                y2cut=y
                xmax=len(xs)
        #print(y2cut,xmax)
        
        if ymax>xmax:#cut x2cut
            for y in XS[x2cut]:
                YS[y].discard(x2cut)
                if len(YS[y])==0:
                    del YS[y]
            del XS[x2cut]
            X2DEL.append(x2cut)
        else: #cut y2cut
            for x in YS[y2cut]:
                XS[x].discard(y2cut)
                if len(XS[x])==0:
                    del XS[x]
            del YS[y2cut]
            Y2DEL.append(y2cut)
        
    print(len(X2DEL)+len(Y2DEL))
    print(len(X2DEL))
    if len(X2DEL)==0:
        print()
    else:
        print(" ".join(map(str,X2DEL)))
    print(len(Y2DEL))
    if len(Y2DEL)==0:
        print()
    else:
        print(" ".join(map(str,Y2DEL)))

if __name__ == "__main__":
    solve()