// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <cstdio>
#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <utility>
using namespace std;


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

void solve() {
	int N;
	cin >> N;
	map<int, set<int>> XS;
	map<int, set<int>> YS;

	for (int i = 0; i < N; i++) {
		int x1, y1, x2, y2;
		cin >> x1 >> y1 >> x2 >> y2;
		for (int x = x1; x <= x2; x++)
			for (int y = y1; y <= y2; y++) {
				XS[x].insert(y);
				YS[y].insert(x);
			}
	}
	vector<int> X2DEL;
	vector<int> Y2DEL;
	while (XS.size() || YS.size()) {
		int x2cut = -1;
		int ymax = 0;
		for (auto& it : XS) {
			if (x2cut<0 || it.second.size()>ymax) {
				x2cut = it.first;
				ymax = it.second.size();
			}
		}
		int y2cut = -1;
		int xmax = 0;
		for (auto& it : YS) {
			if (y2cut<0 || it.second.size()>xmax) {
				y2cut = it.first;
				xmax = it.second.size();
			}
		}

		if (ymax > xmax) {
			for (auto y : XS[x2cut]) {
				YS[y].erase(x2cut);
				if (YS[y].size() == 0) {
					YS.erase(y);
				}
			}
			XS.erase(x2cut);
			X2DEL.push_back(x2cut);
		}
		else {
			for (auto x : YS[y2cut]) {
				XS[x].erase(y2cut);
				if (XS[x].size() == 0) {
					XS.erase(x);
				}
			}
			YS.erase(y2cut);
			Y2DEL.push_back(y2cut);
		}
	}
	cout << X2DEL.size() + Y2DEL.size() << endl;
	cout << X2DEL.size() << endl;
	if (X2DEL.size() == 0) {
		cout << endl;
	}
	else {
		for (int i = 0; i < X2DEL.size(); i++) {
			if (i > 0) cout << " ";
			cout << X2DEL[i];
		}
		cout << endl;
	}
	cout << Y2DEL.size() << endl;
	if (Y2DEL.size() == 0) {
		cout << endl;
	}
	else {
		for (int i = 0; i < Y2DEL.size(); i++) {
			if (i > 0) cout << " ";
			cout << Y2DEL[i];
		}
		cout << endl;
	}
}
//        
//        if ymax>xmax:#cut x2cut
//            for y in XS[x2cut]:
//                YS[y].discard(x2cut)
//                if len(YS[y])==0:
//                    del YS[y]
//            del XS[x2cut]
//            X2DEL.append(x2cut)
//        else: #cut y2cut
//            for x in YS[y2cut]:
//                XS[x].discard(y2cut)
//                if len(XS[x])==0:
//                    del XS[x]
//            del YS[y2cut]
//            Y2DEL.append(y2cut)
//        
//    print(len(X2DEL)+len(Y2DEL))
//    print(len(X2DEL))
//    if len(X2DEL)==0:
//        print()
//    else:
//        print(" ".join(map(str,X2DEL)))
//    print(len(Y2DEL))
//    if len(Y2DEL)==0:
//        print()
//    else:
//        print(" ".join(map(str,Y2DEL)))

int main() {
	boost;
	solve();
}