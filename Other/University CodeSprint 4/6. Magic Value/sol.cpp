// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

//#include <intrin.h>
#include <cmath>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <map>
#include <utility>
using namespace std;

#define MAXG 8000

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

#define MOD 1000000007

/**/
int gcd(int a, int b)
{
if (a < b)
swap(a, b);
if (b == 0)
return a;
return gcd(b, a%b);
}

/*/
unsigned int gcd(unsigned int u, unsigned int v)
{
	int shift;
	if (u == 0) return v;
	if (v == 0) return u;
	shift = __builtin_ctz(u | v);
	u >>= __builtin_ctz(u);
	do {
		v >>= __builtin_ctz(v);
		if (u > v) {
			unsigned int t = v;
			v = u;
			u = t;
		}
		v = v - u;
	} while (v != 0);
	return u << shift;
}
/**/


#define MAXN 200000
int A[MAXN];
int N = 0;

typedef long long ll;


int GCDMIN[MAXG + 1][MAXG + 1];
int GCDMAX[MAXG + 1][MAXG + 1];


int main() {
	boost;

	cin >> N;
	for (int i = 0; i < N; i++)
		cin >> A[i];

	if (N>MAXG) {
		cout << 0;
		return 0;
	}
	for (int i = 0; i<N; i++) {
		int g = gcd(A[i], A[i]);
		int vmin = g;
		int vmax = g;
		int idx = 0;
		for (int j = i; j<N; j++,idx++) {
			g = gcd(g, A[j]);
			vmin = MIN(vmin, g);
			vmax = MAX(vmax, g);
			GCDMIN[i][j] = vmin;
			GCDMAX[i][j] = vmax;
		}
	}

	int res = 0;
	for (int d = 2; d <= N; d++) {
		for (int l = 0; l < N - d + 1; l++) {
			int r = l + d - 1;
			ll vmin = LLONG_MAX;
			ll vmax = 0;
			ll vm = 0;
			ll g = 0;
			for (ll i = l; i <= r; i++) {
				ll vmin1 = GCDMIN[i][r] * (i - l + 1l);
				ll vmax1 = GCDMAX[i][r] * (i - l + 1l);
				vmin = MIN(vmin, vmin1);
				vmax = MAX(vmax, vmax1);
			}			
			int magic = ((vmax - vmin)*d) % MOD;
			res = (res + magic) % MOD;
		}
	}
	cout << res;
}