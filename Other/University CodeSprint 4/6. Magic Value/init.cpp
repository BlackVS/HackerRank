#include <bits/stdc++.h>

using namespace std;

vector<string> split_string(string);


/*
 * Complete the function below.
 */
long sumOfMagicValues(vector<long> A) {
    // Return the sum of the magic values of all nonempty contiguous subarrays of A modulo 10^9+7.


}


int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    int A_size;
    cin >> A_size;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    string A_str_temp;
    getline(cin, A_str_temp);

    vector<string> A_str = split_string(A_str_temp);

    vector<int> A(A_size);
    for (int A_i = 0; A_i < A_size; A_i++) {
        int A_item = stoi(A_str[A_i]);

        A[A_i] = A_item;
    }

    long result = sumOfMagicValues(A);

    fout << result << "\n";

    fout.close();

    return 0;
}

vector<string> split_string(string input_string) {
    string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {
        return x == y and x == ' ';
    });

    input_string.erase(new_end, input_string.end());

    while (input_string[input_string.length() - 1] == ' ') {
        input_string.pop_back();
    }

    vector<string> splits;
    char delimiter = ' ';

    size_t i = 0;
    size_t pos = input_string.find(delimiter);

    while (pos != string::npos) {
        splits.push_back(input_string.substr(i, pos - i));

        i = pos + 1;
        pos = input_string.find(delimiter, i);
    }

    splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));

    return splits;
}
