#!/bin/python3
import os,sys
from collections import *

fstd="t1"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

c2i=lambda c:(ord(c)-ord('a'))

def getFR(s):
    res=[0]*26
    for c in s:
        res[c2i(c)]+=1
    return res

def solve():
    PAT = input()
    plen=len(PAT)

    S = input()
    slen=len(S)

    FPAT=getFR(PAT);
    FC  =getFR(S[:plen])
    FCNT=defaultdict(int)

    i=0
    fFound=False
    while i+plen<=slen:
        if FPAT==FC:
            FCNT[ S[i:i+plen] ]+=1
            fFound=True
        if i+plen+1<=slen:
            FC[ c2i(S[i]) ]-=1
            FC[ c2i(S[i+plen]) ]+=1
        i+=1
    if not fFound:
        return -1
    cmax=max( FCNT.values() )
    rmax=None
    for p,c in FCNT.items():
        if c==cmax and (rmax==None or p<rmax):
            rmax=p
    return rmax




if __name__ == '__main__':
    #f = open(os.environ['OUTPUT_PATH'], 'w')
    T = int(input())
    for _ in range(T):
        print(solve())
        #f.write(result + "\n")
    #f.close()