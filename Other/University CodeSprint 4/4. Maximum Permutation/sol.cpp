// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <cstdio>
#include <iostream>
#include <string>
#include <utility>
#include <map>
using namespace std;


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

using namespace std;


/*
* Complete the function below.
*/
string maximumPermutation(string pat, string s) {
	// Return the string representing the answer.
	int plen = (int)pat.size();
	int slen = (int)s.size();

	int FPAT[26];
	int FC[26];
	map< string, int > FCNT;

	memset(FPAT, 0, sizeof(FPAT));
	memset(FC, 0, sizeof(FC));

	int i = 0;
	for (auto c : pat) {
		FPAT[c - 'a']++;
		FC[s[i] - 'a']++;
		i++;
	}

	i = 0;
	bool bFound = false;
	while (i + plen <= slen) {
		bool bEq = true;
		for(int j=0;j<26;j++)
			if (FPAT[j] != FC[j]) {
				bEq = false;
				break;
			}
		if (bEq) {
			FCNT[s.substr(i, plen)]++;
			bFound = true;
		}
		if (i + plen + 1 <= slen) {
			FC[s[i] - 'a'] --;
			FC[s[i + plen] - 'a']++;
		}
		i++;
	}
	if(!bFound)
		return "-1";
	int cmax = -1;
	for (auto& it : FCNT) {
		if (cmax == -1 || cmax < it.second)
			cmax = it.second;
	}
	string res;
	for (auto& it : FCNT) {
		if (cmax == it.second && (res.size() == 0 || res > it.first))
			res = it.first;
	}
	return res;
}


int main()
{
	//ofstream fout(getenv("OUTPUT_PATH"));

	int t;
	cin >> t;
	cin.ignore(numeric_limits<streamsize>::max(), '\n');

	for (int i = 0; i < t; i++) {
		string w;
		getline(cin, w);

		string s;
		getline(cin, s);

		string result = maximumPermutation(w, s);

		//fout << result << "\n";
		cout << result << endl;
	}

	//fout.close();

	return 0;
}

