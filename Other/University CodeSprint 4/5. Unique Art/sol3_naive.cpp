// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <cstdio>
#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <utility>
#include <algorithm>
using namespace std;


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

#define MAXT 1000000

map<int, int> DICT;

typedef struct
{
	int l, r, idx;
} QUERY;

vector<QUERY> queries;

// cmp function to sort queries according to r
bool CMP_QUERY(QUERY x, QUERY y)
{
	if (x.l == y.l)
		return x.r < y.r;
	return x.l < y.l;
}


int FREQS[MAXT + 1];

int randint(int range_min, int range_max)
{
	// [range_min, range_max]. In other words,
	return (rand()*(range_max - range_min + 1)) / RAND_MAX + range_min;
}

int main() {
	int N;
	//boost;

	//cin >> N;
	N = 20;
	cout << N << endl;
	vector<int> T(N);
	int idx = 0;
	int t;
	for (int i = 0; i < N; i++) {
		//cin >> t;
		t = randint(-5, 5);
		cout << t;
		if (i < N - 1)
			cout << " ";
		else
			cout << endl;
		auto id = DICT.find(t);
		if (id == DICT.end()) {
			DICT[t] = idx;
			T[i] = idx;
			idx++;
		}
		else {
			T[i] = id->second;
		}
	}
	int MAXI = idx;

	int Q;
	//cin >> Q;
	Q = 10;
	cout << Q << endl;
	queries.resize(Q);
	for (int i = 0; i < Q; i++) {
		int l, r;
		//cin >> l; l--;
		//cin >> r; r--;
		l = randint(0, N - 1);
		r = randint(l, N - 1);
		cout << l+1 << " " << r+1 << endl;
		queries[i].idx = i;
		queries[i].l = l;
		queries[i].r = r;
	}
	std::sort(queries.begin(), queries.end(), CMP_QUERY);
	////////////////////solve()


	vector<int> ANS(Q);

	int iq = 0;
	int pL = -1;
	int pR = -1;

	set<int> FREQ1;

	for (int q = 0; q<Q; q++)
	{
		int l = queries[q].l;
		int r = queries[q].r;
		int idx = queries[q].idx;
		//cout << idx << " -> " <<l << " : " << r << endl;

		//reset if non overlapping
		//if (l >= pR) 
		{
			memset(FREQS, 0, MAXI * sizeof(FREQS[0]));
			FREQ1.clear();
			for (int j = l; j <= r; j++) {
				int t = T[j];
				int ft = FREQS[t];
				if (ft == 0)
					FREQ1.insert(t);
				else if (ft == 1)
					FREQ1.erase(t);
				FREQS[t]++;
			}
		}

		//
		ANS[idx] = (int)FREQ1.size();
	}

	// print answer for each query
	for (int i = 0; i<Q; i++)
		cout << ANS[i] << endl;
}