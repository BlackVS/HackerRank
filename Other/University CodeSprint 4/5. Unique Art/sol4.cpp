// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <cstdio>
#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <utility>
#include <algorithm>
using namespace std;


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

#define MAXT 1000000

map<int,int> DICT;

typedef struct 
{
	int l, r, idx;
} QUERY;

vector<QUERY> queries;

int block;

// Function used to sort all queries so that all queries 
// of same block are arranged together and within a block,
// queries are sorted in increasing order of R values.
bool CMP_QUERY(QUERY x, QUERY y)
{
	// Different blocks, sort by block.
	if (x.l / block != y.l / block)
		return x.l / block < y.l / block;

	// Same block, sort by R value
	return x.r < y.r;
}

int FREQS[MAXT + 1];

int main() {
	int N;
	//boost;

	cin >> N;
	// Find block size
	block = (int)sqrt(N);

	vector<int> T(N);
	int idx = 0;
	int t;
	for (int i = 0; i < N; i++) {
		cin >> t;
		auto id = DICT.find(t);
		if (id == DICT.end()) {
			DICT[t]=idx;
			T[i] = idx;
			idx++;
		}
		else {
			T[i] = id->second;
		}
	}
	int MAXI = idx;

	int Q;
	cin >> Q;
	queries.resize(Q);
	for (int i = 0; i < Q; i++) {
		queries[i].idx = i;
		cin >> queries[i].l; queries[i].l--;
		cin >> queries[i].r; queries[i].r--;
	}
	std::sort(queries.begin(), queries.end(), CMP_QUERY);
	////////////////////solve()
	

	vector<int> ANS(Q);

	int iq = 0;
	int pL = -1;
	int pR = -1;

	int res = 0;
	for (int q = 0; q<Q; q++)
	{
		int l = queries[q].l;
		int r = queries[q].r;
		int idx = queries[q].idx;
		//cout << idx << " -> " <<l << " : " << r ;

		//reset if non overlapping
		if (l >= pR) 
		{
			memset(FREQS, 0, MAXI*sizeof(FREQS[0]));
			pL = pR = l;
			int t = T[l];
			FREQS[t]++;
			res = 1;//itself
		}


		//adjust left
		while (pL < l) {
			int t = T[pL];
			FREQS[t]--;
			if (FREQS[t] == 1) //new 1s
				res++;
			else if (FREQS[t] == 0)
				res--;
			pL++;
		}
		// left <-
		while (pL>l) {
			pL--;
			int t = T[pL];
			FREQS[t]++;
			if (FREQS[t] == 1) //new 1s
				res++;
			else if (FREQS[t] == 2)
				res--;
		}


		//adjust right ->
		while (pR<r) {
			pR++;
			int t = T[pR];
			FREQS[t]++;
			if (FREQS[t] == 1) //new 1s
				res++;
			else if (FREQS[t] == 2)
				res--;
		}

		//adjust right <-
		while (pR>r) {
			int t = T[pR];
			FREQS[t]--;
			if (FREQS[t] == 1) //new 1s
				res++;
			else if (FREQS[t] == 0)
				res--;
			pR--;
		}

		//
		/*int res = 0;
		for (int j = l; j <= r; j++) {
			if (FREQS[T[j]] == 1)
				res++;
		}*/
		ANS[idx] = res;
		//cout << " = " << ANS[idx] << endl;
	}

	// print answer for each query
	for (int i = 0; i<Q; i++)
   		cout << ANS[i] << endl;
}