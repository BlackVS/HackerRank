#!/bin/python3
import sys, os
from itertools import *
fstd="t1"

sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')

#1. Only Queens and may be Knights have sense
#2. 2,3,4 is enough.
#    2 - if all cells around Kings including Kings are under attacj and it is safe
#    3 - the same as 2 but one\two figure ander attack but can be protected by one more Q or N
#    4 is always possible - 2 queens on one line around K or queen protected by N

NMOVES=( (-1,-2),(-1, 2),(-2,-1),(-2,1),(+1,-2),(+1, 2),(+2,-1),(+2,1) )

#returns (k1check, k2check, qcheck)
#i.e. if Q attack K1, K2 and if Q under attack from K1 or K2
def placeQ(board, hits, qx, qy, wq ):
    hits[qx][qy]|=wq
    # up
    y=qy
    for x in range(qx-1,-1,-1):
        hits[x][y]|=wq
        if board[x][y]==0:continue
        break
    # down
    for x in range(qx+1,8,1):
        hits[x][y]|=wq
        if board[x][y]==0:continue
        break
    # left
    x=qx
    for y in range(qy-1,-1,-1):
        hits[x][y]|=wq
        if board[x][y]==0:continue
        break
    # right
    for y in range(qy+1,8,1):
        hits[x][y]|=wq
        if board[x][y]==0:continue
        break
    #QB
    x,y=qx,qy
    while x>0 and y>0:
        x,y=x-1,y-1
        hits[x][y]|=wq
        if board[x][y]==0:continue
        break
    x,y=qx,qy
    while x>0 and y<7:
        x,y=x-1,y+1
        hits[x][y]|=wq
        if board[x][y]==0:continue
        break
    x,y=qx,qy
    while x<7 and y>0:
        x,y=x+1,y-1
        hits[x][y]|=wq
        if board[x][y]==0:continue
        break
    x,y=qx,qy
    while x<7 and y<7:
        x,y=x+1,y+1
        hits[x][y]|=wq
        if board[x][y]==0:continue
        break


DICT={-1:"k",0:".",1:"Q",2:"Q",3:"Q",4:"Q"}
def printBoard(board,hits):
    print("BOARD:")
    for b in board:
        print("".join(map(lambda x:DICT[x],b)))

#-1 King
# 1 Queen
# 

AROUND=((-1,-1),(-1,0),(-1,1),\
        ( 0,-1),       ( 0,1),\
        ( 1,-1),( 1,0),( 1,1))

def  checkHits(board,hits,kx,ky):
    #check all free cells around
    for dx,dy in AROUND:
        x,y=kx+dx,ky+dy
        if x<0 or y<0 or x>7 or y>7:continue
        if board[x][y]==0 and hits[x][y]==0:
            return False #cells for escape exist
    return True

def isCheck(board,hits,k1x,k1y,k2x,k2y):
    return hits[k1x][k1y] and hits[k2x][k2y]


q2xy=lambda q: (q>>3,q&7)

def checkmate(k1x, k1y, k2x, k2y):
    k1=k1x*8+k1y
    k2=k2x*8+k2y
    for z in range(2,4):
#        for zq in range(1,z+1):
        for zq in range(z,z+1):
            zn=z-zq
            FIGS=["Q"]*zq+["N"]*zn
            for zxy in combinations(list(range(64)),z):
                toCheck=True
                for zi in zxy:
                    if  zi==k1 or zi==k2:
                        toCheck-False
                        break
                if not toCheck:
                    continue


                qxy=list(map(lambda xy:q2xy(xy),zxy))
                #print(zxy,qxy)
                board=[[0]*8 for _ in range(8)]
                hits =[[0]*8 for _ in range(8)]
                
                board[k1x][k1y]=-1
                board[k2x][k2y]=-1
                for qx,qy in enumerate()
                #board[q1x][q1y]= 1
                #board[q2x][q2y]= 2
                #board[q3x][q3y]= 4
            
                #placeQ(board,hits,q1x, q1y, 1)
                #placeQ(board,hits,q2x, q2y, 2)
                #placeQ(board,hits,q3x, q3y, 4)

                #if isCheck(board,hits,k1x, k1y, k2x, k2y):
                #    #print("Check")
                #    #printBoard(board,hits);
                #    res=True
                #    #try move to free cell or hit blacks and check mate
                #    #K1
                #    for dx,dy in AROUND:
                #        x,y=k1x+dx,k1y+dy
                #        if x<0 or y<0 or x>7 or y>7:continue
                #        if x==k2x and y==k2y:continue
                #        board=[[0]*8 for _ in range(8)]
                #        hits =[[0]*8 for _ in range(8)]
                #        board[q1x][q1y]= 1
                #        board[q2x][q2y]= 2
                #        board[q3x][q3y]= 4
                #        board[x][y]=-1
                #        board[k2x][k2y]=-1
                #        placeQ(board,hits,q1x, q1y, 1)
                #        placeQ(board,hits,q2x, q2y, 2)
                #        placeQ(board,hits,q3x, q3y, 4)
                #        if not isCheck(board,hits,x, y, k2x, k2y):
                #            #print("Avoid Check")
                #            #printBoard(board,hits);
                #            res=False
                #            break
                #    for dx,dy in AROUND:
                #        x,y=k2x+dx,k2y+dy
                #        if x<0 or y<0 or x>7 or y>7:continue
                #        if x==k1x and y==k1y:continue
                #        board=[[0]*8 for _ in range(8)]
                #        hits =[[0]*8 for _ in range(8)]
                #        board[q1x][q1y]= 1
                #        board[q2x][q2y]= 2
                #        board[q3x][q3y]= 4
                #        board[k1x][k1y]=-1
                #        board[x][y]=-1
                #        placeQ(board,hits,q1x, q1y, 1)
                #        placeQ(board,hits,q2x, q2y, 2)
                #        placeQ(board,hits,q3x, q3y, 4)
                #        if not isCheck(board,hits,k1x, k1y, x, y):
                #            #print("Avoid Check")
                #            #printBoard(board,hits);
                #            res=False
                #            break
                #    #all around is checked
                #    if res:
                #        print(3)
                #        print('Q',q1x,q1y)
                #        print('Q',q2x,q2y)
                #        print('Q',q3x,q3y)
                #        return True
    return False

if __name__ == '__main__':
    T = int(input())
    for _ in range(T):
        x1,y1,x2,y2 = map(int,input().split())
        checkmate(x1, y1, x2, y2)
