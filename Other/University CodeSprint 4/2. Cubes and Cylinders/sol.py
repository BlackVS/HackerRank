#!/bin/python3
import os, sys

fstd="t2"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')

# Complete the function below.
sqrt2=(2**0.5)/2

def getRes(NP,PS,PK,NC,CR,CC):
    # Return the maximum number of packages that can be put in the containers.
    print("Packages=",NP)
    print("Package size: ", PS)
    print("Package qnty: ", PK)
    P=sorted( [[PS[i]*sqrt2,PK[i]] for i in range(NP)])
    print(P)
    print("Containers=",NC)
    print("Container size: ", CR)
    print("Package qnty: ", CC)
    C=sorted( [[CR[i],CC[i]] for i in range(NC)], reverse=False)
    print(C)
    res=0
    for j in range(NC):
        if C[j][1]==0: continue
        for i in range(NP):
            if P[i][1]==0: continue
            if C[j][0]>P[i][0]:
                dc=min(C[j][1],P[i][1])
                res+=dc
                P[i][1]-=dc
                C[j][1]-=dc
                print(j,i,dc)
                print(P)
                print(C)

    return res



def solve():
    #f = open(os.environ['OUTPUT_PATH'], 'w')
    NP, NC = map(int, input().split())
    #package edge length of type i
    PS = list(map(int, input().rstrip().split()))
    #number packages of type i
    PK = list(map(int, input().rstrip().split()))
    #container radius
    CR = list(map(int, input().rstrip().split()))
    #cantainer capacity
    CC = list(map(int, input().rstrip().split()))
    result = getRes(NP,PS,PK,NC,CR,CC)
    #f.write(str(result) + "\n")
    #f.close()
    print(result)


if __name__ == '__main__':
    solve()