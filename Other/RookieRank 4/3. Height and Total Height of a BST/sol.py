#!/bin/python3
import sys
fstd="t2"

sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')

#!/bin/python3

class Node:
    def __init__(self, val, d):
        self.val = val
        self.depth = d
        self.height = None
        self.leftChild = None
        self.rightChild = None
    
    def get(self):
        return self.val
    
    def set(self, val):
        self.val = val
        
    def getChildren(self):
        children = []
        if(self.leftChild != None):
            children.append(self.leftChild)
        if(self.rightChild != None):
            children.append(self.rightChild)
        return children

    def printNode(self):
        print(self.val,self.depth)
        if self.leftChild:
            self.leftChild.printNode()
        if self.rightChild:
            self.rightChild.printNode()

    def calcHeight(self):
        h,s=0,0
        #
        hl,sl=-1,0
        if self.leftChild:
            hl,sl=self.leftChild.calcHeight()

        hr,sr=-1,0
        if self.rightChild:
            hr,sr=self.rightChild.calcHeight()

        h=1+max(hr,hl)
        s=h+sl+sr
        return (h,s)
        
class BST:
    def __init__(self):
        self.root = None
        self.height = 0
 
    def setRoot(self, val):
        self.root = Node(val,0)
        self.height =0;

    def insert(self, val):
        if(self.root is None):
            self.setRoot(val)
        else:
            self.insertNode(self.root, val)

    def insertNode(self, currentNode, val):
        if(val < currentNode.val):
            if(currentNode.leftChild):
                return self.insertNode(currentNode.leftChild, val)
            else:
                currentNode.leftChild = Node(val,currentNode.depth+1)
                self.height=max(self.height,currentNode.depth+1)
                return True

        elif(val > currentNode.val):
            if(currentNode.rightChild):
                return self.insertNode(currentNode.rightChild, val)
            else:
                currentNode.rightChild = Node(val,currentNode.depth+1)
                self.height=max(self.height,currentNode.depth+1)
                return True
        #else: #vall==currentNode.val
        return False

    def printTree(self):
        print("Height=",self.height)
        self.root.printNode()

    def calcHeights(self):
        if not self.root: return (0,0)
        return self.root.calcHeight()

if __name__ == '__main__':
    N = int(input())
    A = list(map(int, input().rstrip().split()))
    print(N,A)
    
    bst=BST();
    bst.setRoot(A[0])
    for a in A[1:]:
        bst.insert(a)
    bst.printTree()
    res=bst.calcHeights()
    print(res)
