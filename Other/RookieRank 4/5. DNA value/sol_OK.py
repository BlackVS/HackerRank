import sys
#Editorial

def getPrefFunc(S):
    N=len(S)
    P=[0]*N
    k=-1
    P[0] = -1
    for i in range(1,N):
        while k > -1 and S[i] != S[k + 1]:
           k = P[k]
        if S[i] == S[k + 1]:
           k+=1
        P[i] = k
    return P

def getZFunc(S):
    N=len(S)
    Z=[0]*N
    i=1
    L=0
    R=1
    while i<N:
        Z[i]=min(R-i,Z[i-L])
        while i+Z[i]<N and S[i+Z[i]]==S[Z[i]]:
            Z[i]+=1
            R=i+Z[i]
            L=i
        i+=1
        if i>=R:
            R=i
    return Z


def solve2(S):
    N=len(S)
    P=getPrefFunc(S)
    Z=getZFunc(S)

    K=21 #???? max string len is 30 or?
    PP=[ [0]*N for _ in range(K)]
    PP[0]=P.copy()

    for k in range(1,K):
        for i in range(N):
            if PP[k-1][i]>=0:
                PP[k][i] = PP[k - 1][PP[k - 1][i]]
            else:
                PP[k][i]=-1

    Z[0] = N
    res=[0]*N
    for i in range(N):
        if Z[i]>=i+1:
            res[i]=i+1
            continue
        v=i
        for k in range(K-1,-1,-1):
            if PP[k][v] + 1 > Z[i]: 
                v = PP[k][v]
        res[i]=P[v]+1
    return res


def solve():
    s = input().strip()
    return solve2(s)

if __name__ == "__main__":
    print(" ".join(map(str,solve())))