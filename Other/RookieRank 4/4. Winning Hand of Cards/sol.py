#!/bin/python3
import sys
from itertools import *
from functools import *
from collections import *
from operator import mul
from random import *

fstd="t4"

sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')

#very slow 
def solve0(X,M,R):
    N=len(X)
    res=0
    for c in range(1,N+1):
        for vv in combinations(X,c):
            s=reduce(lambda x,y:(x*y)%M,vv)
            res+=(s%M)==R
    return res

#semi-DP
def solve1(X,M,R):
    rset=defaultdict(int)
    rset[ X[0] ]=1
    for x in X[1:]:
        rnew=rset.copy()
        rnew[x]+=1
        for v,c in rset.items():
            rnew[ (v*x)%M ]+=c
        rset=rnew
    return  rset[R]

def solve():
    N, M, R = map(int, input().strip().split())
    X=tuple(map(lambda s: int(s)%M,input().strip().split()))
    #N=18
    #M=1000;
    #R=randint(0,M-1)
    #MAXX=10000000
    #X=[ randint(1,MAXX)%M for _ in range(N)]
    #print(N,M,R)
    #print(X)
    #res0=solve0(X,M,R)
    res1=solve1(X,M,R)
    #print(res0,res1)
    return res1

if __name__ == "__main__":
    print(solve())