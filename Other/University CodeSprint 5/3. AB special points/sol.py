#!/bin/python3
import sys

fstd="t3"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

# vertices have nums 0..N-1
class Graph(object):
    def __init__(self,n):
        self.VN=n
        self.G = [[] for _ in range(n)]
        self.VDegree = [0]*n
        # DFS
        self.DFS_NR  = None
        self.DFS_PR  = None
        #SubGraphs
        self.SB_P=None
        self.SB_R=None
        self.SB_L=None
        self.SB_N=None

    def __str__(self,shortform=True):
        if shortform:
            return str(self.G)
        res = "vertices: " + str(self.VN)
        res += "\nedges:\n"
        for i,x in enumerate(self.G):
            for y in x:
                res += str(i) + " -> " + str(y) + "\n"
        return res

    def merge(self,v1,v2):
        N=self.VN
        #init
        if not self.SB_P:
            self.SB_P=[None]*N
            self.SB_R=list(range(N))
            self.SB_L=list(range(N))
            self.SB_N=N
        #vertex parent
        P=self.SB_P
        #subgraph root
        R=self.SB_R
        #subgraph end
        L=self.SB_L
        r1 = R[v1]
        r2 = R[v2]
        if r1 == r2:
            #already in one subgraph
            return
        #join subgraphs
        r1, r2=min(r1,r2),max(r1,r2)
        P[r2]=L[r1]
        L[r1]=L[r2]
        v=L[r1]
        while R[v]==r2:
            R[v]=r1
            v=P[v]
        self.SB_N-=1

    def isSameSubgraph(self,U,V):
        if not self.SB_R:
            return None
        return self.SB_R[U]==self.SB_R[V]

    def add_edge(self,x,y,directed=False,fmerge=True):
        self.G[x].append(y)
        self.VDegree[x]+=1
        if not directed:
            self.G[y].append(x)
            self.VDegree[y]+=1
        if fmerge:
            self.merge(x,y)

    def solve(self,A,B):
        SB_N=self.SB_N
        SB_R=self.SB_R
        VN=self.VN
        VD=self.VDegree

        res=0
        #we have few components (possible) - check results separatly
        C_res=[0]*VN #component results
        C_min=[None]*VN #VDegree min for each component, could be joined with merge
        C_max=[None]*VN #VDegree max for each component, could be joined with merge

        # check min-max first
        for i in range(VN):
            c=SB_R[i]
            if C_min[c]==None or VD[i]<C_min[c]:
                C_min[c]=VD[i]
            if C_max[c]==None or VD[i]>C_max[c]:
                C_max[c]=VD[i]
        # count a-b
        for i in range(VN):
            c=SB_R[i]
            res+=VD[i]>C_min[c]*A and VD[i]<C_max[c]*B
        return res
          
s2i=lambda i:int(i)-1


if __name__ == "__main__":
    N,M,A,B = map(int,input().strip().split())
    G=Graph(N)
    #do we need renumber vertices? I.e. u,v <=N or not?
    for _ in range(M):
        u,v=map(s2i,input().strip().split())
        G.add_edge(u,v)

    res=G.solve(A,B)
    print(res)