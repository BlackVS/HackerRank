#!/bin/python3
import sys
from  itertools import *

fstd="t0"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

# from array A iterate through all possible subsets not longer K with sum S
# for each such subset call countK with
def countK(A,S,K,stage=0):
    res=0
    ####
    # check first A[0] itself
    if A[0]==S:
        if stage==1:
            res+=1
        else:
            res+=countK(A[1:], S, len(A)-2, 1)

    for c in range(1,K):
        for v in combinations(range(1,len(A)),c):
            V=[0]*len(A)
            V[0]=1
            s=A[0]
            for i in v:
                s+=A[i]
                V[i]=1
            if s==S:
                if stage==1:
                    res+=1
                    continue
                res+=countK([A[i] for i in range(len(A)) if V[i]==0], S, len(A)-(c+1)-1, 1)
    return res

def solve(A,N):
    if N<3:
        return 0
    s=sum(A)
    if s%3!=0:
        return 0
    S=s//3
    return countK(A,S,N-2)*6

if __name__ == "__main__":
    N = int(input())
    A = sorted(map(int, input().strip().split()))
    print(solve(A,N))