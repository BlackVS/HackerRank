#!/bin/python3
import sys
from collections import *
from itertools import *
from functools import *
from random import *

fstd="t1"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')

def primes(n):
    ps, sieve = [], [True] * (n+1)
    for p in range(2, n):
        if sieve[p]:
            ps.append(p**3)
            for i in range(p*p, n, p):
                sieve[i] = False
    return ps

PRIMES3=primes(100000)

def solveT(N):
    r=set()
    for p3 in PRIMES3:
        for n in range(p3,N+1,p3):
            r.add(n)
        if p3>N:
            break;
    return len(r)

if __name__ == "__main__":
    ##fptr = open(os.environ['OUTPUT_PATH'], 'w')
    Q = int(input().strip())
    INP=[]
    MX=0
    for q in range(Q):
        N = int(input().strip())
        INP.append( N )
        MX=max(MX,N)
    PRIMES=primes(int(N**(1/3))+1)
    for n in INP:
        res=solveT(n)
        #fptr.write(str(res)+"\n")
        print(res)
    ##fptr.close()
