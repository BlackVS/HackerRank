#!/bin/python3
import sys,copy
from collections import *
from itertools import *

fstd="t2"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

sys.setrecursionlimit(10000)

def less(string1, string2):
    # Compare character by character
    for idx in range(min(len(string1), len(string2))):
        # Get the "value" of the character
        ordinal1, ordinal2 = ord(string1[idx]), ord(string2[idx])
        # If the "value" is identical check the next characters
        if ordinal1 == ordinal2:
            continue
        # If it's smaller we're finished and can return True
        elif ordinal1 < ordinal2:
            return True
        # If it's bigger we're finished and return False
        else:
            return False
    # We're out of characters and all were equal, so the result is False
    return False

#directed, acyclic, no self-loops
class Graph(object):
    def __init__(self,n):
        self.VN=n
        self.G = [[] for _ in range(n)]
        self.W = [0]*n

        # DFS BFS
        self.D   = None #depth
        self.PR  = None #current parent
        self.RESPATH = None
        #self.D   = None
        #self.PTHC= None

    
    def add_edge(self,x,y):
        self.G[x].append( y )
        #self.G[y].append( x )

    def setVWeights(self,itw):
        self.W=itw

    def prepare(self):
        for g in self.G:
            g.sort(key=lambda i:self.W[i],reverse=True)

    def DFS(self,src,dst):
        N =self.VN
        W =self.W
        D =self.D   = [None]*N
        P =self.PR  = [None]*N
        #PTHC= self.PTHC= [None]*N
        #RMQ
        #D  = self.D   = [-1]*N
        #
        GU=self.G #no need to save G
        idx=0
        v=src
        D[v] =0
        RES = None
        PTHI = deque([ v ]) #in any case first
        PTHS = deque(W[v])
        idx+=1
        while True:
            #forward
            #get forward edges for v
            while GU[v]:
                w=GU[v].pop()
                if w==src:
                    print("Loop detected")
                    break
                if P[w]==None: #not in path yet, 
                    P[w]=v
                    D[w]=D[v]+1
                    PTHI.append(w)
                    PTHS.append(W[w])
                    v=w #else continue
                    if w==dst:
                        print(PTHI)
                        print(PTHS)
                        if RES==None or less(PTHS,RES):
                            RES=PTHS.copy()
                        break #avoid going back
                    
            #we back to v
            #update parent
            p=P[v]
            P[v]=None #reset visit label
            D[v]=None
            PTHI.pop() #cut path
            PTHS.pop()
            #if v!=root:
            if v==src and not GU[v]:
                break; #means we back to src
            v=p
        return "No way" if RES==None else "".join(RES)

    def BFS(self,root=0):
        N =self.VN
        W =self.W
        P  = self.PR  = [None]*N
        D  = self.D   = [0]*N
        #PTH= self.PATH= [ [] for _ in range(N) ]
        G=self.G #no need to save G
        i=0
        v=root
        D[root]=0
        q=deque([root])
        while q:
            v=q.popleft()
            pv=P[v]
            dv=D[v]
            for w in G[v]:
                if w==pv:continue
                P[w]=v
                D[w]=dv+1
                q.append(w)


    def allpaths0(self, source_node, sink_node, memo_dict = None):
        if memo_dict is None:
            # putting {}, or any other mutable object
            # as the default argument is wrong 
            memo_dict = dict()

        if source_node == sink_node: # Don't memoize trivial case
            return frozenset([(self.W[source_node],)])
        else:
            pair = (source_node, sink_node)
            if pair in memo_dict: # Is answer memoized already?
                return memo_dict[pair]
            else:
                result = set()
                for new_source in self.G[source_node]:
                    paths = self.allpaths(new_source, sink_node, memo_dict)
                    for path in paths:
                        #path = (source_node,) + path
                        path = (self.W[source_node],) + path
                        result.add(path)
                result = frozenset(result)
                # Memoize answer
                memo_dict[(source_node, sink_node)] = result
                return result

    def allpaths1(self, source_node, sink_node):
        if source_node == sink_node: # Handle trivial case
            return frozenset([(self.W[source_node],)])
        else:
            result = set()
            for new_source in self.G[source_node]:
                paths = self.allpaths(new_source, sink_node)
                for path in paths:
                    #path = (source_node,) + path
                    path = (self.W[source_node],) + path
                    result.add(path)
            result = frozenset(result)
            return result

    def allpaths(self, start, end):
        path  = []
        paths = []
        queue = [(start, end, path)]
        RES=None
        while queue:
            start, end, path = queue.pop()
            path = path + [start]
            if start == end:
                r="".join(map(lambda i:self.W[i],path))
                if RES==None or less(r,RES):
                    RES=r
                #paths.append(path)
                #paths.append(r)
            for node in set(self.G[start]).difference(path):
                queue.append((node, end, path))
        return RES

if __name__ == "__main__":
    N, M = map(int, input().split())
    G = Graph(N)
    G.setVWeights(input())
    for _ in range(M):
        u,v=map(lambda x:int(x)-1,input().split())
        G.add_edge(u,v)
    G.prepare()
    u,v=map(lambda x:int(x)-1,input().split())
    #res=G.DFS(u,v)
    RES=G.allpaths(u,v)

    res=None
    for r in RES:
        if res==None or less(r,res):
            res=r

    res="No way" if res==None else "".join(res)
    print(res)
