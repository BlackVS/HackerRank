#!/bin/python3

import sys

def solve(A, B):
    a=[]
    if A[0][0]!=B[0][0]: a.append(A[0][1]+B[0][1])
    if A[1][0]!=B[0][0]: a.append(A[1][1]+B[0][1])
    if A[0][0]!=B[1][0]: a.append(A[0][1]+B[1][1])
    return min(a)
    
N = int(input().strip())
A = sorted(enumerate(map(int, input().strip().split())),key=lambda x:x[1])
B = sorted(enumerate(map(int, input().strip().split())),key=lambda x:x[1])
print(solve(A,B))