#!/bin/python3

import sys

def get2lowest(X):
    resx0,resi0=X[0],0
    resx1,resi1=X[1],1
    for i,x in enumerate(X[2:]):
        if x>=resx0 and x>=resx1: 
            continue
        if resx1>resx0:
            resx1,resi1=x,i
        else:
            resx0,resi0=x,i
    return ( (resi0,resx0),(resi1,resx1))
    
def solve(A, B):
    a=[]
    if A[0][0]!=B[0][0]: a.append(A[0][1]+B[0][1])
    if A[1][0]!=B[0][0]: a.append(A[1][1]+B[0][1])
    if A[0][0]!=B[1][0]: a.append(A[0][1]+B[1][1])
    return min(a)
    
N = int(input().strip())
A = tuple(map(int, input().strip().split()))
B = tuple(map(int, input().strip().split()))
print(solve(get2lowest(A),get2lowest(B)))