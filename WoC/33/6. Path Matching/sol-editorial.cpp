#include <bits/stdc++.h>

using namespace std;

const int N = 100010, LN = 17;
const int P = 110;

vector<int> adj[N];
int parent[LN][N], depth[N];
char a[N], pat[P];

void dfs(int u = 0, int prev = -1, int d = 0) {
	depth[u] = d;
	parent[0][u] = prev;

	for (int v : adj[u]) {
		if (v == prev) continue;
		dfs(v, u, d + 1);
	}
}

int lca(int u, int v) {
	if (depth[u] < depth[v]) swap(u, v);

	int diff = depth[u] - depth[v];
	for (int i = 0; i < LN; i++) {
		if ((diff >> i) & 1) {
			u = parent[i][u];
		}
	}

	if (u == v) return u;

	for (int i = LN - 1; i >= 0; i--) {
		if (parent[i][u] != parent[i][v]) {
			u = parent[i][u];
			v = parent[i][v];
		}
	}

	return parent[0][u];
}

vector<int> pf(const string& p) {
	vector<int> pi(p.size() + 1);

	for (int i = 2; i <= p.size(); i++) {
		pi[i] = pi[i - 1];
		while (pi[i] > 0 && p[pi[i]] != p[i - 1]) {
			pi[i] = pi[pi[i]];
		}

		if (p[pi[i]] == p[i - 1]) {
			pi[i]++;
		}
	}

	return pi;
}

int S[P][26], T[P][26];
int F[2][N][P], K[2][N][P];
int G[2][N][P], L[2][N][P];

int X[N][2]; // current fsm state for each query
int W[N][2]; // current tree node for each query
int ans[N], tmp[N];

int main() {
	int n, q;
	assert(scanf("%d %d %s %s", &n, &q, a, pat) == 4);

	for (int i = 1; i < n; i++) {
		int u, v; 
		assert(scanf("%d %d", &u, &v) == 2);
		adj[u - 1].push_back(v - 1);
		adj[v - 1].push_back(u - 1);
	}

	dfs();

	for (int j = 1; j < LN; j++) {
		for (int i = 0; i < n; i++) {
			if (parent[j - 1][i] != -1) {
				parent[j][i] = parent[j - 1][parent[j - 1][i]];
			}
		}
	}

	string s(pat), t(pat);
	reverse(t.begin(), t.end());

	s += '\0'; t += '\0';

	auto pi_s = pf(s), pi_t = pf(t);

	for (int c = 0; c < 26; c++) {
		S[0][c] = s[0] == 'a' + c;
		T[0][c] = t[0] == 'a' + c;
	}

	for (int i = 1; i < s.size(); i++) {
		for (int c = 0; c < 26; c++) {
			if ('a' + c == s[i]) {
				S[i][c] = i + 1;
			} else {
				S[i][c] = S[pi_s[i]][c];
			}

			if ('a' + c == t[i]) {
				T[i][c] = i + 1;
			} else {
				T[i][c] = T[pi_t[i]][c];
			}
		}
	}

	vector<tuple<int, int, int>> order, queries;
	vector<int> special;

	order.reserve(LN * N);
	queries.reserve(N);
	special.reserve(N);

	for (int j = 0; j < q; j++) {
		int u, v;
		assert(scanf("%d %d", &u, &v) == 2);
		u--, v--;

		int p = lca(u, v);

		W[j][0] = u;
		W[j][1] = v;
		X[j][0] = X[j][1] = 0;

		queries.emplace_back(u, v, p);

		if (p != u && p != v) {
			special.push_back(j);
		}
	}

	for (int i = 0; i < LN; i++) {
		for (int j = 0; j < q; j++) {
			int u, v, p;
			tie(u, v, p) = queries[j];

			if (p == v) {
				int diff = depth[u] - depth[v] + 1;
				if ((diff >> i) & 1) {
					order.emplace_back(i, 0, j);
				}
			} else if (p == u) {
				int diff = depth[v] - depth[u] + 1;
				if ((diff >> i) & 1) {
					order.emplace_back(i, 1, j);
				}
			} else {
				int dx = depth[u] - depth[p] + 1;
				int dy = depth[v] - depth[p];

				if ((dx >> i) & 1) {
					order.emplace_back(i, 0, j);
				}

				if ((dy >> i) & 1) {
					order.emplace_back(i, 1, j);
				}
			}
		}
	}

	int at = 0;
	for (int j = 0; j < LN; j++) {
		int cur = j & 1;
		int pre = (j + 1) & 1;
		for (int i = 0; i < n; i++) {
			for (int k = 0; k < s.size(); k++) {
				F[cur][i][k] = K[cur][i][k] = 0;
				G[cur][i][k] = L[cur][i][k] = 0;

				if (j == 0) {
					F[0][i][k] = S[k][a[i] - 'a'];
					K[0][i][k] = F[0][i][k] + 1 == s.size();

					G[0][i][k] = T[k][a[i] - 'a'];
					L[0][i][k] = G[0][i][k] + 1 == t.size();
				} else {
					int p = parent[j - 1][i];
					if (p != -1) {
						int x = F[pre][i][k];
						F[cur][i][k] = F[pre][p][x];
						K[cur][i][k] = K[pre][i][k] + K[pre][p][x];

						int y = G[pre][i][k];
						G[cur][i][k] = G[pre][p][y];
						L[cur][i][k] = L[pre][i][k] + L[pre][p][y];
					}
				}
			}
		}

		while (at < order.size() && get<0>(order[at]) == j) {
			int wh, i;
			tie(ignore, wh, i) = order[at++];

			if (wh == 0) {
				int& u = W[i][0];
				int& x = X[i][0];

				ans[i] += K[cur][u][x];
				x = F[cur][u][x];
				u = parent[j][u];
			} else {
				int& v = W[i][1];
				int& y = X[i][1];

				ans[i] += L[cur][v][y];
				y = G[cur][v][y];
				v = parent[j][v];
			}
		}
	}

	for (auto j : special) {
		int x = X[j][0], y = X[j][1];
		bool mark[P] = {0};

		while (y > 0) {
			mark[y] = true;
			y = pi_t[y];
		}

		while (x > 0) {
			ans[j] += mark[s.size() - x - 1];
			x = pi_s[x];
		}
	}

	for (int j = 0; j < q; j++) {
		printf("%d\n", ans[j]);
	}

	return 0;
}