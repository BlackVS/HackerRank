#!/bin/python3

import sys
from collections import deque

s2i=lambda i:int(i)-1
N,Q = map(int,input().strip().split())

def countP(string, sub,shift):
    count = start = 0
    while True:
        start = string.find(sub, start)
        if start==-1:
            return count
        count+=1
        start+=shift

def sssp(root,G,targets):
    P=[None]*N
    q=deque([root])
    while q and targets:
        u=q.popleft()
        for v in G[u]:
            if P[v]!=None or v==root: continue
            P[v]=u
            targets.discard(v)
            if not targets:break            
            q.append(v)
    return P

def getshift(str):
    if len(str) == 0:
        return 0
    else:
        s = str[0]
        match = False
        end = 0
        i = 0

        for k in range(1,len(str)):
            c = str[k]

            if not match:
                if c == s:
                    i = 1
                    match = True
                    end = k
            else:
                if not c == str[i]:
                    match = False
                i += 1
        if match:
            return end
        else:
            return len(str)    
        
def solve():
    G =[[] for _ in range(N)]
    GQ=[set() for _ in range(N)]
    
    S=input().strip()
    PAT=input().strip()
    shiftP=getshift(PAT)
    PINV="".join(PAT[::-1])
    shiftPI=getshift(PINV)

    for _ in range(N-1):
        x, y = map(s2i, input().strip().split())
        G[x].append(y)
        G[y].append(x)    
        
    DUV=dict()
    RES=[0]*Q
    for q in range(Q):
        u,v=map(s2i,input().strip().split())
        u,v,f=min(u,v),max(u,v),u>v
        GQ[u].add(v)
        GQ[v].add(u)    
        if not u in DUV:
            DUV[u]=deque()
        DUV[u].append( (v,f,q) )
            
    for u in DUV.keys():
        P=sssp(u,G,GQ[u])
        for v,frev,i in DUV[u]:
            path=""
            while v!=None:
                path+=S[v]
                v=P[v]
            if frev:
                cnt=countP(path, PAT,  shiftP)
            else:
                cnt=countP(path, PINV, shiftPI)
            RES[i]=cnt

    for r in RES:
        print(r)

solve()
