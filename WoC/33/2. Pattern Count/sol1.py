#!/bin/python3

import sys
import re
prog = re.compile("(10+)(?=1)")

def solve(s):
    return len(prog.findall(s))

Q = int(input().strip())
for _ in range(Q):
    print(solve(input().strip()))