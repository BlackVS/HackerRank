#include <bits/stdc++.h>

using namespace std;

const int MAXN = 100000 + 500;
vector<vector<int> > f, sum;
int was[(1 << 10) + (1 << 10)], del[MAXN + 1];

vector<vector<int> > gen(int n, int m) {
    vector<vector<int> > f(n + 1);
    for(int i = 0; i <= n; i++)
        f[i].resize(m + 1);
    return f;
}

bool check(int a, int b, int c, int d) {
    int res = sum[c][d] - sum[a - 1][d] - sum[c][b - 1] + sum[a - 1][b - 1];
    return res > 1;
}

int calcBits(int x) {
    int res = 0;
    while(x > 0) {
        res += x & 1;
        x /= 2;
    }
    return res;
}

int main() {
    int n, m;
    cin >> n >> m;
    f = gen(n, m);
    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= m; j++) {
            char c;
            cin >> c;
            f[i][j] = c - '0';
        }
    }
    bool rev = false;
    if(n > m) {
        rev = true;
        vector<vector<int> > nw = gen(m, n);
        for(int i = 1; i <= n; i++) {
            for(int j = 1; j <= m; j++) {
                nw[j][i] = f[i][j];
            }
        }
        f = nw;
        swap(n, m);
    }
    sum = gen(n, m);
    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= m; j++) {
            sum[i][j] = f[i][j] > 0;
            sum[i][j] += sum[i - 1][j] + sum[i][j - 1] - sum[i - 1][j - 1];
        }
    }
    vector<vector<vector<int> > > precalc(m + 1);
    for(int i = 0; i <= m; i++) {
        precalc[i].resize(n + 1);
        for(int j = 0; j <= n; j++)
            precalc[i][j].resize(n + 1);
    }
    long long ans = 1, _a = 1, _b = 1, _c = 1, _d = 1, a, b, c, d;
    for(int i = 1; i <= m; i++) {
        for(int j = 1; j <= n; j++) {
            precalc[i][j][j] = (1 << f[j][i]);
            for(int k = j + 1; k <= n; k++) {
                precalc[i][j][k] = precalc[i][j][k - 1] ^ (1 << f[k][i]);
                if(calcBits(precalc[i][j][k]) <= 1 && check(j, i, k, i)) {
                    a = j;
                    b = i;
                    c = k;
                    d = i;
                    if(ans < (c - a + 1LL) * (d - b + 1LL)) {
                        ans = (c - a + 1LL) * (d - b + 1LL);
                        _a = a;
                        _b = b;
                        _c = c;
                        _d = d;
                    }
                }
            }
        }
    }
    for(int i = 1; i <= n; i++) {
        for(int j = i; j <= n; j++) {
            int mask = 0, nmask, curSz = 0;
            was[0] = 1;
            for(int k = 1; k <= m; k++) {
                mask ^= precalc[k][i][j];
                for(int l = 0; l < 10; l++) {
                    nmask = mask ^ (1 << l);
                    if(was[nmask] && check(i, was[nmask], j, k)) {
                        a = i;
                        b = was[nmask];
                        c = j;
                        d = k;
                        if (ans < (c - a + 1LL) * (d - b + 1LL)) {
                            ans = (c - a + 1LL) * (d - b + 1LL);
                            _a = a;
                            _b = b;
                            _c = c;
                            _d = d;
                        }
                    }
                }
                nmask = mask;
                if(was[nmask] && check(i, was[nmask], j, k)) {
                    a = i;
                    b = was[nmask];
                    c = j;
                    d = k;
                    if(ans < (c - a + 1LL) * (d - b + 1LL)) {
                        ans = (c - a + 1LL) * (d - b + 1LL);
                        _a = a;
                        _b = b;
                        _c = c;
                        _d = d;
                    }
                }
                if(!was[mask])
                    was[mask] = k + 1, del[curSz++] = mask;
            }
            for(int k = 0; k < curSz; k++)
                was[del[k]] = 0;
        }
    }
    cout << ans << "\n";
    if(!rev)
        cout << _a - 1 << " " << _b - 1 << " " << _c - 1 << " " << _d - 1 << "\n";
    else
        cout << _b - 1 << " " << _a - 1 << " " << _d - 1 << " " << _c - 1 << "\n";
    return 0;
}