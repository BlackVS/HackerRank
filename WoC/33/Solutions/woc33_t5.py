#!/bin/python3
import sys, copy
from builtins import sorted
from collections import *
sys.stdin = open('g:\hackerrank\woc\\33\woc33_t5_t1.in', 'r')
sys.stdout= open('g:\hackerrank\woc\\33\woc33_t5_t1.res', 'w+')

s2i=lambda i:int(i)-1
N, M, Q = map(int, input().strip().split())
#print(N,M,Q)

# vertices have nums 0..N-1
class Graph(object):
    def __init__(self,n):
        self.VN=n
        self.G = [[] for _ in range(n)]
        self.VDegree = [0]*n
        # DFS
        self.DFS_NR  = None
        self.DFS_PR  = None
        # BCT
        #low value for each vertex
        self.BCT_L = None
        #artic points
        self.BCT_C = None
        #Biconencted components
        self.BCT_B = None
        #save num of BCC for each Vertex
        self.BCT_GB = None
        #graph
        self.BCT_G = None
        #traverse order
        self.BCT_NR  = None
        #parents
        self.BCT_PR  = None
        #parent in sense of sub-graphs
        self.SB_P=None
        #root of current subgraph i.e. subgraph id
        self.SB_R=None
        #end of current subgraph
        self.SB_L=None

    def __str__(self,shortform=True):
        if shortform:
            return str(self.G)
        res = "vertices: "
        for k in self.G:
            res += str(k) + " "
        res += "\nedges:\n"

        for x in self.G:
            for y in self.G[x]:
                res += str(x) + " -> " + str(y) + "\n"
        return res

    #split into separate components
    #main idea - each subgraph store as sequence of vertecis in order of finding to speed up traverse
    #for each subgraph keep beginning and end of this sequence (root and last)
    #for each vertice keep it's parent in sequence
    #in such case joining of subgraphs means just corrrecting few values       
    def merge(self,v1,v2):
        N=self.VN
        if not self.SB_P:
            self.SB_P=[None]*N
            self.SB_R=list(range(N))
            self.SB_L=list(range(N))
        #vertex parent
        P=self.SB_P
        #subgraph root
        R=self.SB_R
        #subgraph end
        L=self.SB_L
        r1 = R[v1]
        r2 = R[v2]
        if r1 == r2:
            return
        r1, r2=min(r1,r2),max(r1,r2)
        P[r2]=L[r1]
        L[r1]=L[r2]
        v=L[r1]
        while R[v]==r2:
            R[v]=r1
            v=P[v]

    def isSameSubgraph(self,U,V):
        if not self.SB_R:
            return None
        return self.SB_R[U]==self.SB_R[V]

    def isSameBCC(self,U,V):
        if not self.BCT_GB:
            return None
        return self.BCT_GB[U]==self.BCT_GB[V]

    def add_edge(self,x,y,directed=False,fmerge=True):
        self.G[x].append(y)
        self.VDegree[x]+=1
        if not directed:
            self.G[y].append(x)
            self.VDegree[y]+=1
        if fmerge:
            self.merge(x,y)

    def DFS(self,root=0):
        self.DFS_NR  = [-1]*self.VN
        self.DFS_PR  = [None]*self.VN
        NR=self.DFS_NR
        P =self.DFS_PR
        GU=copy.deepcopy(self.G)
        i=0
        v=root
        NR[v]=i
        while True:
            while GU[v]:
                w=GU[v].pop()
                if w==P[v]: continue
                if NR[w]==-1:
                    P[w]=v
                    i+=1
                    NR[w]=i
                    v=w
            if v==root and not GU[v]:
                break;
            v=P[v]


    #generate block-tree
    #each block has it own number
    #each vertex belongs either to some block or cut-point
    #includes DFS
    def _initBCC(self):
        self.BCT_L = [-1]*self.VN
        self.BCT_C = set()
        self.BCT_B = []
        self.BCT_GB= [-1]*self.VN
        self.BCT_G = copy.deepcopy(self.G)
        #init DFS
        self.BCT_NR  = [-1]*self.VN
        self.BCT_PR  = [None]*self.VN

  
    def _subgraphBCC(self,root):
        #DEBUG
        #debugC=set()

        #init BCT
        L = self.BCT_L
        C = self.BCT_C
        B = self.BCT_B
        GB= self.BCT_GB
        NR =self.BCT_NR
        P  =self.BCT_PR
        GU =self.BCT_G
        #
        RootCnt = 0 
        stack=deque()
        #init loop
        i=0
        v=root
        L[v]=NR[v]=i
        stack.append(v)
        while True:
            #check neigbours
            while GU[v]:
                w=GU[v].pop()
                if w==P[v]:#not check parent
                    continue
                if NR[w]==-1:
                    P[w]=v
                    i+=1
                    L[w]=NR[w]=i
                    stack.append(w)
                    v=w
                else:
                    #next w is already visited
                    L[v]=min(L[v],NR[w])
            
            if v==root: 
                break

            pv=P[v]
            if pv!=root and L[v]<NR[pv]:
                #current BCC - just traverse back and propagate current L to ancestor
                L[pv]=min(L[pv],L[v])
            else:
                #store next art.point
                #root special case - must have 2 adgacent BCC to be art.point
                if pv!=root or RootCnt>0:
                    C.add(pv)
                    #debugC.add(pv)
                RootCnt+=pv==root
                #reveal and store next BCC
                b=[]
                p=None
                bID=len(B)
                while p!=v:
                    p=stack.pop()
                    b.append(p)
                    GB[p]=bID
                b.append(pv)
                GB[pv]=bID
                B.append(b)

            ######
            v=P[v]
        ##### DEBUG
        #print("Artic points, root is :",root)
        #for c in sorted(debugC):
        #    print(c+1)
        return (B,C)

    def BCC(self):
        self._initBCC()
        for v in range(self.VN):
            if self.BCT_NR[v]==-1:
                self._subgraphBCC(v)
        return (self.BCT_B,self.BCT_C)

def check(U,V,G):
    if G.isSameBCC(U,V):
        return "YES"
    return "-"

def solve():
    G=Graph(N)
    for _ in range(M):
        x, y = map(s2i, input().strip().split())
        G.add_edge(x,y)
        #print(x,y)
    #print(G)
    #print(G.VDegree)
    #print(G.DFS())
    (B,C)=G.BCC()
    for _ in range(Q):
        U, V, W = map(s2i, input().strip().split())
        if not G.isSameSubgraph(U,W) or not G.isSameSubgraph(V,W):
            print("NO")
            continue
        print(check(U,V,G))

solve()
