#!/bin/python3

import sys


N,K,M = map(int, input().strip().split())
#parent
P=[None]*N
#root
R=list(range(N))
#last
L=list(range(N))

def merge(v1,v2):
    #print(v1,v2)
    #print("R: ", R)
    #print("P: ", P)
    #print("L: ", L)
    r1 = R[v1]
    r2 = R[v2]
    if r1 == r2:
        #print("in same tree")
        return
    r1, r2=min(r1,r2),max(r1,r2)
    #print(r1,r2)
    #attach g2 to g1
    P[r2]=L[r1]
    L[r1]=L[r2]
    v=L[r1]
    #print(v,R[v],P[v],r2)
    while R[v]==r2:
        #print(v)
        R[v]=r1
        v=P[v]
    #print("===>")
    #print("R: ", R)
    #print("P: ", P)
    #print("L: ", L)

def lps(A):
    n = len(A)
 
    L = [[0]*n for _ in range(n)]
 
    for i in range(n):
        L[i][i] = 1
 
    for cl in range(2, n+1):
        for i in range(n-cl+1):
            j = i+cl-1
            if A[i] == A[j] and cl == 2:
                L[i][j] = 2
            elif A[i] == A[j]:
                L[i][j] = L[i+1][j-1] + 2
            else:
                L[i][j] = max(L[i][j-1], L[i+1][j]);
    return L[0][n-1]    

for _ in range(K):
    x,y = map(int, input().strip().split())
    merge(x-1,y-1)

    

    
A = list(map(lambda x:R[int(x)-1], input().strip().split()))
#print(A)
print(lps(A))