#!/bin/python3

import sys
from collections import defaultdict
from math import gcd

class Graph:
 
    def __init__(self,vertices):
        self.V= vertices
        self.graph = []
         
    def addEdge(self,u,v,a,b):
        self.graph.append([u,v,a,b])
 
    def find(self, parent, i):
        if parent[i] == i:
            return i
        return self.find(parent, parent[i])
 
    def union(self, parent, rank, x, y):
        xroot = self.find(parent, x)
        yroot = self.find(parent, y)
 
        if rank[xroot] < rank[yroot]:
            parent[xroot] = yroot
        elif rank[xroot] > rank[yroot]:
            parent[yroot] = xroot
        else :
            parent[yroot] = xroot
            rank[xroot] += 1
 
    
    def KruskalMST(self):
        res_n, res_d=0,0
        i = e = 0 
        self.graph =  sorted(self.graph,key=lambda item:(item[3]*10100)/item[2]+item[3]*(1,-1)[item[2]>item[3]])

        parent = list(range(self.V))
        rank = [0]*self.V
    
        while e < self.V -1 :
            u,v,a,b =  self.graph[i]
            x = self.find(parent, u)
            y = self.find(parent ,v)
            if x != y:
                e+=1  
                res_n+=a
                res_d+=b
                self.union(parent, rank, x, y)          
            i+=1
        cd=gcd(res_n,res_d)
        if cd>0:
            res_n//=cd
            res_d//=cd
        return (res_n,res_d)

N,M = map(int, input().strip().split())
g = Graph(N)
for _ in range(M):
    u,v,a,b = map(int, input().strip().split())
    if u==v: continue
    g.addEdge(u, v, a, b)
print("{}/{}".format(*g.KruskalMST()))