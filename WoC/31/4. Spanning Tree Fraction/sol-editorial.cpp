#include <bits/stdc++.h>
using namespace std;
#define sz(x) ((int) (x).size())
#define forn(i,n) for (int i = 0; i < int(n); ++i)
typedef long long ll;
typedef long long i64;
typedef double ld;
const int inf = int(1e9) + int(1e5);
const ll infl = ll(2e18) + ll(1e10);

const int maxn = 200200;
int u[maxn];
int v[maxn];
int a[maxn];
int b[maxn];
ld w[maxn];
int p[maxn];
int n, m;

int col[maxn];
int rk[maxn];

int get(int u) {
    if (col[u] == u)
        return u;
    return col[u] = get(col[u]);
}

bool join(int u, int v) {
    u = get(u), v = get(v);
    if (u == v)
        return false;
    if (rk[u] > rk[v])
        swap(u, v);
    rk[v] += rk[u];
    col[u] = v;
    return true;
}

int A, B;
bool check(ld C) {
    forn (i, m)
        w[i] = a[i] - b[i] * C;
    sort(p, p + m, [](int i, int j) {
            return w[i] > w[j];
            });
    iota(col, col + n, 0);
    fill(rk, rk + n, 1);
    A = 0, B = 0;
    forn (i, m) {
        int id = p[i];
        if (join(u[id], v[id]))
            A += a[id], B += b[id];
    }
    return A >= B * C;
}

int gcd(int a, int b) {
    while (a && b) {
        if (a >= b)
            a %= b;
        else
            b %= a;
    }
    return a + b;
}

int main() {
    #ifdef LOCAL
    assert(freopen("test.in", "r", stdin));
    #endif
    cin >> n >> m;
    forn (i, m)
        scanf("%d%d%d%d", u + i, v + i, a + i, b + i);

    iota(p, p + m, 0);
    ld L = 0, R = 1e9;
    forn (iter, 100) {
        ld C = (L + R) / 2;
        if (check(C))
            L = C;
        else
            R = C;
    }
    assert(check(L));
    int g = gcd(A, B);
    cout << A / g << '/' << B / g << '\n';
}