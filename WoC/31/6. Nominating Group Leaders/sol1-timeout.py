#!/bin/python3

import sys
from collections import*

def solve():
    N = int(input().strip())
    V = tuple(map(int, input().strip().split()))
    G = int(input().strip())
    for _ in range(G):
        L,R,X = map(int,input().strip().split())
        print(min(map(lambda y:y[0],filter(lambda x:x[1]==X,Counter(V[L:R+1]).items())),default=-1))
    
T = int(input().strip())
for _ in range(T):
    solve()
