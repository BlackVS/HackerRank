#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <cstring>
#include <deque>
#include <stack>
#include <stdio.h>
#include <map>
#include <set>
#include <time.h>
#include <string>
#include <fstream>
#include <queue>
#include <bitset>
#include <cstdlib>
#include <assert.h>
#include <list>
#include <unordered_map>
#define X first
#define Y second
#define mp make_pair
#define pb push_back
#define pdd pair<double,double>
#define pii pair<ll,ll>
#define PI 3.14159265358979323846
#define MOD 1000000007
#define MOD2 1000000009
#define INF ((ll)1e+18)
#define x1 fldgjdflgjhrthrl
#define x2 fldgjdflgrtyrtyjl
#define y1 fldggfhfghjdflgjl
#define y2 ffgfldgjdflgjl
#define SQ 317
#define MAG 33554431
#define RED 0
#define BLUE 1
#define ALP 26
typedef int ll;
typedef long double ld;
using namespace std;
ll i,j,k,l,m,r,x,y,K,tot,sz,cur,sum,n,c, maxlvl,q,z,N;
ll a[100500], ans[100500], freq[100500];
ll Dec[405][100500];
vector<ll> g[100500];
pair<pii,pii> queries[100500];
bool cmp(pair<pii,pii> x,pair<pii,pii> y)
{
    if (x.X.X/SQ > y.X.X/SQ)
        return false;
    if (x.X.X/SQ < y.X.X/SQ)
        return true;
    return (x.X.Y < y.X.Y);
}
int main() {
	//freopen("input.txt","r",stdin);
 	//freopen("input.txt","w",stdout);
    ll tests;
    ll sum_n = 0, sum_q = 0;
    cin >> tests;
    assert(tests >= 1 && tests <= 5);
    while (tests--)
    {
        cin >> n;
        assert(n >= 1 && n <= 100000);
        sum_n += n;
        for (i = 0; i <= n; i++)
        {
            freq[i] = 0;
        }
        for (i = 1; i <= n; i++)
        {
            scanf("%d",&a[i]);
            a[i]++;
            assert(a[i] >= 1 && a[i] <= n);
        }
        for (i = 0; i <= n/SQ+1; i++)
            for (j = 0; j <= n; j++)
                Dec[i][j] = 0;
        cin >> q;
        assert(q >= 1 && q <= 100000);
        sum_q += q;
        for (i = 0; i < q; i++)
        {
            scanf("%d %d %d",&x,&y,&z);
            x++;y++;
            assert(1 <= x && x <= y && y <= n && 1 <= z && z  <= n);
            queries[i] = mp(mp(x,y), mp(z,i));
        }
        sort(queries, queries+q, cmp);
        ll L = queries[0].X.X, R = queries[0].X.Y;
        z = queries[0].Y.X;
        ll num = queries[0].Y.Y;
        for (j = L; j <= R; j++)
        {
            Dec[a[j]/SQ][freq[a[j]]]--;
            freq[a[j]]++;
            Dec[a[j]/SQ][freq[a[j]]]++;
        }
        ans[num] = MOD;
        for (int ii = 0; ii <= n/SQ+1; ii++)
            if (Dec[ii][z] > 0)
            {
                for (j = ii*SQ; j < ii*SQ+SQ; j++)
                    if (freq[j] == z)
                    {
                        ans[num] = j;
                        break;
                    }
                break;
            }
        for (i = 1; i < q; i++)
        {
            ll l = queries[i].X.X, r = queries[i].X.Y;
            while (L < l)
            {
                Dec[a[L]/SQ][freq[a[L]]]--;
                freq[a[L]]--;
                Dec[a[L]/SQ][freq[a[L]]]++;
                L++;
            }
            while (R > r)
            {
                Dec[a[R]/SQ][freq[a[R]]]--;
                freq[a[R]]--;
                Dec[a[R]/SQ][freq[a[R]]]++;
                R--;
            }
            while (L > l)
            {
                L--;
                Dec[a[L]/SQ][freq[a[L]]]--;
                freq[a[L]]++;
                Dec[a[L]/SQ][freq[a[L]]]++;
            }
            while (R < r)
            {
                R++;
                Dec[a[R]/SQ][freq[a[R]]]--;
                freq[a[R]]++;
                Dec[a[R]/SQ][freq[a[R]]]++;
            }
            z = queries[i].Y.X;
            ll num = queries[i].Y.Y;
            ans[num] = MOD;
            for (int ii = 0; ii <= n/SQ+1; ii++)
                if (Dec[ii][z] > 0)
                {
                    for (j = ii*SQ; j < ii*SQ+SQ; j++)
                        if (freq[j] == z)
                        {
                            ans[num] = j;
                            break;
                        }
                    break;
                }
        }
        for (i = 0; i < q; i++)
            printf("%d\n",(ans[i]==MOD?-1:ans[i]-1));
    }
    assert(sum_n >= 1 && sum_n <= 300000 && sum_q >= 1 && sum_q <= 300000);
    return 0;
}