#include <bits/stdc++.h>

using namespace std;
const int NMAX = 100000;
int Tree[NMAX * 2];
int F[NMAX];
int main() {
	int T;
	cin >> T;
	for (int t=0; t<T; t++) {
		int N, G, v;
		cin >> N;
		for (int n = 0; n < N; n++) {
			cin >> Tree[n];
		}
		cin >> G;
		for (int g = 0; g < G; g++) {
			int L, R, X;
			cin >> L >> R >> X;
			memset(F, 0, sizeof(int)*N);
			for (int i = L; i <= R; i++)
				F[Tree[i]]++;
			int res = -1;
			for (int j = 0; j < N && res==-1; j++)
				if (F[j] == X)
					res = j;
			cout << res << endl;
		}
	}
	return 0;
}