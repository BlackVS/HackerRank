#include<bits/stdc++.h>
using namespace std;
#define D(x)        cout << #x " = " << (x) << endl
#define MAX         100000

const int bucksz = 320;
set<int> candidates[MAX + 5];

struct query{
    int l, r, x, id;

    query(){}
    query(int _l, int _r, int _x, int _id){
        l = _l, r = _r, x = _x, id = _id;
    }
};

bool operator < (const query &u, const query &v){
    if(u.l / bucksz == v.l / bucksz) return u.r < v.r;
    return u.l < v.l;
}

struct solver{
    int n, q;
    vector<int> numbers, L, R, X;

    solver(){}
    void takeInput(){
        scanf("%d", &n);
        numbers.resize(n + 5);
        for(int i = 1; i <= n; i++)
        {
			scanf("%d", &numbers[i]);
			numbers[i]++; 
		}

        scanf("%d", &q);
        L.resize(q + 5);
        R.resize(q + 5);
        X.resize(q + 5);
        for(int i = 1; i <= q; i++)
        {
			scanf("%d %d %d", &L[i], &R[i], &X[i]);
			L[i]++;
			R[i]++;
		}
    }

    vector<int> brute(){
        vector<int> frequency, answers;
        frequency.resize(MAX+5);
        answers.resize(q + 5);

        for(int i = 1; i <= q; i++)
        {
            for(int k = L[i]; k <= R[i]; k++)
                frequency[numbers[k]]++;
            int pos = 1;
            while(pos <= MAX && frequency[pos] != X[i]) pos++;

            answers[i] = (pos <= MAX) ? pos : -1;

            for(int k = L[i]; k <= R[i]; k++)
                frequency[numbers[k]] = 0;
        }

        return answers;
    }

    vector<int> O_NrtN(){
        vector<int> answers;
        answers.resize(q + 5, -1);

        vector<query> Q;
        Q.resize(q + 5);
        for(int i = 1; i <= q; i++)
            Q[i] = query(L[i], R[i], X[i], i);
        sort(Q.begin() + 1, Q.begin() + q + 1);

        int ttl_buk = MAX / bucksz + 1;
        vector<int> frequency, bucket_info[ ttl_buk + 5];
        frequency.resize(MAX + 5);
        for(int i = 1; i <= ttl_buk; i++)
            bucket_info[i].resize(MAX+5);

        int l = 0, r = 0;

        for(int i = 1; i <= q; i++)
        {
            while(l > Q[i].l){
                l--;
                int prv = frequency[numbers[l]];
                int buck_no = numbers[l] / bucksz + 1;
                bucket_info[buck_no][prv]--;
                bucket_info[buck_no][prv + 1]++;
                frequency[numbers[l]]++;
            }

            while(r < Q[i].r){
                r++;
                if(r){
                    int prv = frequency[numbers[r]];
                    int buck_no = numbers[r] / bucksz + 1;
                    bucket_info[buck_no][prv]--;
                    bucket_info[buck_no][prv+1]++;
                    frequency[numbers[r]]++;
                }
            }

            while(l < Q[i].l){
                if(l){
                    int prv = frequency[numbers[l]];
                    int buck_no = numbers[l] / bucksz + 1;
                    bucket_info[buck_no][prv]--;
                    bucket_info[buck_no][prv-1]++;
                    frequency[numbers[l]]--;
                }
                l++;
            }

            while(r > Q[i].r){
                int prv = frequency[numbers[r]];
                int buck_no = numbers[r] / bucksz + 1;
                bucket_info[buck_no][prv]--;
                bucket_info[buck_no][prv-1]++;
                frequency[numbers[r]]--;
                r--;
            }

            for(int b = 1; b <= ttl_buk; b++)
                if(bucket_info[b][Q[i].x]){
                    int st = max(1, (b - 1) * bucksz);
                    while(frequency[st] != Q[i].x) st++;
                    answers[Q[i].id] = st;
                    break;
                }
        }

        return answers;
    }

    vector<int> O_NrtNLgN(){
        vector<int> answers;
        answers.resize(q + 5, -1);

        vector<query> Q;
        Q.resize(q + 5);
        for(int i = 1; i <= q; i++)
            Q[i] = query(L[i], R[i], X[i], i);
        sort(Q.begin() + 1, Q.begin() + q + 1);

        vector<int> frequency;
        frequency.resize(MAX + 5);
        for(int i = 1; i <= MAX; i++)
            candidates[i].clear();


        int l = 0, r = 0;

        for(int i = 1; i <= q; i++)
        {
            while(l > Q[i].l){
                l--;
                int prv = frequency[numbers[l]];
                if(prv) candidates[prv].erase(numbers[l]);
                candidates[prv + 1].insert(numbers[l]);
                frequency[numbers[l]]++;
            }

            while(r < Q[i].r){
                r++;
                if(r){
                    int prv = frequency[numbers[r]];
                    if(prv) candidates[prv].erase(numbers[r]);
                    candidates[prv + 1].insert(numbers[r]);
                    frequency[numbers[r]]++;
                }
            }

            while(l < Q[i].l){
                if(l){
                    int prv = frequency[numbers[l]];
                    if(prv) candidates[prv].erase(numbers[l]);
                    if(prv - 1) candidates[prv-1].insert(numbers[l]);
                    frequency[numbers[l]]--;
                }
                l++;
            }

            while(r > Q[i].r){
                int prv = frequency[numbers[r]];
                candidates[prv].erase(numbers[r]);
                if(prv - 1) candidates[prv - 1].insert(numbers[r]);
                frequency[numbers[r]]--;
                r--;
            }

            if(candidates[Q[i].x].empty()) answers[Q[i].id] = -1;
            else answers[Q[i].id] = *(candidates[Q[i].x].begin());
        }

        return answers;
    }

    vector<int> O_shouldNotPass(){
        vector<int> answers;
        answers.resize(q + 5, -1);

        vector<query> Q;
        Q.resize(q + 5);
        for(int i = 1; i <= q; i++)
            Q[i] = query(L[i], R[i], X[i], i);
        sort(Q.begin() + 1, Q.begin() + q + 1);

        vector<int> frequency;
        frequency.resize(MAX + 5);
        int mv = *max_element(numbers.begin() + 1, numbers.begin() + n + 1);

        int l = 0, r = 0;
        for(int i = 1; i <= q; i++)
        {
            while(l > Q[i].l){
                l--;
                int prv = frequency[numbers[l]];
                frequency[numbers[l]]++;
            }

            while(r < Q[i].r){
                r++;
                if(r){
                    int prv = frequency[numbers[r]];
                    frequency[numbers[r]]++;
                }
            }

            while(l < Q[i].l){
                if(l){
                    int prv = frequency[numbers[l]];
                    frequency[numbers[l]]--;
                }
                l++;
            }

            while(r > Q[i].r){
                int prv = frequency[numbers[r]];
                frequency[numbers[r]]--;
                r--;
            }

            int v = 1;
            while(v <= mv && frequency[v] != Q[i].x) v++;
            if(v > mv) answers[Q[i].id] = -1;
            else answers[Q[i].id] = v;
        }

        return answers;
    }

    void checker_O_NrtN(){
        vector<int> u = O_NrtN();
        vector<int> v = brute();
        int ret = 0;
        for(int i = 1; i <= q; i++)
            ret += (u[i] != v[i]);

        printf("Missmatches: %d\n", ret);
    }

    void checker_O_NrtNLgN(){
        vector<int> u = O_NrtN();
        vector<int> v = O_NrtNLgN();
        int ret = 0;
        for(int i = 1; i <= q; i++)
            ret += (u[i] != v[i]);

        printf("Missmatches: %d\n", ret);
    }

    void checker_O_shouldNotPass(){
        vector<int> u = O_NrtN();
        vector<int> v = O_shouldNotPass();
        int ret = 0;
        for(int i = 1; i <= q; i++)
            ret += (u[i] != v[i]);

        printf("Missmatches: %d\n", ret);
    }

    void print(vector<int> answers){
        for(int i = 1; i <= q; i++)
            if(answers[i] == -1) printf("%d\n", answers[i]);
			else printf("%d\n", answers[i] - 1);
    }
}S;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    int t;
    scanf("%d", &t);
    while(t--)
    {
        S.takeInput();
        S.print(S.O_NrtN());
    }

    return 0;
}
