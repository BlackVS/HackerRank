#include <bits/stdc++.h>
using namespace std;
#define sz(x) ((int) (x).size())
#define forn(i,n) for (int i = 0; i < int(n); ++i)
typedef long long ll;
typedef long long i64;
typedef long double ld;
const int inf = int(1e9) + int(1e5);
const ll infl = ll(2e18) + ll(1e10);

const int maxn = 100100;
int r[maxn];

int main() {
    #ifdef LOCAL
    assert(freopen("test.in", "r", stdin));
    #endif
    int n, k;
    cin >> n >> k;
    ld s = 0, t = 0;
    forn (i, n) {
        cin >> r[i];
        s += ld(r[i]) * ld(r[i]);
        t += r[i];
    }
    t = t * t - s;
    ld prob = 0;
    ld all = 1;
    forn (iter, k) {
        ld cur = 2 / ld(n) / ld(n - 1);
        prob += cur * all;
        all *= (1 - cur);
        --n;
    }

    cout << fixed;
    cout.precision(10);
    cout << (prob * t + s) * acosl(-1) << '\n';
}