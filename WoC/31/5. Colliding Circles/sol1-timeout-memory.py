#!/bin/python3

import sys
from itertools import*
import math

def nextjoin(X):
    if len(X)==1: return X
    res=[]
    for i in combinations(range(len(X)),2):
        res.append([X[i[0]]|X[i[1]]]+[X[j] for j in range(len(X)) if not j in i])
    return res

def cycle(vars):
    res=[]
    for v in vars:
        res+=nextjoin(v)
    return res

N,K = map(int, input().strip().split())
if K>=N-1:
    print(math.pi*sum(map(int, input().strip().split()))**2)
else:
    vars=[[ {i} for i in map(int, input().strip().split())]]
    for c in range(K):
        vars=cycle(vars)
    cnt=len(vars)
    res=0
    for x in chain(*vars):
        res+=sum(x)**2
    print(res*math.pi/cnt)
