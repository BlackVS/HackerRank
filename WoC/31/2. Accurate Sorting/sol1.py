#!/bin/python3

import sys

q = int(input().strip())
for _ in range(q):
    N = int(input().strip())
    A = map(int, input().strip().split())
    res=True
    for i,a in enumerate(A):
        if abs(i-a)>1:
            res=False
            break
    print(("No","Yes")[res])
