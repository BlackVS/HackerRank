#!/bin/python3

import sys

VOWELS="aeiouy"

w = input().strip()
# Print 'Yes' if the word is beautiful or 'No' if it is not.

res=True
for i in range(1,len(w)):
    if w[i-1]==w[i] or w[i-1] in VOWELS and w[i] in VOWELS:
        res=False
        break
print(("No","Yes")[res])