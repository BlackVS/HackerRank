#include <bits/stdc++.h>
using namespace std;
#define sz(x) ((int) (x).size())
#define forn(i,n) for (int i = 0; i < int(n); ++i)
#define all(x) x.begin(), x.end()
typedef pair<int, int> pii;
typedef long long ll;

struct DSU {
    vector<int> col, rk;

    DSU(int n): col(n), rk(n, 1) {
        iota(all(col), 0);
    }

    int get(int u) {
        if (u == col[u])
            return u;
        return col[u] = get(col[u]);
    }

    void join(int u, int v) {
        u = get(u), v = get(v);
        if (u == v)
            return;
        if (rk[u] > rk[v])
            swap(u, v);
        rk[v] += rk[u];
        col[u] = v;
    }
};

struct Event {
    int x, type, id;

    bool operator<(const Event &e) const {
        return x < e.x || (x == e.x && type < e.type);
    }
};

struct Tree {
    int base;
    vector<int> cnt;
    vector<int> cls;
    DSU dsu;

    Tree(int ys, int n): dsu(n) {
        base = 1;
        while (base < ys)
            base *= 2;
        cnt.resize(2 * base, 0);
        cls.resize(2 * base, -1);
    }

    void _put(int v, int val) {
        if (val == -1 || cnt[v] == 0)
            return;
        if (cls[v] != -1) {
            //cerr << "join " << val << ' ' << cls[v] << '\n';
            dsu.join(val, cls[v]);
        } else {
            cls[v] = val;
        }
    }

    void push(int v) {
        if (v < base) {
            _put(v * 2, cls[v]);
            _put(v * 2 + 1, cls[v]);
        }
        cls[v] = -1;
    }

    ll put(int l, int r, int val, int v = 1, int cl = 0, int cr = 1e9) {
        cr = min(cr, base);
        if (l <= cl && cr <= r) {
            _put(v, val);
            return cnt[v];
        }
        if (r <= cl || cr <= l)
            return 0;
        push(v);
        int cc = (cl + cr) / 2;
        ll res = 0;
        res += put(l, r, val, v * 2, cl, cc);
        res += put(l, r, val, v * 2 + 1, cc, cr);
        return res;
    }

    void ch(int pos, int val, int v = 1, int cl = 0, int cr = 1e9) {
        cr = min(cr, base);
        push(v);
        if (cl + 1 == cr) {
            if (val == -1)
                cnt[v] = 0;
            else
                cnt[v] = 1;
            cls[v] = val;
            return;
        }
        int cc = (cl + cr) / 2;
        if (pos < cc)
            ch(pos, val, v * 2, cl, cc);
        else
            ch(pos, val, v * 2 + 1, cc, cr);
        cnt[v] = cnt[v * 2] + cnt[v * 2 + 1];
    }
};

vector<pii> segUnion(vector<pii> seg) {
    vector<pii> ev;
    for (auto s: seg) {
        ev.emplace_back(s.first, -1);
        ev.emplace_back(s.second, 1);
    }
    sort(all(ev));
    vector<pii> res;
    int bal = 0, l = 0;
    for (auto e: ev) {
        bal += e.second;
        if (e.second == -1 && bal == -1)
            l = e.first;
        if (e.second == 1 && bal == 0)
            res.emplace_back(l, e.first);
    }
    return res;
}

const int maxn = 200100;
vector<pii> byx[maxn];
vector<pii> byy[maxn];

void solve() {
    int n;
    cin >> n;
    vector<int> x1(n), y1(n), x2(n), y2(n);
    vector<int> xs, ys;
    forn (i, n) {
        scanf("%d%d%d%d", &x1[i], &y1[i], &x2[i], &y2[i]);
        if (x1[i] > x2[i])
            swap(x1[i], x2[i]);
        if (y1[i] > y2[i])
            swap(y1[i], y2[i]);
        xs.push_back(x1[i]);
        xs.push_back(x2[i]);
        ys.push_back(y1[i]);
        ys.push_back(y2[i]);
    }
    sort(all(xs));
    xs.erase(unique(all(xs)), xs.end());
    sort(all(ys));
    ys.erase(unique(all(ys)), ys.end());
    
    //Compressing semgents
    forn (i, sz(xs))
        byx[i].clear();
    forn (i, sz(ys))
        byy[i].clear();
    forn (i, n) {
        x1[i] = lower_bound(all(xs), x1[i]) - xs.begin();
        y1[i] = lower_bound(all(ys), y1[i]) - ys.begin();
        x2[i] = lower_bound(all(xs), x2[i]) - xs.begin();
        y2[i] = lower_bound(all(ys), y2[i]) - ys.begin();
        if (x1[i] == x2[i])
            byx[x1[i]].emplace_back(y1[i], y2[i]);
        else if (y1[i] == y2[i])
            byy[y1[i]].emplace_back(x1[i], x2[i]);
        else
            assert(false);
    }
    x1.clear();
    y1.clear();
    x2.clear();
    y2.clear();
    forn (i, sz(xs)) {
        auto v = segUnion(byx[i]);
        for (auto s: v) {
            x1.push_back(i);
            x2.push_back(i);
            y1.push_back(s.first);
            y2.push_back(s.second);
        }
    }
    forn (i, sz(ys)) {
        auto v = segUnion(byy[i]);
        for (auto s: v) {
            x1.push_back(s.first);
            x2.push_back(s.second);
            y1.push_back(i);
            y2.push_back(i);
        }
    }
    n = sz(x1);

    vector<Event> ev;
    forn (i, n) {
        if (x1[i] == x2[i]) {
            ev.push_back(Event{x1[i], 0, i});
        } else if (y1[i] == y2[i]) {
            ev.push_back(Event{x1[i], -1, i});
            ev.push_back(Event{x2[i], 1, i});
        } else
            assert(false);
    }
    //forn (i, n)
        //cerr << x1[i] << ' ' << y1[i] << ' ' << x2[i] << ' ' << y2[i] << '\n';
    sort(all(ev));
    ll ints = 0;
    Tree t(sz(ys), n);
    for (auto e: ev) {
        int id = e.id;
        if (e.type == 0) {
            ints += t.put(y1[id], y2[id] + 1, id);
        } else if (e.type == -1) {
            t.ch(y1[id], id);
        } else if (e.type == 1) {
            t.ch(y1[id], -1);
        } else
            assert(false);
    }
    int active = n, comps = 0;
    forn (i, n)
        if (t.dsu.col[i] == i) {
            if (t.dsu.rk[i] == 1)
                --active;
            else
                ++comps;
        }
    //cerr << ints << ' ' << active << ' ' << comps << '\n';
    ll V = ints;
    ll E = ints * 2 - active;
    ll C = 1 + comps;
    ll res = C + E - V;
    cout << res << '\n';
}

int main() {
    #ifdef LOCAL
    //assert(freopen("test.in", "r", stdin));
    #endif
    int tn;
    cin >> tn;
    forn (i, tn)
        solve();
}