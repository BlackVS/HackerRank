#include <bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define forn(i, n) for (int i = 0; i < (int)(n); ++i)
typedef long long LL;
typedef pair<int, int> PII;

const int INF = int(1e9);

int tt, n;
int x[100000], y[100000], xx[100000], yy[100000];

void readTest() {
    scanf("%d", &n);
    forn(i, n) {
        scanf("%d%d%d%d", x + i, y + i, xx + i, yy + i);
    }
}

vector<int> xs, ys;

void compress() {
    forn(i, n) {
        if (x[i] > xx[i]) {
            swap(x[i], xx[i]);
        }
        if (y[i] > yy[i]) {
            swap(y[i], yy[i]);
        }
    }
    xs.clear();
    ys.clear();
    forn(i, n) {
        xs.pb(x[i]);
        xs.pb(xx[i]);
        ys.pb(y[i]);
        ys.pb(yy[i]);
    }
    sort(xs.begin(), xs.end());
    xs.erase(unique(xs.begin(), xs.end()), xs.end());
    sort(ys.begin(), ys.end());
    ys.erase(unique(ys.begin(), ys.end()), ys.end());
    forn(i, n) {
        x[i] = lower_bound(xs.begin(), xs.end(), x[i]) - xs.begin();
        y[i] = lower_bound(ys.begin(), ys.end(), y[i]) - ys.begin();
        xx[i] = lower_bound(xs.begin(), xs.end(), xx[i]) - xs.begin();
        yy[i] = lower_bound(ys.begin(), ys.end(), yy[i]) - ys.begin();
    }
}

vector<PII> vx[200000], vy[200000];
pair<int, PII> px[100000], py[100000];
int cx, cy;

void mergeSegments() {
    forn(i, 2 * n) {
        vx[i].clear();
        vy[i].clear();
    }
    forn(i, n) if (x[i] == xx[i]) {
        vx[x[i]].pb(mp(y[i], -1));
        vx[x[i]].pb(mp(yy[i], 1));
    } else {
        vy[y[i]].pb(mp(x[i], -1));
        vy[y[i]].pb(mp(xx[i], 1));
    }
    cx = 0;
    forn(i, 2 * n) {
        sort(vx[i].begin(), vx[i].end());
        int bal = 0, st = -1;
        for (PII p : vx[i]) {
            bal += p.second;
            if (p.second == -1 && bal == -1) {
                st = p.first;
            }
            if (p.second == 1 && bal == 0) {
                px[cx++] = mp(i, mp(st, p.first));
            }
        }
    }
    cy = 0;
    forn(i, 2 * n) {
        sort(vy[i].begin(), vy[i].end());
        int bal = 0, st = -1;
        for (PII p : vy[i]) {
            bal += p.second;
            if (p.second == -1 && bal == -1) {
                st = p.first;
            }
            if (p.second == 1 && bal == 0) {
                py[cy++] = mp(i, mp(st, p.first));
            }
        }
    }
    sort(px, px + cx);
    sort(py, py + cy);
}

int par[100000], ra[100000];

int dsuParent(int v) {
    if (par[v] == v) return v;
    return par[v] = dsuParent(par[v]);
}

int dsuMerge(int u, int v) {
    u = dsuParent(u);
    v = dsuParent(v);
    if (u == v) {
        return 0;
    }
    if (ra[u] < ra[v]) {
        swap(u, v);
    }
    par[v] = u;
    ra[u] += ra[v];
    return 1;
}

void dsuInit() {
    forn(i, cx + cy) {
        par[i] = i;
        ra[i] = 1;
    }
}

struct Ev {
    int t, ty, ind;

    Ev(int t, int ty, int ind) : t(t), ty(ty), ind(ind) {}

    inline bool operator < (const Ev &rhs) const {
        if (t != rhs.t) return t < rhs.t;
        return ty < rhs.ty;
    }
};

vector<Ev> ev;
set<PII> se, se2;

int fen[200000], fensz;

void fenInc(int pos) {
    for (; pos < fensz; pos |= pos + 1) {
        ++fen[pos];
    }
}

void fenDec(int pos) {
    for (; pos < fensz; pos |= pos + 1) {
        --fen[pos];
    }
}

int fenGet(int pos) {
    int res = 0;
    for (; pos >= 0; pos = (pos & (pos + 1)) - 1) {
        res += fen[pos];
    }
    return res;
}

LL solve() {
    compress();
    mergeSegments();
    dsuInit();
    fensz = (int)xs.size();
    forn(i, fensz) {
        fen[i] = 0;
    }
    ev.clear();
    LL ans = 1;
    forn(i, cx) {
        ev.pb(Ev(px[i].second.first, 0, i));
        ev.pb(Ev(px[i].second.second, 2, i));
    }
    forn(i, cy) {
        ev.pb(Ev(py[i].first, 1, i));
    }
    sort(ev.begin(), ev.end());
    se.clear();
    se2.clear();
    for (Ev e : ev) {
        if (e.ty == 0) {
            fenInc(px[e.ind].first);
            auto it = se.insert(mp(px[e.ind].first, e.ind)).first;
            se2.insert(*it);
            if (it != se.begin()) {
                --it;
                se2.insert(*it);
            }
        } else if (e.ty == 1) {
            int from = py[e.ind].second.first;
            int to = py[e.ind].second.second;
            ans += fenGet(to) - fenGet(from - 1);
            auto it = se.lower_bound(mp(from, -1));
            if (it != se.end() && it->first <= to) {
                ans -= dsuMerge(it->second, cx + e.ind);
            }
            for (auto it = se2.lower_bound(mp(from, -1)); it != se2.end() && it->first <= to; ) {
                auto it2 = se.lower_bound(mp(it->first + 1, -1));
                if (it2 == se.end() || it2->first > to) {
                    break;
                }
                ans -= dsuMerge(it->second, it2->second);
                auto it3 = it;
                ++it3;
                se2.erase(it);
                it = it3;
            }
        } else {
            fenDec(px[e.ind].first);
            se2.erase(mp(px[e.ind].first, e.ind));
            auto it = se.find(mp(px[e.ind].first, e.ind));
            if (it != se.begin()) {
                auto pre = it;
                --pre;
                se2.insert(*pre);
            }
            se.erase(it);
        }
    }
    return ans;
}

int main() {
    scanf("%d", &tt);
    forn(test, tt) {
        readTest();
        cout << solve() << '\n';
    }
    return 0;
}