#!/bin/python3
import sys, re
fstd="t1"

sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')


if __name__ == "__main__":
    n = int(input().strip())
    reNMA=re.compile("^hydro\w*ic$")
    rePAA=re.compile("\w*ic$")
    for _ in range(n):
        s=input()
        if reNMA.match(s):
            print("non-metal acid")
        elif rePAA.match(s):
            print("polyatomic acid")
        else:
            print("not an acid")