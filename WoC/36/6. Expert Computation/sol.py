#!/bin/python3
import sys
fstd="t1"

sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')


MOD=1000000007

def getF(i,H,C,L):
    res=None
    for j in range(i-L[i]+1):
        v=H[j]*C[i]-C[j]*H[i];
        print("F({},{})={}".format(i,j,v))
        if res==None or res<v:
            res=v
    return res

def solve():
    N = int(input().strip())
    X = tuple(map(int,input().strip().split()))
    Y = tuple(map(int,input().strip().split()))
    Z = tuple(map(int,input().strip().split()))
    H=[0]*N
    C=[0]*N
    L=[0]*N
    F=[0]*N
    G=[0]*N

    H[0]=X[0]
    C[0]=Y[0]
    L[0]=Z[0]
    F[0]=getF(0,H,C,L)
    G[0]=F[0]
    for i in range(1,N):
        print("i=",i)
        H[i]=X[i]^G[i-1]
        C[i]=Y[i]^G[i-1]
        L[i]=Z[i]^G[i-1]
        F[i]=getF(i,H,C,L)
        G[i]=(G[i-1]+F[i])%MOD
        print("i={} H={} C={} L={} F={} G={} ".format(i,H[i], C[i], L[i], F[i],G[i]))
    return G[-1]

if __name__ == "__main__":
    print(solve())