// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <cstdio>
#include <iostream>
#include <map>
#include <utility>
using namespace std;


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

typedef long long LL;

const long MOD = 1000000007;


LL* H = NULL;
LL* C = NULL;
LL* L = NULL;

LL getF(int i, int N) {
	LL res = LLONG_MIN;
	for (int j = 1; j <= i - L[i]; j++) {
		LL v = H[j] * C[i] - C[j] * H[i];
		res = MAX(res, v);
	}
	return res;
}

int main() {
	int N;
	//boost;

	cin >> N;
	//cout<<MOD<<endl;
	H = (LL*)malloc((N + 1) * sizeof(*H));
	C = (LL*)malloc((N + 1) * sizeof(*C));
	L = (LL*)malloc((N + 1) * sizeof(*L));
	for (int i = 1; i <= N; i++)  cin >> H[i];
	for (int i = 1; i <= N; i++)  cin >> C[i];
	for (int i = 1; i <= N; i++)  cin >> L[i];

	LL F = getF(1, N);
	LL G = F;
	for (int i = 2; i <= N; i++) {
		H[i] ^= G;
		C[i] ^= G;
		L[i] ^= G;
		F = getF(i, N);
		G = (G + F) % MOD;
	}
	cout << G;
}