#!/bin/python3
import sys
fstd="t6"

sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')


def raceAgainstTime(n, mheight, heights, prices):
    heights = [mheight] + heights
    prices = [0] + prices
    
    nextincrease = list(range(1,n+1))
    for x,i in sorted(list((x,i) for (i,x) in enumerate(heights))):
        j = i+1
        while j < n and heights[i] >= heights[j]:
            j = nextincrease[j]
        nextincrease[i] = j
    
    answers = [0]*n
    
    stack = []
    for i in range(n-1,-1,-1):
        
        j = nextincrease[i]
        if j < n:
            answers[i] = answers[j] + abs(heights[i] - heights[j]) + prices[j]
        
        j = len(stack)-1
        while 0 <= j and stack[j] <= nextincrease[i]:
            k = stack[j]
            ans = answers[k] + prices[k] + abs(heights[i] - heights[k])
            answers[i] = min(answers[i], ans)
            j -= 1
        
        if prices[i] < 0:
            while stack and heights[i] > heights[stack[-1]]:
                stack.pop()
        else:
            while stack and heights[i] > heights[stack[-1]] and prices[stack[-1]] >= 0:
                stack.pop() 
        
        stack.append(i)
    
    return answers[0] + n


if __name__ == "__main__":
    n = int(input().strip())
    mason_height = int(input().strip())
    heights = list(map(int, input().strip().split(' ')))
    prices = list(map(int, input().strip().split(' ')))
    result = raceAgainstTime(n, mason_height, heights, prices)
    print(result)