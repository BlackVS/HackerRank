#!/bin/python3
import sys
fstd="t6"

sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')


#!/bin/python3
import sys
input = sys.stdin.readline

def solve():
    N = int(input().strip())
    H  = [int(input().strip())]+list(map(int, input().strip().split(' ')))
    W  = [0]+list(map(int, input().strip().split(' ')))
    HC = [0]*N
    MC = [None]*(N+1)

    HC[0]=H[0]
    MC[0]=0

    #get each person
    i=0
    while i<N:
        inext=None
        #and check all pissible directions, N+1 - final point
        for j in range(i+1,N+1):
            if j==N: #finish
                if MC[j]==None:
                    MC[j]=MC[i]
                else:   
                    MC[j]=min(MC[j],MC[i])
                break
            if W[j]<=0 and inext==None:
                inext=j
            m=MC[i]+abs(H[j]-H[i])+W[j]
            if MC[j]==None or m<MC[j]:
                MC[j]=m
            if H[j]>H[i]:
                break
        if inext==None:
            i+=1
        else:
            i=inext
    return MC[-1]+N
    
if __name__ == "__main__":
    print(solve())