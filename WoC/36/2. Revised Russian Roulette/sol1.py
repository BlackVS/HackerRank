#!/bin/python3

import sys
from collections import *
from itertools import *

if __name__ == "__main__":
    n = int(input().strip())
    doors = groupby( list(input().strip().split(' '))[:n] )
    cnt1, cnt2 = 0, 0
    for k,v in doors:
        if k!='1': continue
        c=len(list(v))
        cnt1+=(c+1)//2
        cnt2+=c
        #print(k,c,cnt1,cnt2)
    print(cnt1,cnt2)