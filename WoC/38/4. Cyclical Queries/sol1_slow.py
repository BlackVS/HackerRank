#!/bin/python3
import sys, os
from collections import *
from random import *

fstd="t2"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

#we have 10**5 nodes and max 10**5 new possible nodes
class Cycle(object):
    def __init__(self,n,wi):
        self.N=n
        #node index
        self.W     =[ wi[(i+N-1)%N] for i in range(N)]
        self.NNext =[ [(i+1)%N] for i in range(N)]
        self.NPrev =[ (i+N-1)%N for i in range(N)]

    #nodeFrom is only 0..N-1 or ends of tails
    def newNode(self,node,weight):
        N=self.N
        W=self.W
        NNext=self.NNext
        NPrev=self.NPrev
        idx=len(W)
        W.append(weight)
        NNext[node].append( idx )
        NNext.append([])
        NPrev.append(node)


    #removes only end of tails!
    def removeNode(self,node):
        pr=self.NPrev[node]
        self.NNext[node]=None
        self.NPrev[node]=None
        self.NNext[pr].remove(node)

    def getFarthest(self,node):
        resn,resw=-1,0
        Q=deque([(node,0)])

        while Q:
            v=Q.pop()
            if v[1]>resw:
                resn=v[0]
                resw=v[1]
            elif v[1]==resw:
                resn=max(resn,v[0])
            for u in self.NNext[v[0]]:
                if u==node: 
                    break
                Q.append( (u, v[1] + self.W[u]) )
        return (resn,resw)


#if __name__ == "__main__":
#    #fptr = open(os.environ['OUTPUT_PATH'], 'w')
#    N = int(input().strip())
#    W = list(map(int,input().strip().split()))
#    M = int(input().strip())
#    G = Cycle(N,W)
#    for _ in range(M):
#        scmd,*par=input().strip().split()
#        cmd=int(scmd)
#        if cmd==1:
#            x=int(par[0])-1
#            w=int(par[1])
#            y,_=G.getFarthest(x)
#            G.newNode(y,w)
#            continue
#        if cmd==2:
#            x=int(par[0])-1
#            w=int(par[1])
#            G.newNode(x,w)
#            continue
#        if cmd==3:
#            x=int(par[0])-1
#            y,_=G.getFarthest(x)
#            G.removeNode(y)
#            continue
#        if cmd==4:
#            x=int(par[0])-1
#            _,yw=G.getFarthest(x)
#            print(yw)
#            #fptr.write('\n'.join(map(str, result)))
#            continue
#    #fptr.close()

if __name__ == "__main__":
    WMAX=100000
    NMAX=100
    N = NMAX
    print(N)
    W = [ randint(1,WMAX) for _ in range(N)]
    print(" ".join(map(str,W)))
    M = 1000
    print(M)
    G = Cycle(N,W)
    for _ in range(M):
        cmd=randint(1,4)
        x=randint(0,N-1)
        w=randint(1,WMAX)
        if cmd==1:
            #print(cmd,x,w)
            y,_=G.getFarthest(x)
            G.newNode(y,w)
            continue
        if cmd==2:
            #print(cmd,x,w)
            G.newNode(x,w)
            continue
        if cmd==3:
            y,_=G.getFarthest(x)
            if y<N:
                continue
            #print(cmd,x)
            G.removeNode(y)
            continue
        if cmd==4:
            #print(cmd,x)
            _,yw=G.getFarthest(x)
            #print("> ",yw)
            continue
