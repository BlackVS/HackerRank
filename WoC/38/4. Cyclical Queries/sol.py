#!/bin/python3
import sys
from random import *
from collections import *

fstd="t1"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

#!/bin/python3

import math
import os
import random
import re
import sys
import heapq

class Node:
    def __init__(self):
        self.q = []
        
    def addfar(self, n, serial):
        heapq.heappush(self.q, ((self.q[0][0] if self.q else 0) - n, -serial))
        
    def addroot(self, n, serial):
        heapq.heappush(self.q, (-n, -serial))
    
    def max(self):
        return (-self.q[0][0], -self.q[0][1]) if self.q else (0, -1) 
            
    def remove(self):
        heapq.heappop(self.q)
        
    def __str__(self):
        return ', '.join(map(str, self.q))
            
class Ring:
    def __init__(self, w):
        self.n = len(w)
        self.w = w
        self.wsum = [0] * (len(w) + 1)
        for i in range(len(w)):
            self.wsum[i+1] = self.wsum[i] + w[i]
        self.nodes = [Node() for _ in range(self.n)]
        self.maxes = [[(0, i, i) for i in range(self.n)]]
        self.calc_max()
        self.serial = self.n
        
    def ws(self, x, y):
        if y >= x:
            return self.wsum[y] - self.wsum[x]
        return self.wsum[-1] + self.wsum[y] - self.wsum[x]
    
    def addfar(self, x, w):
        y = self.farthest_num(x)[2]
        self.nodes[y].addfar(w, self.serial)
        self.patch_max(y)
        self.serial += 1
        
    def add(self, x, w):
        self.nodes[x].addroot(w, self.serial)
        self.patch_max(x)
        self.serial += 1
        
    def remove(self, x):
        y = self.farthest_num(x)[2]
        self.nodes[y].remove()
        self.patch_max(y)
                
    def farthest(self, x):
        return self.farthest_num(x)[0]
    
    def calc_max(self):
        level, step = 0, 1
        while step < self.n:
            mprev = self.maxes[-1]
            mx = []
            for x in range(len(mprev) // 2):
                m1 = mprev[2*x]
                m20, m21, m22 = mprev[2*x+1]
                mx.append(max(m1,
                              (m20 + self.ws(2 * x * step, (2 * x + 1) * step),
                               m21, m22)))
            self.maxes.append(mx)
            level, step = level + 1, step * 2
            
    def patch_max(self, x):
        self.maxes[0][x] = self.nodes[x].max() + (x,)
        level, step = 0, 1
        ml = self.maxes[level]
        while step < self.n and x | 1 < len(self.maxes[level]):
            m1 = ml[x&~1]
            m20, m21, m22 = ml[x|1]
            ml = self.maxes[level+1]
            ml[x//2] = max(m1, (m20 + self.ws((x&~1) * step, (x|1) * step), m21, m22))
            level, step, x = level + 1, step * 2, x // 2
        
    def far_num(self, x, y):
        level, step = 0, 1
        xx = x
        mx = (0, -1, -1)
        while xx < y:
            while xx & (2 * step - 1) == 0 and xx + 2 * step < y:
                level, step = level + 1, step * 2
            while xx + step > y:
                level, step = level - 1, step // 2
            m0, m1, m2 = self.maxes[level][xx//step]
            mx = max(mx, (m0 + self.ws(x, xx), m1, m2))
            xx += step
        return mx
        
    def farthest_num(self, x):
        w1, ser1, i1 = self.far_num(0, x)
        w2, ser2, i2 = self.far_num(x, self.n)
        result = max((w1 + self.ws(x, 0), ser1, i1), (w2, ser2, i2))
        return result
    
    def __str__(self):
        return ';'.join(map(str, self.nodes))

# Complete the cyclicalQueries function below.
def cyclicalQueries(w, m):
    # Return the list of answers to all queries of type 4. Take the query information from standard input.
    ring = Ring(w)
    result = []
    for _ in range(m):
        inp = list(map(int, input().rstrip().split()))
        if inp[0] == 1:
            ring.addfar(inp[1] - 1, inp[2])
        elif inp[0] == 2:
            ring.add(inp[1] - 1, inp[2])
        elif inp[0] == 3:
            ring.remove(inp[1] - 1)
        else:
            result.append(ring.farthest(inp[1] - 1))
    return result

if __name__ == '__main__':
    n = int(input())
    w = list(map(int, input().rstrip().split()))
    m = int(input())
    result = cyclicalQueries(w, m)
    print('\n'.join(map(str, result)))
