#!/bin/python3
import sys
from heapq import *
from collections import *
fstd="t3"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

class Graph(object):
    def __init__(self,n):
        self.VN=n
        self.G  = [[] for _ in range(n)]
        self.E  = []
        self.EW = []
        self.T  = 0
        self.R  = []

    def add_edge(self,u,v,w=0):
        u,v=min(u,v),max(u,v)
        eidx=len(self.E)
        self.E.append( (u,v) )
        self.EW.append(0)
        self.G[u].append( (v,eidx) )
        self.G[v].append( (u,eidx) )

    def getBestPath(self,s,t):
        VN = self.VN
        EW = self.EW
        G = self.G
        R = self.R
        Q = [(0, s)]
        P = [None]*VN
        V = [False]*VN
        
        while Q:
            (totalCost,u) = heappop(Q)
            if not V[u]:
                V[u]=True
                if u == t:
                    while P[t]!=None:
                        p,e=P[t]
                        EW[e]+=1
                        t=p
                    return int(totalCost)

                for v,e in G[u]:
                    if not V[v]:
                        # It's a python inbuilt Heap. Whenever we will do POP , we will get the element with MinCost .
                        P[v]=(u,e)
                        heappush(Q, (max(totalCost,EW[e])+EW[e]/5000,v) )
        return None

  
    def solve(self,K,T):
        E  = self.E
        self.T=T
        self.R=[0]*len(E)
        #
        res=0
        for k in range(K):
            if T==0:
                res=max(res,self.getBestPath(0,self.VN-1))
                continue
            ET=self.EW.copy()
            r=0
            r=max(res,self.getBestPath(0,self.VN-1))
            self.EW,ET=ET,self.EW
            if max(ET)>=2:
                for i in range(len(E)):
                    if T==0: break
                    if ET[i]>1:
                        self.EW[i]-=1
                        T-=1
            res=max(res,self.getBestPath(0,self.VN-1))
        return res

if __name__ == "__main__":
    N,M,K,T = map(int, input().strip().split())
    G=Graph(N)
    for _ in range(M):
        u, v = map(int,input().strip().split())
        G.add_edge(u, v)
    print(G.solve(K,T))