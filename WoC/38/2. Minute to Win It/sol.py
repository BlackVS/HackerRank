#!/bin/python3
import sys
from collections import *
fstd="t1"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

if __name__ == "__main__":
    N,K = map(int,input().strip().split())
    A=map(lambda s:int(s[1])-K*s[0],enumerate(input().strip().split()))
    C=Counter(A)
    print(N-max(C.values()),C)