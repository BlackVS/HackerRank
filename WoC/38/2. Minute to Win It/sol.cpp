// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <cstdio>
#include <iostream>
#include <map>
#include <unordered_map>


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)
using namespace std;

/////////////////////// TIME 0
#include <chrono>
#include <utility>
typedef std::chrono::high_resolution_clock::time_point TimeVar;
#define duration(a) std::chrono::duration_cast<std::chrono::nanoseconds>(a).count()
#define timeNow() std::chrono::high_resolution_clock::now()

template<typename F, typename... Args>
double funcTime(F func, Args&&... args) {
	TimeVar t1 = timeNow();
	func(std::forward<Args>(args)...);
	return duration(timeNow() - t1);
}

class CTime {
	TimeVar t1;
	CTime() {
	}
	void Init() {
		t1 = timeNow();
	}
	template<typename F, typename... Args> double funcTime(F func, Args&&... args) {
		func(std::forward<Args>(args)...);
		return duration(timeNow() - t1);
	}
};

//////////////////////////////
int main() {
	int N, K;
	boost;
	cin >> N >> K;
	int d = 0, a, c, r = 0;
	unordered_map<int, int> F;
	for (int i = 0; i < N; i++, d+=K) {
		cin >> a;
		a -= d;
		c = F[a] = F[a] + 1;
		r = MAX(c, r);
		cout << a << " " << c << endl;
	}
	cout << N - r << endl;
}