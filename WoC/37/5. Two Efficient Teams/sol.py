#!/bin/python3
import sys
from collections import *
from bisect import *

fstd="t1"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')


#fstdg="g1"
#sys.stdout = open('..\\..\\%s.txt' % fstdg , 'w+')

#input = sys.stdin.readline

sys.setrecursionlimit(10000)

ab2i =lambda  a,b: (b<<6)+a 
abc2i=lambda  a,b,c: (c<<12)+(b<<6)+a

NMAX=40
def B2I(b):
    if b==0: return None
    i=1
    while b>1:
        b>>=1
        i+=1
    return i

def grayencode(g): 
    return g ^ (g >> 1);

def getGroupColors(G,VCLR):
    res=[0]*len(G)
    for ig,vG in enumerate(G):
        for v in vG:
            res[ig]+=VCLR[v]
    return res

def getGroupRes(G,VCLR,W):
    res=0
    for ig,vG in enumerate(G):
        r=0
        for v in vG:
            r+=VCLR[v]
        if r==0 or r==len(vG):
            res+=W[ig]
    return res

class Graph(object):
    def __init__(self,N):
        self.m_N=N
        #self.m_M=0
        self.m_V=[ [] for _ in range(N+1) ]
        self.m_G=[]
        self.m_W=[]
        self.m_Fmax=0
        self.m_NB=[set() for _ in range(N+1)]
        self.m_S0=set(range(1,N+1))
        self.m_S1=set()
        #
        self.m_VCLR=None
        self.m_GCKR=None
        self.m_eff =None

    def addGroup(self,Vi,Fi):
        idx = len(self.m_G)
        self.m_G.append(Vi)
        self.m_W.append(Fi)
        s=set(Vi)
        for v in Vi:
            self.m_V[v].append(idx)
            self.m_NB[v]|=s-{v}
        self.m_Fmax+=Fi
        return idx

    def prepare(self):
        M=len(self.m_G)
        #all in one team (0)
        self.m_VCLR = [0]*(self.m_N+1)
        self.m_GCLR = [0]*M
        self.m_eff = self.m_Fmax #max efficiency but single team
        return self.m_eff
    
    def getColor(self,vi):
        return self.m_VCLR[vi]

    def changeColor(self,vi):
        V=self.m_V
        W=self.m_W
        G=self.m_G
        VCLR=self.m_VCLR
        GCLR=self.m_GCLR

        idx=vi
        #changing color of idx vertex
        if VCLR[idx]==1:
            self.m_S1.discard(vi)
        else:
            self.m_S0.discard(vi)
        VCLR[idx]=1-VCLR[idx]
        if VCLR[idx]==1:
            self.m_S1.add(vi)
        else:
            self.m_S0.add(vi)
        d=(-1,1)[VCLR[idx]]
        #check all groups for Vidx
        for ig in V[idx]:
            oldv=GCLR[ig]
            GCLR[ig]+=d
            if GCLR[ig]==0 or GCLR[ig]==len(G[ig]):
                self.m_eff+=W[ig]
            if oldv==0 or oldv==len(G[ig]):
                self.m_eff-=W[ig]
        return self.m_eff

    def getNeighbours(self,vi,clr):
        res=[]
        V=self.m_V
        VCLR=self.m_VCLR
        idx=vi
        for n in self.m_NB[idx]:
            if VCLR[n]==clr:
                res.append(n)
        return res

    def isConnected(self):
        N=self.m_N
        V=self.m_V
        G=self.m_G
        F=[0]*(N+1)
        root=1
        stack=deque([root])
        while stack:
            v=stack.popleft()
            if F[v]==1: continue
            F[v]=1 #mark and add to stack connected
            for g in V[v]: #go through groups
                for u in G[g]: #and check group members
                    if F[u]==0:
                        stack.append(u)
        return sum(F)==N

def solve():
    N,M = map(int,input().strip().split())
    sol=Graph(N)

    giMax=None
    gfMax=0
    ggMax=None
    for iG in range(M):
        _, Fi = map(int, input().strip().split())
        g = tuple(map(int,input().strip().split()))
        gi=sol.addGroup(g,Fi)
        if giMax==None or gfMax<Fi:
            giMax=gi
            gfMax=Fi
            ggMax=g

    resMax=sol.prepare()
    #if not sol.isConnected():
    #    return resMax

    res = 0
    stack=deque(ggMax)

    while(stack and res<resMax):
        vi=stack.popleft()
        #if sol.getColor(vi)==1: continue
        #try switch
        if sol.getColor(vi)==0 and len(sol.m_S0)<2:
            continue
        if sol.getColor(vi)==1 and len(sol.m_S1)<2:
            continue
        r = sol.changeColor(vi)
        if res==None or r>=res:
            res=r
            #add non-switched neigbors
            #nb=sol.getNeighbours(vi,0)
            #if nb:
            #    stack.extend(nb)
            stack.extend(sol.m_NB[vi])
            continue
        #switch back
        sol.changeColor(vi)

    return res

if __name__ == "__main__":
    print(solve())