#!/bin/python3
import os
import sys,math 
from decimal import *

fstd="t1"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)


# Complete the averageOfTopEmployees function below.
def averageOfTopEmployees(rating):
    cnt=0
    s =0
    for r in rating:
        if r>=90:
            s+=r
            cnt+=1
    s/=cnt
    #s=95.0
    s*=100
    i=int(s)
    r=s-i
    if r>=0.5: i+=1
    print("{}.{:02}".format(i//100,i%100))

if __name__ == '__main__':
    n = int(input())

    rating = []

    for _ in range(n):
        rating_item = int(input())
        rating.append(rating_item)

    averageOfTopEmployees(rating)
