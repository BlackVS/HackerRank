#!/bin/python3
import sys
from itertools import *

fstd="t2"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

MODM=1000000007

def getZ_2(S):
    N=len(S)
    Z=[0]*N
    i=1
    L=0
    R=1
    while i<N:
        Z[i]=min(R-i,Z[i-L])
        while i+Z[i]<N and S[i+Z[i]]==S[Z[i]]:
            Z[i]+=1
            R=i+Z[i]
            L=i
        i+=1
        if i>=R:
            R=i
    return Z

def getMaxZ_2(S):
    resmax=0
    
    N=len(S)
    Z=[0]*N
    i=1
    L=0
    R=1
    while i<N:
        Z[i]=min(R-i,Z[i-L])
        resmax=max(resmax,Z[i])
        while i+Z[i]<N and S[i+Z[i]]==S[Z[i]]:
            Z[i]+=1
            resmax=max(resmax,Z[i])
            R=i+Z[i]
            L=i
        i+=1
        if i>=R:
            R=i
    return resmax

grayencode = lambda g: g ^ (g >> 1)

def solve(N,K):
    res=0

    for s in product(range(K),repeat=N-1):
        #print(s,max(getZ_2(s)),getMaxZ_2(s))
        res+=getMaxZ_2([0]+list(s))
        if res>=MODM:
            res%=MODM
    return (res*K)%MODM

if __name__ == "__main__":
    N,K = map(int, input().strip().split())
    #print(solve(N,K))
    #print(solve(5,4))
    for K in range(1,10):
        for N in range(2,10):
            print(K,N,solve(N,K))