#!/bin/python3
import os,sys
from itertools import *

fstd="t1"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

def countMaxima(X):
    r=0
    for i in range(1,len(X)-1):
        r+=X[i]>X[i-1] and X[i]>X[i+1]
    return r

def solve0(F):
    res = 0
    s = tuple(chain(*([i]*f for i,f in enumerate(F))))
    for p in permutations(s):
        #print(p)
        r=countMaxima(p)
        #print(p,r)
        if r>res:
            res=r
            lastres=tuple(p)
    return res,lastres

def solve1(F):
    N=sum(F)
    Nres=N
    #if N&1==0: Nres-=1
    S=[0]*Nres
    idx=1
    for i in range(len(F)-1,-1,-1):
        while idx<Nres and F[i]:
            S[idx]=i
            F[i]-=1
            idx+=2
            if idx>=Nres:
                idx=0
    res=countMaxima(S)
    return res,S

def solve2(F):
    N=sum(F)
    Nres=N
    #if N&1==0: Nres-=1
    S=[0]*Nres
    idx =1-(Nres&1)
    idx1=1-idx
    for i in range(len(F)):
        while idx<Nres and F[i]:
            S[idx]=i
            F[i]-=1
            idx+=2
            if idx>=Nres:
                idx=idx1
    res=countMaxima(S)
    return res,S

def getObsolete(a,b,N):
    #count odd indexes inside [a,b] such that have neighbour at right
    res=0
    if b==N-1:
        b-=1
    if a&1==0: 
        a+=1
    if b&1==0:
        b-=1
    if a>b:
        return 0
    return ((b-a)>>1)+1

def solve3(F):
    res=0
    N=sum(F)
    resMax=(N-1)>>1
    N0=(((N-1)&-1)>>1)+1
    N1=N-N0
    #print(N0,N1)
    idx=len(F)-1
    acc=N1
    cur=1
    #
    ovrIdx   =None
    ovrStart1=None
    ovrEnd1  =None
    ovrStart2=None
    ovrEnd2  =None
    delta=0
    while acc and cur<N:
        while(F[idx]==0):
            idx-=1
            if idx<0:
                break #ERROR!!!! never should
        if acc<F[idx]:
            #possible overlapped found, check and break
            ovrStart1=cur
            ovrEnd1  =cur+(acc-1)*2 
            ovrStart2=0
            ovrEnd2  =(F[idx]-acc-1)*2
            if ovrEnd2+1==ovrStart1:
                delta=getObsolete(ovrEnd2,ovrStart1,N)
            if ovrEnd2>ovrStart1:
                ovrEnd2+=(ovrEnd2+1<N)
                ovrStart1-=(ovrStart1>0)
                delta=getObsolete(ovrStart1,ovrEnd2,N)
            break
        acc-=F[idx]
        cur+=F[idx]*2
        F[idx]=0
    return resMax-delta,delta



if __name__ == "__main__":
    #fptr = open(os.environ['OUTPUT_PATH'], 'w')
    T = int(input().strip())
    #print(T)
    #for _ in range(T):
    #    F = list(map(int, input().rstrip().split()))
    #    print(solve1(F.copy()))
    #    print(solve1_2(F.copy()))
    #    #fptr.write(str(result) + '\n')
    #fptr.close()
    
    #Nmax=10
    #for f0 in range(1,Nmax+1):
    #    for f1 in range(1,Nmax+1):
    #        for f2 in range(1,Nmax+1):
    #            for f3 in range(1,Nmax+1):
    #                F=[f0,f1,f2,f3]
    #                #print(F)

    #                r1,lastres1=solve1(F.copy())
    #                #print(r1,lastres)

    #                r2,lastres2=solve2(F.copy())
    #                #print(r12,lastres)

    #                r3=solve3(F.copy())

    #                if r1!=r3:
    #                    NF=sum(F)
    #                    resMax=(NF-1)>>1
    #                    print(F)
    #                    print("ResMax=",resMax)
    #                    print("r1=", r1, lastres1)
    #                    print("r3=", r3)
    #                    print()
    
    #F=[1,1,1,6]
    #NF=sum(F)
    #resMax=(NF-1)>>1
    #print("ResMax=",resMax)
    #print(F,solve1(F.copy()))
    #print(F,solve2(F.copy()))
    #print(F,solve3(F.copy()))


    #Nmax=10
    #for f0 in range(1,Nmax+1):
    #    for f1 in range(1,Nmax+1):
    #        for f2 in range(1,Nmax+1):
    #            for f3 in range(1,Nmax+1):
    #                F=[f0,f1,f2,f3]
                    
    #                print(F)
    #                NF=sum(F)
    #                resMax=(NF-1)>>1
    #                print(NF,resMax,max(F),solve3(F.copy()))
    #                print()
    F=[1,1,1,7]
    NF=sum(F)
    resMax=(NF-1)>>1
    print(F)
    print(NF,resMax,max(F),solve3(F.copy()))
    print()

    F=[1,1,7,1]
    NF=sum(F)
    resMax=(NF-1)>>1
    print(F)
    print(NF,resMax,max(F),solve3(F.copy()))
    print()

    F=[1,7,1,1]
    NF=sum(F)
    resMax=(NF-1)>>1
    print(F)
    print(NF,resMax,max(F),solve3(F.copy()))
    print()

    F=[7,1,1,1]
    NF=sum(F)
    resMax=(NF-1)>>1
    print(F)
    print(NF,resMax,max(F),solve3(F.copy()))
    print()
