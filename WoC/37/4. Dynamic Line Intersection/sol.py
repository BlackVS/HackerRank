#!/bin/python3
import sys
from collections import *
from bisect import *


fstd="g2"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)


YMAX=100000

class Solution(object):
    def __init__(self,linesY):
        #current result
        self.m_accY  = [0]*(1+YMAX)
        #self.m_lines=Counter()
        self.m_LY = linesY


    def update(self,chunk):
        for (k,b),c in chunk.items():
            for y in self.m_LY[(k,b)]:
                if y%k==b:
                    self.m_accY[y]+=c

    def query(self,q):
        return self.m_accY[q]

def solve():
    Q = int(input().strip())

    queries=deque()
    lines=Counter()
    linesY=defaultdict(set)
    chunk=Counter()

    for i in range(Q):
        s=input().strip()
        if s[0]=="+":
            k,b=map(int,s[2:].split())
            b%=k
            chunk[(k,b)]+=1
        elif s[0]=="-":
            k,b=map(int,s[2:].split())
            b%=k
            chunk[(k,b)]-=1
        else: # ?
            q=int(s[2:])
            lines.update(chunk)
            for k,l in lines.items():
                if l:
                    linesY[k].add(q)
            queries.append( (chunk,q) )
            chunk.clear()

    L=Solution(linesY)
    for chunk,q in queries:
        L.update(chunk)
        r=L.query(q)
        print(r)

if __name__ == "__main__":
    print(solve())
