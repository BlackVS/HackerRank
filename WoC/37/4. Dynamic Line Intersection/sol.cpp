// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#define getchar_unlocked getchar
#define putchar_unlocked putchar

#include <cmath>
#include <cstdio>
#include <iostream>
#include <unordered_map>
#include <set>
#include <utility>
#include <string>
using namespace std;


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

typedef long long ll;




void fastscan(int &number)
{
	//variable to indicate sign of input number
	bool negative = false;
	register int c;

	number = 0;

	// extract current character from buffer
	do {
		c = getchar_unlocked();
	} while (c == ' ' || c == '\n');

	if (c == '-')
	{
		// number is negative
		negative = true;

		// extract the next character from the buffer
		c = getchar_unlocked();
	}

	// Keep on extracting characters if they are integers
	// i.e ASCII Value lies from '0'(48) to '9' (57)
	for (; (c>47 && c<58); c = getchar_unlocked())
		number = number * 10 + c - 48;

	// if scanned input has a negative sign, negate the
	// value of the input number
	if (negative)
		number *= -1;
}

template<typename T> void scan(T &x)
{
	x = 0;
	bool neg = 0;
	register T c = getchar();

	if (c == '-')
		neg = 1, c = getchar();

	while ((c < 48) || (c > 57))
		c = getchar();

	for (; c < 48 || c > 57; c = getchar());

	for (; c > 47 && c < 58; c = getchar())
		x = (x << 3) + (x << 1) + (c & 15);

	if (neg) x *= -1;
}

template<typename T> void print(T n)
{
	bool neg = 0;

	if (n < 0)
		n *= -1, neg = 1;

	char snum[65];
	int i = 0;
	do
	{
		snum[i++] = n % 10 + '0';
		n /= 10;
	}

	while (n);
	--i;

	if (neg)
		putchar_unlocked('-');

	while (i >= 0)
		putchar_unlocked(snum[i--]);

	putchar_unlocked('\n');
}


#define YMAX 100000

int main() {
	int N;
	boost;

	fastscan(N);
	//cin.ignore(numeric_limits<streamsize>::max(), '\n');

	unordered_map< int, unordered_map<int, int> > L;
 
	int k, b, q;
	for (int i = 0; i < N; i++) {
		char ch = getchar_unlocked();
		if (ch == '+') {
			//cin >> k >> b;
			fastscan(k);
			fastscan(b);
			b %= k;
			L[k][b]++;
		}
		else
			if (ch == '-') {
				//cin >> k >> b;
				fastscan(k);
				fastscan(b);
				b %= k;
				L[k][b]--;
			}
			else
				if (ch == '?') { 
					fastscan(q);
					int res = 0;
					for (auto& l : L) {
						int b = q%l.first;
						res += l.second[b];
					}
					print(res);
				}
	}
}