#!/bin/python3
import sys
from collections import *
sys.stdin = open('..\\..\\t1.txt', 'r')



if __name__ == '__main__':
    r, c = list(map(int, input().strip().split()))

    a = [[0]*c]
    dp = [[0]*c for _ in range(r + 1)]

    for i in range(1, r + 1):
        row = list(map(int, input().strip().split()))
        a.append(row)

        left = right = 0
        temp = [[0, 0] for _ in range(c)]
        for j in range(c):
            temp[j][0] = left
            temp[c - j - 1][1] = right
            left = max(0, left + a[i][j])
            right = max(0, right + a[i][c - j - 1])

        curr = dp[i - 1][0]
        for j in range(c):
            curr = max(curr + a[i][j], dp[i - 1][j] + a[i][j] + temp[j][0])
            dp[i][j] = curr + temp[j][1]

        curr = dp[i - 1][c - 1]
        for j in range(c - 1, -1, -1):
            curr = max(curr + a[i][j], dp[i - 1][j] + a[i][j] + temp[j][1])
            dp[i][j] = max(dp[i][j], curr + temp[j][0])
        
    print(max(dp[r]))