#!/bin/python3
import sys
from bisect import *
#fstd="t3"
#sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')

class Airports(object):
    def __init__(self,d):
        # sorted by x, freq
        self.X=[]
        self.D=d
        #self.dX=[]

    def add(self,x):
        X=self.X
        D=self.D
        if len(X)==0:
            self.X.append( x )
            return
        #ptr=bisect_left(X, x)
        #X.insert(ptr, x )
        insort_left(X,x)
        return

    def check(self):
        X=self.X
        D=self.D
        if len(X)<2:
            return 0
        if len(X)==2:
            return D-(X[1]-X[0])

        # R-L>=D
        res=max(0, D-(X[-1]-X[0]))

        dmax=0
        L=X[-1]-D
        R=X[0]+D
        xLi=bisect_left(X,L)
        xRi=bisect_right(X,R)
        
        
        if X[-1]-X[0]<D:
            xLi+=1
            xRi-=1

        dmax=max(dmax, X[xLi]-L )
        dmax=max(dmax, R-X[xRi-1])
        for i in range(xLi,xRi-1):
            dmax=max(dmax,X[i+1]-X[i])

        return max(res,R-L-dmax)

if __name__ == "__main__":
    Q = int(input().strip())
    for _ in range(Q):
        N, D = map(int, input().strip().split(' '))
        ports=Airports(D)
        res =""
        for i,x in enumerate(map(int, input().strip().split(' '))):
            ports.add(x)
            r=ports.check()
            if len(res): 
                res=res+" "
            res+=str(r)
            #print(i,r)
        print(res)