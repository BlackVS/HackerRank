// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <cstdio>
#include <iostream>
#include <map>
#include <utility>
using namespace std;


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

class CAirports {
		int m_D;
		map<int, int> m_F;

	public:
		CAirports(int D) {
			m_D = D;
		}
		
		void add(int x) {
			m_F[x]++;
		}

		int check(void) {
			if (m_F.size() == 1) {
				if (m_F.begin()->second == 1) 
					return 0;
				else 
					return m_D;
			}
			int res = m_D; //it is garanteed maximum
			//check bounds
			int dl=m_F.begin()->first
			return 0;
		}
};

int main() {
	int N, D, Q;
	boost;

	cin >> Q;
	for (int q = 0; q < Q; q++) {
		cin >> N >> D;
		CAirports airport(D);
		int x;
		for (int i = 0; i < N; i++) {
			cin >> x;
			airport.add(x);
			int r = airport.check();
			std::cout << ((i == 0) ? "" : " ") << r;
		}
	}
}