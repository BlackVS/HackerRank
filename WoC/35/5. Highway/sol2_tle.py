#!/bin/python3
import sys
from collections import *
#sys.stdin = open('..\\..\\t1.txt', 'r')

MOD = 1000000009
 
def solveSlow(n, k):
    res=0
    for i in range(2,n):
        res+=i**k
        if res>=MOD:
            res%=MOD
    return res


def prepare(maxk=1010):
    a = []
    for i in range(maxk):
        t = [0] * (i + 1)
        a.append(t)
        for j in range(0, len(t)):
            if j in [0, i]:
                a[i][j] = 1
            else:
                a[i][j] = (a[i - 1][j - 1] + a[i - 1][j]) % MOD
    return a


A=prepare()

def solveFast(n, k):
    if n<3:
        return 0
    n-=1
    n_k = []
    n_k.append(n)
    for i in range(1, k + 1):
        t = (pow(n + 1, i + 1, MOD) - 1) % MOD
        s = 0
        for j in range(2, i + 2):
            s += (A[i + 1][j] * n_k[i + 1 - j]) % MOD
        n_k.append((((t - s) % MOD) * pow(A[i + 1][1], MOD - 2, MOD)) % MOD)
    res=n_k[-1]-1
    if res<0:res+=MOD
    return res

if __name__ == "__main__":
    Q = int(input().strip())
    for _ in range(Q):
        N, K = map(int, input().strip().split(' '))
        #print(solveSlow(N, K))
        print(solveFast(N, K))
