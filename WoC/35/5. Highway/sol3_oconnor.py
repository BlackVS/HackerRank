#!/bin/python3

import time
from fractions import Fraction as fr
from math import floor

def euclidean_alg(a,b):
    """This function implements the Extended Euclidean algorithm.  It returns
    three values.  The first is the greatest common divisor of a and b.  The
    other two are the Bezout coefficients such that

    r = a*s+b*t

    """
    rm1 = a
    sm1 = 1
    tm1 = 0

    r = b
    s = 0
    t = 1

    while r != 0:
        q = rm1 // r
        temp_r = rm1
        temp_s = sm1
        temp_t = tm1

        rm1 = r
        sm1 = s
        tm1 = t

        r = temp_r-q*rm1
        s = temp_s-q*sm1
        t = temp_t-q*tm1

    return (rm1,sm1,tm1)


def modular_inverse(n,p):
    """For a prime number p this function returns the modular inverse
    
    n^(-1) mod p

    """
    r,s,t = euclidean_alg(n,p)
    return s



modulus = 10**9+9




def bernoulli_mod(n,modulus):
    res = []
    A = []
    for m in range(n+1):
        A.append(modular_inverse(m+1,modulus))
        for j in range(m,0,-1):
            A[j-1] = j*(A[j-1]-A[j]) % modulus
        res.append(A[0])
    return res

bern = bernoulli_mod(1001,modulus)


def solve(n,k):
    if n <= 2:
        return 0
    ans = 0
    bin = 1
    for j in range(k+1):
        ans = (ans+bin*bern[j]*pow(n-1,k+1-j,modulus)) % modulus
        bin = (bin*(k+1-j)*modular_inverse(j+1,modulus)) % modulus
    ans = (ans*modular_inverse(k+1,modulus)-1) % modulus
    return ans

q = int(input())
for i in range(q):
    n,k = [int(val) for val in input().split()]
    print(solve(n,k))
