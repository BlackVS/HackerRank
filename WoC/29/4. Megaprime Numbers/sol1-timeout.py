#!/bin/python3

import sys
import math
from itertools import*
from fractions import gcd

def isqrt(n):
    x = n
    while 1:
        y = (n//x + x) // 2
        if x <= y: return x
        x = y

def primes(n):
    ps, sieve = [], [True] * (n+1)
    for p in range(2, n):
        if sieve[p]:
            ps.append(p)
            for i in range(p*p, n, p):
                sieve[i] = False
    return ps
 
def jacobi(a, p):
    a, t = a%p, 1
    while a != 0:
        while a%2 == 0:
            a = a//2
            if p%8 in (3, 5):
                t = -t
        a, p = p, a
        if a%4 == 3 and p%4 == 3:
            t = -t
        a = a%p
    if p == 1: return t
    else: return 0

def isStrongPseudoprime(n, a):
    d, s = n-1, 0
    while d%2 == 0:
        d, s = d//2, s+1
    t = pow(a, d, n)
    if t == 1:
        return True
    while s > 0:
        if t == n-1:
            return True
        t, s = (t*t) % n, s-1
    return False
 
def chain(n, u, v, u2, v2, d, q, m):
    k = q
    while m > 0:
        u2 = (u2*v2) % n
        v2 = (v2*v2 - 2*q) % n
        q = (q*q) % n
        if m%2 == 1:
            t1, t2 = u2*v, u*v2
            t3,t4 = v2*v, u2*u*d
            u, v = t1+t2, t3+t4
            if u%2 == 1: u = u+n
            if v%2 == 1: v = v+n
            u, v = (u//2)%n, (v//2)%n
            k = (q*k) % n
        m = m // 2
    return u, v, k
 
def selfridge(n):
    d, s = 5, 1; ds = d * s
    while 1:
        if gcd(ds, n) > 1: return ds, 0, 0
        if jacobi(ds, n) == -1: return ds, 1, (1-ds) // 4
        d, s = d+2, s*-1; ds = d * s

def isStrongLucasPseudoprime(n):
    d, p, q = selfridge(n)
    if p == 0: return n == d
    s, t = 0, n+1
    while t%2 == 0:
        s, t = s+1, t//2
    u, v, k = chain(n, 1, p, 1, p, d, q, t//2)
    if u == 0 or v == 0: return True
    r = 1
    while r < s:
        v = (v*v - 2*k) % n
        k = (k*k) % n
        if v == 0: return True
    return False
 
def isBaillieWagstaffPrime(n, limit = 100):
    def isSquare(n):
        s = isqrt(n)
        return s*s == n
    if n<2 or isSquare(n): return False
    for p in primes(limit):
        if n % p == 0:
            return n == p
    return isStrongPseudoprime(n, 2) \
       and isStrongPseudoprime(n, 3) \
       and isStrongLucasPseudoprime(n) # or standard

def b2i(x):
    r=0
    for i in x:
        r=r*10+i
    return r

def solve(m,n):
    if n<10:
        return sum(map(lambda x: x in (2,3,5,7),range(m,n+1)))
    res=0
    ml=int(math.log10(m))+1
    nl=int(math.log10(n))+1
    for ll in range(ml-1,nl-1+1):
        if ll==0:
            res+=(2>=m and 2<=n)
            res+=(3>=m and 3<=n)
            res+=(5>=m and 5<=n)
            res+=(7>=m and 7<=n)
            continue
        for base in product((2,3,5,7),repeat=ll):
            sb=b2i(base)
            if sb*10>n: break
            if sb*10+7<m: continue
            ss=sum(base)
            if (ss+3)%3:
                n1=sb*10+3
                res+=n1>=m and n1<=n and isBaillieWagstaffPrime(n1)
            if (ss+7)%3:
                n2=sb*10+7
                res+=n2>=m and n2<=n and isBaillieWagstaffPrime(n2)
    return res

m,n = map(int, input().strip().split())
# your code goes here
print(solve(m,n))