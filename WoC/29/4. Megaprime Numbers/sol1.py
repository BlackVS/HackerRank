#!/bin/python3

import sys


import sys
import math
from itertools import*
import sys

def tryComposite(a, d, n, s):
    if pow(a, d, n) == 1:
        return False
    for i in range(s):
        if pow(a, 2**i * d, n) == n-1:
            return False
    return True # n  is definitely composite

normDigits = [2,2,2,3,5,5,7,7,2,2,2]
def initNext(a):
    carry = 0
    twos = len(a)
    for i in range(len(a)):
        ao=a[i]+carry
        carry=ao>7
        if carry:
            twos=i
        a[i]=normDigits[ao]
        if a[i]!=ao:
            twos=i
    if carry:
        a.append(2)

    for i in range(twos):
        a[i]=2
    return a

nextDigits = [2,2,3,5,5,7,7,2,2,2]
def getNextNum(a):
    carry = True
    for i in range(len(a)):
        a[i]=nextDigits[a[i]]
        if a[i]!=2:
            carry=False
            break
    if carry:
        a.append(2)

    #simple pre-checks
    if len(a)>1 and (a[0]==2 or a[0]==5):
        a[0]=nextDigits[a[0]]
    return a

def toInt(a):
    res=0
    for n in a[::-1]:
        res=res*10+n
    return res

def isLess(a,b):
    if len(a)==len(b):
        for i in range(len(a)-1,-1,-1):
            if a[i]==b[i]: continue
            return a[i]<b[i]
        return False
    return len(a)<len(b)

def isPrime(n):
    millerRabinTests =  [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]
    if n in millerRabinTests or n in (0, 1):
        return True
    if any((n % p) == 0 for p in millerRabinTests):
        return False
    d, s = n - 1, 0
    while not d % 2:
        d, s = d >> 1, s + 1

    testlim = len(millerRabinTests)
    if n < 3825123056546413051:
        testlim = min(testlim, 9)
    elif n < 341550071728321:
        testlim = 7
    elif n < 3474749660383:
        testlim = 6
    elif n < 2152302898747:
        testlim = 5
    elif n < 3215031751:
        testlim = 4
    elif n < 25326001:
        testlim = 3
    elif n < 1373653:
        testlim = 2

    return not any(tryComposite(a, d, n, s) for a in millerRabinTests[:testlim])

#sL="500"
#sR="10000"
sL,sR = input().strip().split()

L=list(map(lambda c:ord(c)-ord('0'),sL[::-1]))
R=list(map(lambda c:ord(c)-ord('0'),sR[::-1]))
L=initNext(L)
cnt=0
while isLess(L,R):
    il=toInt(L)
    if isPrime(il):
        cnt+=1
        #print(il)
    L=getNextNum(L)
print(cnt)
