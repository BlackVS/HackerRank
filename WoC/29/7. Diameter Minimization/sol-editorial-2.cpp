#include <vector>
#include <cassert>
#include <cstdio>
#include <algorithm>
using namespace std;

int const INF = 1 << 30;

int diam(vector<vector<int> > const &edges, int n) {
  vector<int> d(n);
  vector<int> q(n);
  int ans = 0;
  for (int start = 0; start < n; start++) {
    fill(d.begin(), d.end(), INF);
    d[start] = 0;
    int head = 0;
    int tail = 0;
    q[tail++] = start;
    while (head < tail) {
      int v = q[head++];
      for (int to : edges[v]) {
        if (d[to] > d[v] + 1) {
          d[to] = d[v] + 1;
          q[tail++] = to;
        }
      }
    }
    if (head != n) {
      return -1;
    }
    if (ans < d[q[head - 1]]) {
      ans = d[q[head - 1]];
    }
  }
  return ans;
}

int main() {
  int n, m;
  assert(2 == scanf("%d%d", &n, &m));
  vector<vector<int> > edges(n, vector<int>(m));
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      edges[i][j] = (i * m + j) % n;
    }
  }
  printf("%d\n", diam(edges, n));
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      if (j > 0) putchar(' ');
      printf("%d", edges[i][j]);
    }
    puts("");
  }
}
Statistics
Difficulty: Expert
Time Complexity:
Required Knowledge: BFS
Publish Date: Dec 19 2016
