#!/bin/python3

import sys
from itertools import*

def getdiam(cons):
    n=len(cons)
    dists= [ [n]*n for _ in range(n)]
    for i in range(n):
        dists[i][i]=0
    for i in range(n):
        for j in cons[i]:
            dists[i][j] = 1
    #for d in dists:print(d)
            
    for k in range(n):
        for i in range(n): 
            for j in range(n): 
                if dists[i][j] > dists[i][k] + dists[k][j]: 
                    dists[i][j] = dists[i][k] + dists[k][j]     
    #for d in dists:print(d)
    mx=0
    for i in range(n): 
        for j in range(n): 
            mx=max(mx,dists[i][j])
    return mx
        
n,m = map(int, input().strip().split())
# your code goes here
vrts = [m]*n
edges= []

while any(v>0 for v in vrts):
    #print("v> ", vrts)
    imin=None
    for i in range(len(vrts)):
        if vrts[i]>0 and (imin==None or vrts[i]<vrts[imin]):
            imin=i
    imax=None
    for i in range(len(vrts)-1,-1,-1):
        if vrts[i]>0 and (imax==None or vrts[i]>vrts[imax]):
            imax=i
    
    edges.append((imin, imax))
    edges.append((imax, imin))
    vrts[imin]-=1
    vrts[imax]-=imin!=imax
edges.sort(key=lambda x:x[0])
cons=[ list(map(lambda y:y[1],g)) for k,g in groupby(edges,key=lambda x:x[0])]
print(getdiam(cons))
for c in cons:
    print(" ".join(map(str,c)))

