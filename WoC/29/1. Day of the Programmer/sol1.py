#!/bin/python3

import sys


y = int(input().strip())
if y<1918:
    print("{:02d}.09.{:4d}".format(13-(y%4==0),y))
elif y==1918:
    print("26.09.1918")
else:
    print("{:02d}.09.{:4d}".format(13-( y%400==0 or (y%4==0 and y%100!=0) ),y))