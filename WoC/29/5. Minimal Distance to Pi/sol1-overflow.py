#!/bin/python3

import sys, math
from functools import reduce 
import operator

def solve1(mn,mx):
    pi_q=(3, 7, 15, 1, 292, 1, 1, 1, 2, 1, 3, 1, 14, 2, 1, 1, 2, 2, 2, 2, 1, 84, 2, 1, 1, 15, 3, 13, 1, 4, 2, 6, 6, 99)
    #print(reduce(operator.mul,pi_q,1))
    #pi_q = []
    #pi_c = math.pi
    #for _ in range(15):
    #    q=math.floor(pi_c)
    #    pi_q.append(int(q))
    #    pi_c=1/(pi_c-q)
    i=0
    p0, q0 = 1, 0
    pi, qi = pi_q[i], 1
    pt, qt = 0, 0
    while qi<=mx:
        t1=math.ceil(mn/qi)
        t2=t=mx/qi
        t=0
        if t1>1 and t2>1:
            t=min(t1,t2)
        else:
            t=t1 or t2
        #print(t,t1,t2)
        if t>1 and qi*t<=mx:
            pt, qt = pi*t, qi*t
        #
        p, q = p0+pi*pi_q[i+1], q0+qi*pi_q[i+1]
        if q>mx: break
        p0, pi = pi, p
        q0, qi = qi, q
        i+=1
    #print(">>: ",qi>=mn and qi<=mx,pi,qi,pt,qt)
    if qi>=mn and qi<=mx:
        return (pi,qi)
    return (pt,qt)
    
mn,mx = map(int, input().strip().split())
#
res=solve1(mn,mx)

#print("%i/%i"%(res[0],res[1]),abs(math.pi-res[0]/res[1]))
resd=0
resn=0
resdelta=0
for d in range(res[1],min(res[1]+1000000,mx+1)):
    pid=math.pi*d
    n=round(pid)
    delta=abs(pid-n)
    if not resn or delta<resdelta:
        resn,resd,resdelta=n,d,delta
print("%i/%i"%(resn,resd))