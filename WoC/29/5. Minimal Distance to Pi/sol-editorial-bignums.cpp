#include <cstdio>
#include <stack>
#include <set>

const __float128& pi128() {
    static __float128 _1e16 = 10LL * 1000 * 1000 * 1000 * 1000 * 1000;
    static __float128 pi = (31415926535897932LL * _1e16 + 3846264338327950LL) / (_1e16 * _1e16);
    return pi;
}

long long getNum(long long den) {
    static __float128 pi = pi128();
    return (long long)(pi * den + 0.5); // floor(x + 0.5) == round(x) for x > 0
}

__float128 distToPi(long long den) {
    static __float128 pi = pi128();
    long long num = getNum(den);
    __float128 dist = pi - (__float128)num / den;
    if (dist < 0) {
        dist = -dist;
    }
    return dist;
}

struct Item {

    long long den;

    __float128 dist;
};

int main() {
    std::stack<Item> stack;
    std::set<long long> diffs;
    long long START = (long long)1e14;
    long long LEN = (long long)1e7;
    for (long long den = START; den < START + LEN; den++) {
        __float128 dist = distToPi(den);
        while (!stack.empty() && dist < stack.top().dist) {
            long long diff = den - stack.top().den;
            if (diffs.count(diff) == 0) {
                diffs.insert(diff);
            }
            stack.pop();
        }
        stack.push(Item{ den, dist });
    }
    printf("diffs(%d):", (int)diffs.size());
    for (long long diff : diffs) {
        printf(" %lld", diff);
    }
    return 0;
}