#!/bin/python3

import sys

def squareOfTriangle(x1,y1,x2,y2,x3,y3):
    return abs( (x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2) ) /2 )

def isRect(x,y,x1,y1,x2,y2,x3,y3,x4,y4):
    res=    squareOfTriangle(x,y,x1,y1,x2,y2)+\
            squareOfTriangle(x,y,x2,y2,x3,y3)+\
            squareOfTriangle(x,y,x3,y3,x4,y4)+\
            squareOfTriangle(x,y,x4,y4,x1,y1)
    return res<= (x1-x2)**2+(y1-y2)**2

def isCircle(x,y,cx,cy,cr):
    return abs(complex(cx-x,cy-y))<=cr

w,h = map(int, input().strip().split())
cX,cY,cR = map(int, input().strip().split())

x1,y1,x3,y3 = map(int, input().strip().split())
xc,yc = (x1+x3)/2,(y1+y3)/2
x2,y2 = xc+(y1-y3)/2,yc+(x3-x1)/2
x4,y4 = xc-(y1-y3)/2,yc-(x3-x1)/2

canvas=[ [(".","#")[isRect(x,y,x1,y1,x2,y2,x3,y3,x4,y4) or isCircle(x,y,cX,cY,cR)] for x in range(w)] for y in range(h)]
for c in canvas:
    print("".join(c))