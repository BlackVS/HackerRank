#pragma GCC diagnostic ignored "-Wunused-result"

#include <cstdio>
#include <cmath>

struct Point {

    double i;
    
    double j;

    Point(const double i, const double j)
        : i(i)
        , j(j)
    {}
};

struct Circle {

    Point center;
    
    int r;

    Circle(const Point &center, const int r)
        : center(center)
        , r(r)
    {}

    static Circle read() {
        int j, i, r;
        scanf("%d %d %d", &j, &i, &r);
        return Circle(Point(i, j), r);
    }

    bool contains(int i, int j) const {
        double di = i - center.i;
        double dj = j - center.j;
        return di * di + dj * dj <= r * r;
    }
};

struct Triangle {

    Point p1;

    Point p2;

    Point p3;
    
    Triangle(const Point &p1, const Point &p2, const Point &p3)
        : p1(p1)
        , p2(p2)
        , p3(p3)
    {}

    bool contains(Point p) const {
        return this->area() == Triangle(p1, p2, p).area() + Triangle(p2, p3, p).area() + Triangle(p3, p1, p).area();
    }

    double area() const {
        return std::abs((p3.i - p1.i) * (p2.j - p1.j) - (p3.j - p1.j) * (p2.i - p1.i)) / 2;
    }
};

struct Square {

    Point p1;

    Point p2;

    Point p3;

    Point p4;

    Square(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
        : p1(p1)
        , p2(p2)
        , p3(p3)
        , p4(p4)
    {}

    static Square read() {
        int j1, i1, j3, i3;
        scanf("%d %d %d %d", &j1, &i1, &j3, &i3);
        double ci = (i1 + i3) / 2.0;
        double cj = (j1 + j3) / 2.0;
        double di = i1 - ci;
        double dj = j1 - cj;
        double pi = -dj;
        double pj = di;
        // pi * di + pj * dj == 0 -> perpendicular
        double i2 = ci + pi;
        double j2 = cj + pj;
        double i4 = ci - pi;
        double j4 = cj - pj;
        return Square(Point(i1, j1), Point(i2, j2), Point(i3, j3), Point(i4, j4));
    }

    bool contains(int i, int j) const {
        return Triangle(p1, p3, p2).contains(Point(i, j)) || Triangle(p1, p3, p4).contains(Point(i, j));
    }
};

int main() {
    int sizeJ, sizeI;
    scanf("%d %d", &sizeJ, &sizeI);
    Circle circle = Circle::read();
    Square square = Square::read();
    for (int i = 0; i < sizeI; i++) {
        for (int j = 0; j < sizeJ; j++) {
            if (circle.contains(i, j) || square.contains(i, j)) {
                printf("#");
            } else {
                printf(".");
            }
        }
        printf("\n");
    }
    return 0;
}
