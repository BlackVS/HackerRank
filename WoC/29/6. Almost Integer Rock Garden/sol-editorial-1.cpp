#pragma GCC diagnostic ignored "-Wunused-result"

#include <cstdio>
#include <vector>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <set>
#include <utility>

bool isPerfectSquare(int n) {
    assert(n >= 0);
    int r = (int)std::round(std::sqrt(n));
    return r * r == n;
}

const std::vector<int>& sortedDistSquares() {
    static std::vector<int> distSquares;
    if (distSquares.empty()) {
        for (int i = -12; i <= 12; i++) {
            for (int j = -12; j <= 12; j++) {
                int d2 = i * i + j * j;
                if (!isPerfectSquare(d2)) {
                    distSquares.push_back(d2);
                }
            }
        }
        fprintf(stderr, "the number of points with non-integer coordinates is %d\n", (int)distSquares.size());
        std::sort(distSquares.begin(), distSquares.end());
        distSquares.erase(std::unique(distSquares.begin(), distSquares.end()), distSquares.end());
    }
    fprintf(stderr, "the number of distances is %d\n", (int)distSquares.size());
    assert(distSquares.size() == 68);
    return distSquares;
}

void precalcSlowRec(
    const int cur,
    const int from,
    const double sum,
    std::vector<int> &index,
    std::vector<int> &count,
    const std::vector<double> &fracs,
    std::vector<std::vector<int>> &ans,
    std::vector<bool> &isMentioned,
    int &nFilled
) {
    const int MAX_EQUAL_COUNT = 4;
    static int updates = 0;
    static long long ops = 0;
    int n = (int)fracs.size();
    if (cur + 3 == 12) { // the last 3 loops without recursion overhead
        for (int i = from; i < n; i++) {
            if (count[i] == MAX_EQUAL_COUNT) {
                continue;
            }
            count[i]++;
            index[9] = i;
            double sumI = sum + fracs[i];
            for (int j = i; j < n; j++) {
                if (count[j] == MAX_EQUAL_COUNT) {
                    continue;
                }
                count[j]++;
                index[10] = j;
                double sumJ = sumI + fracs[j];
                for (int k = j; k < n; k++) {
                    if (count[k] == MAX_EQUAL_COUNT) {
                        continue;
                    }
                    double sum = sumJ + fracs[k];
                    double d = std::abs(sum - std::round(sum));
                    ops++;
                    if (d < 0.9e-12) {
                        index[11] = k;
                        updates++;
                        fprintf(stderr, "found:");
                        for (int x : index) {
                            fprintf(stderr, " %d", x);
                        }
                        fprintf(stderr, "\n");
                        int oldFilled = nFilled;
                        for (int idx : index) {
                            if (!isMentioned[idx]) {
                                isMentioned[idx] = true;
                                nFilled++;
                            }
                        }
                        if (nFilled != oldFilled) {
                            ans.push_back(index);
                        }
                        if (nFilled == n) {
                            return;
                        }
                    }
                }
                count[j]--;
            }
            count[i]--;
        }
    } else {
        if (cur == 5) {
            fprintf(stderr, "[%d/%d/%lld] ", nFilled, updates, ops);
            for (int i = 0; i < 5; i++) {
                fprintf(stderr, "%d ", index[i]);
            }
        }
        if (cur == 6) {
            fprintf(stderr, "*");
        }
        for (int i = from; i < n; i++) {
            if (count[i] == MAX_EQUAL_COUNT) {
                continue;
            }
            count[i]++;
            index[cur] = i;
            precalcSlowRec(cur + 1, i, sum + fracs[i], index, count, fracs, ans, isMentioned, nFilled);
            if (nFilled == n) {
                return;
            }
            count[i]--;
        }
        if (cur == 5) {
            fprintf(stderr, "\n");
        }
    }
}

// measured speed: 10^8 ops/s
// expected time: several days, less if parallelized
void precalcSlow() {
    std::vector<double> fracs;
    for (int square : sortedDistSquares()) {
        double sqrt = std::sqrt(square);
        fracs.push_back(sqrt - std::round(sqrt));
    }
    std::vector<std::vector<int>> ans;
    std::vector<int> index(12);
    std::vector<int> count(fracs.size(), 0);
    std::vector<bool> isMentioned(fracs.size(), false);
    int nFilled = 0;
    precalcSlowRec(0, 0, 0, index, count, fracs, ans, isMentioned, nFilled);
    printf("std::vector<std::vector<int>> groups = {\n");
    for (int i = 0; i < (int)ans.size(); i++) {
        assert(ans[i].size() == 12);
        printf("\t{");
        double sum = 0;
        for (int j = 0; j < (int)ans[i].size(); j++) {
            if (j > 0) {
                printf(", ");
            }
            printf("%d", ans[i][j]);
            sum += fracs[ans[i][j]];
        }
        sum = sum - std::round(sum);
        printf("}");
        if (i != (int)ans.size() - 1) {
            printf(",");
        }
        printf(" // %.1e", sum);
        printf("\n");
    }
    printf("};\n");
    fprintf(stderr, "precalcSlow complete\n");
    std::exit(0);
}

struct Item {
    double sum;
    char indexes[6];
};

inline bool isPairValid(const Item &first, const Item &second) {
    double sum = first.sum + second.sum;
    double d = std::abs(sum - std::round(sum));
    if (d < 0.9e-12) {
        std::vector<int> count;
        for (int v : first.indexes) {
            while (v >= (int)count.size()) {
                count.push_back(0);
            }
            if (count[v] == 4) {
                return false;
            }
            count[v]++;
        }
        for (int v : second.indexes) {
            while (v >= (int)count.size()) {
                count.push_back(0);
            }
            if (count[v] == 4) {
                return false;
            }
            count[v]++;
        }
        return true;
    }
    return false;
}

void addPairsToAns(const Item &first, const Item &second, std::vector<bool> &present, std::vector<std::vector<int>> &ans) {
    bool isNew = false;
    for (int v : first.indexes) {
        if (!present[v]) {
            isNew = true;
        }
    }
    for (int v : second.indexes) {
        if (!present[v]) {
            isNew = true;
        }
    }
    if (!isNew) {
        return;
    }
    ans.push_back(std::vector<int>());
    for (int v : first.indexes) {
        present[v] = true;
        ans.back().push_back(v);
    }
    for (int v : second.indexes) {
        present[v] = true;
        ans.back().push_back(v);
    }
    std::sort(ans.back().begin(), ans.back().end());
}

// ~3 G memory + ~30 seconds
void precalcFast() {
    std::vector<double> fracs;
    for (int square : sortedDistSquares()) {
        double sqrt = std::sqrt(square);
        fracs.push_back(sqrt - std::round(sqrt));
    }
    int n = (int)fracs.size();
    int size = 0;
    for (int i0 = 0; i0 < n; i0++) {
        for (int i1 = i0; i1 < n; i1++) {
            for (int i2 = i1; i2 < n; i2++) {
                for (int i3 = i2; i3 < n; i3++) {
                    for (int i4 = i3; i4 < n; i4++) {
                        for (int i5 = i4; i5 < n; i5++) {
                            size++;
                        }
                    }
                }
            }
        }
    }
    std::vector<Item> items;
    items.reserve(size); // more efficient memory use. 170,340,452 * 16 = 2.7G
    for (int i0 = 0; i0 < n; i0++) {
        for (int i1 = i0; i1 < n; i1++) {
            for (int i2 = i1; i2 < n; i2++) {
                for (int i3 = i2; i3 < n; i3++) {
                    for (int i4 = i3; i4 < n; i4++) {
                        for (int i5 = i4; i5 < n; i5++) {
                            double sum = fracs[i0] + fracs[i1] + fracs[i2] + fracs[i3] + fracs[i4] + fracs[i5];
                            sum -= std::floor(sum);
                            assert(0 <= sum && sum < 1);
                            items.push_back(Item{ sum, (char)i0, (char)i1, (char)i2, (char)i3, (char)i4, (char)i5 });
                        }
                    }
                }
            }
        }
    }
    printf("filled, sorting...\n");
    std::sort(items.begin(), items.end(), [](const Item &left, const Item &right) {
        return left.sum < right.sum;
    });
    printf("sorted\n");
    std::vector<bool> present(n, false);
    std::vector<std::vector<int>> ans;
    int j = (int)items.size() - 1;
    int found = 0;
    for (int i = 0; i < (int)items.size(); i++) {
        while (j > 0 && items[i].sum + items[j - 1].sum > 1) {
            j--;
        }
        if (isPairValid(items[i], items[j])) {
            found++;
            addPairsToAns(items[i], items[j], present, ans);
        }
        if (j - 1 >= 0 && isPairValid(items[i], items[j - 1])) {
            found++;
            addPairsToAns(items[i], items[j - 1], present, ans);
        }
    }
    int notPresent = 0;
    for (bool p : present) {
        if (!p) {
            notPresent++;
        }
    }
    printf("found pairs %d not present values %d\n", found, notPresent);
    printf("std::vector<std::vector<int>> groups = {\n");
    for (int i = 0; i < (int)ans.size(); i++) {
        assert(ans[i].size() == 12);
        printf("\t{");
        double sum = 0;
        for (int j = 0; j < (int)ans[i].size(); j++) {
            if (j > 0) {
                printf(", ");
            }
            printf("%d", ans[i][j]);
            sum += fracs[ans[i][j]];
        }
        sum = sum - std::round(sum);
        printf("}");
        if (i != (int)ans.size() - 1) {
            printf(",");
        }
        printf(" // %.1e", sum);
        printf("\n");
    }
    printf("};\n");
    fprintf(stderr, "precalcFast complete\n");
    std::exit(0);
}

std::vector<std::vector<int>> groups = {
    {4, 8, 13, 25, 27, 28, 37, 43, 45, 49, 51, 65}, // 8.0e-013
    {4, 6, 10, 19, 30, 33, 41, 43, 47, 59, 59, 59}, // -9.6e-014
    {12, 15, 23, 45, 47, 48, 52, 54, 57, 65, 65, 65}, // -3.6e-013
    {9, 15, 26, 31, 31, 33, 34, 34, 37, 41, 60, 63}, // 3.2e-013
    {1, 4, 19, 26, 30, 32, 33, 43, 47, 59, 59, 59}, // -9.6e-014
    {2, 3, 6, 31, 31, 31, 45, 46, 50, 52, 52, 54}, // -2.0e-013
    {18, 18, 28, 30, 31, 40, 42, 44, 48, 49, 61, 66}, // -4.7e-013
    {11, 12, 29, 31, 35, 37, 43, 44, 44, 57, 60, 64}, // -8.3e-013
    {2, 3, 6, 7, 8, 20, 25, 26, 34, 39, 43, 62}, // -4.7e-013
    {9, 16, 16, 31, 31, 33, 34, 34, 37, 56, 56, 63}, // 3.2e-013
    {3, 4, 11, 21, 22, 34, 34, 34, 35, 42, 47, 54}, // 2.8e-013
    {9, 10, 21, 21, 24, 25, 30, 38, 41, 44, 48, 62}, // 2.3e-013
    {4, 28, 28, 33, 40, 40, 41, 45, 54, 54, 55, 66}, // 1.5e-013
    {11, 12, 29, 31, 35, 37, 43, 44, 44, 53, 57, 67}, // -8.4e-013
    {0, 3, 4, 8, 10, 17, 20, 25, 34, 43, 56, 62}, // -4.7e-013
    {3, 14, 21, 21, 36, 36, 37, 37, 44, 54, 62, 66}, // 6.9e-013
    {5, 13, 16, 22, 30, 31, 31, 31, 45, 46, 52, 54}, // -2.0e-013
    {18, 21, 22, 26, 45, 46, 51, 53, 58, 65, 65, 65} // 3.6e-013
};

void solve() {
    int x, y;
    scanf("%d %d", &x, &y);
    int d2 = x * x + y * y;
    const std::vector<int> &squares = sortedDistSquares();
    auto it = std::find(squares.begin(), squares.end(), d2);
    assert(it != squares.end());
    int index = (int)(it - squares.begin());
    std::set<std::pair<int, int>> used;
    used.insert(std::make_pair(x, y));
    double sum = std::sqrt(x * x + y * y);
    for (auto group : groups) {
        it = std::find(group.begin(), group.end(), index);
        if (it != group.end()) {
            group.erase(it);
            for (int v : group) {
                int square = squares[v];
                bool found = false;
                for (int xx = -12; xx <= 12 && !found; xx++) {
                    for (int yy = -12; yy <= 12 && !found; yy++) {
                        if (xx * xx + yy * yy == square && used.count(std::make_pair(xx, yy)) == 0) {
                            printf("%d %d\n", xx, yy);
                            used.insert(std::make_pair(xx, yy));
                            found = true;
                            sum += std::sqrt(xx * xx + yy * yy);
                        }
                    }
                }
                assert(found);
            }
            fprintf(stderr, "sum=%.14f diff=%.1e\n", sum, std::abs(sum - std::round(sum)));
            return;
        }
    }
    assert(false); // have not found the index in any group
}

int main() {
    // precalcSlow();
    // precalcFast();
    solve();
    return 0;
}