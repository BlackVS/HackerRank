#!/bin/python3

import sys
from math import*
from collections import*
from itertools import*

def dist(x,y):
    return int(modf(hypot(x,y))[0]*10**12)

BASE=[38404810405, 45361017187, 165525060596, 403124237432, 123105625617, 246211251235, 440306508910, 211102550927, 605551275463, 892443989449, 45361017187, 71067811865, 414213562373, 162277660168, 162277660168, 324555320336, 162277660168, 162277660168, 324555320336]
BASEDS2={970562748477: (414213562373, 556349186104),\
         556349186104: (242640687119, 313708498984),\
         899494936611: (414213562373, 485281374238),\
         944271909999: (236067977499, 708203932499),\
         416407864998: (180339887498, 236067977499),\
         656854249492: (242640687119, 414213562373),\
         727922061357: (313708498984, 414213562373),\
         649110640673: (162277660168, 486832980505),\
         816653826391: (211102550927, 605551275463),\
         313708498984: (71067811865 , 242640687119),\
         486832980505: (162277660168, 324555320336),\
         708203932499: (236067977499, 472135954999),\
         369316876852: (123105625617, 246211251235),\
         485281374238: (71067811865, 414213562373)}

xin,yin=map(int,input().split())
d=[dist(xin,yin)]

f=True
while f:
    f=False
    r=[]
    for x in d:
        if x in BASEDS2:
            r.append(BASEDS2[x][0])
            r.append(BASEDS2[x][1])
            f=True
        else:
            r.append(x)
    d=r

rset=list(BASE)
for x in d:
    if x in rset:
        rset.remove(x)

#print(rset)
#join to 11
f=True
while len(rset)>11 and f:
    f=False
    for x in BASEDS2.items():
        if x[1][0] in rset and x[1][1] in rset:
            rset.remove(x[1][0])
            rset.remove(x[1][1])
            rset.append(x[0])
            f=True
#print(rset)

#restore
for x in range(-12,12+1):
    if len(rset)==0: break
    for y in range(-12,12+1):
        d=dist(x,y)
        if d in rset and x!=xin and y!=yin:
            rset.remove(d)
            print(x,y)
            if len(rset)==0: break