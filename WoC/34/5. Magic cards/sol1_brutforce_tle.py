#!/bin/python3
import sys
from collections import *
from itertools import *
#sys.stdin = open('G:\hackerrank\WoC\\34\\5. Magic cards\sol1\\t0.txt', 'r')

def solve():
    N,M,Q=map(int,input().split())
    C0=[]
    C1=[]
    CFULL=set(map(lambda x:x*x,range(1,M+1)))
    CMAX=sum(CFULL)
    CS=[]
    cnt=Counter()
    for _ in range(N):
        ci=map(int,input().split())
        next(ci)
        c0=set(c*c for c in ci)
        c1=CFULL-c0
        #cs=sum(c0)
        #if CMAX-c0>c0:
        #    c0,c1=c1,c0
        C0.append(c0)
        C1.append(c1)
        cnt+=Counter(c0)
        CS.append(cnt.copy())

    for _ in range(Q):
        l,r=map(lambda x: int(x)-1,input().split())
        resC=CS[r]
        if l>0: resC-=CS[l-1]
        resS=sum(resC.keys())
        F=[False]*(r-l+1)
        flipped=True
        while flipped and resS<CMAX:
            flipped=False
            for i in range(l,r+1):
                if F[i-l]: continue
                #try flip
                cur,nxt=C0[i],C1[i]
                delta=0
                #check if sum decreased
                for c in cur:
                    if resC[c]==1: delta-=c
                #check if sum increased
                for c in nxt:
                    if not resC[c]: delta+=c
                if delta>0:
                    F[i-l]=not F[i-l]
                    resC-=Counter(cur)
                    resC+=Counter(nxt)
                    resS+=delta
                    flipped=True
                if resS==CMAX: break
        print(resS)
    return 

solve()