#!/bin/python3
import sys,copy
from collections import *
from itertools import *
sys.stdin = open('G:\hackerrank\WoC\\34\\6. Path statistics\\t1.txt')

class RMQ(object):
    def __init__(self, a, fn):
        self._a=a
        self._n=n=len(a)
        self._logtable=_logtable=[0]*(n+1)
        for i in range(2,n+1):
            _logtable[i]=_logtable[i>>1]+1
        
        self._rmq = rmq = [ [0]*n for _ in range(_logtable[n] + 1)]

        for i in range(n):
            rmq[0][i] = i

        k=1
        while (1 << k) < n:
            i=0
            while i + (1 << k) <= n:
                x = rmq[k - 1][i];
                y = rmq[k - 1][i + (1 << k - 1)];
                rmq[k][i] = x if a[x] <= a[y] else y
                i+=1
            k+=1

    def query(self, i, j):
        k = self._logtable[j - i]
        x = self._rmq[k][i]
        y = self._rmq[k][j - (1 << k) + 1]
        return x if self._a[x] <= self._a[y] else y

class Graph(object):
    def __init__(self,n):
        self.VN=n
        self.G = [[] for _ in range(n)]
        self.W = [0]*n
        # DFS BFS
        self.NR  = None
        self.PR  = None
        #self.D   = None
        self.PTHC= None
        self.RMQ_L = None #depth-level for RMQ
        self.RMQ_E = None #order for RMQ

    
    def add_edge(self,x,y):
        self.G[x].append( y )
        self.G[y].append( x )

    def setVWeights(self,itw):
        self.W=tuple(itw)

    def DFS(self,root=0):
        N =self.VN
        W =self.W
        NR  = self.NR  = [-1]*N
        P   = self.PR  = [None]*N
        PTHC= self.PTHC= [None]*N
        #RMQ
        #D  = self.D   = [-1]*N
        L  = self.RMQ_L = [-1]*(2*N-1)
        E  = self.RMQ_E = [-1]*(2*N-1)
        #
        GU=self.G #no need to save G
        idx=0
        v=root
        NR[v]=idx
        E[idx]=v
        L[idx]=0
        idx+=1
        PTHC[v]=Counter()
        PTHC[v][W[v]]+=1
        while True:
            while GU[v]:
                w=GU[v].pop()
                if NR[w]==-1:
                    P[w]=v
                    PTHC[w]=PTHC[v]+Counter( [W[w]] )
                    NR[w]=idx
                    E[idx]=w
                    L[idx]=L[ NR[v] ]+1
                    idx+=1
                    v=w
            #we back to v
            #update parent
            #if v!=root:
            if v==root and not GU[v]:
                break;
            v=P[v]
            E[idx]=v
            L[idx]=L[ NR[v] ]
            idx+=1
        #init RMQ
        self.rmq_lca=RMQ(self.RMQ_L,min)
        return 0

    def BFS(self,root=0):
        N =self.VN
        W =self.W
        P  = self.PR  = [None]*N
        D  = self.D   = [0]*N
        #PTH= self.PATH= [ [] for _ in range(N) ]
        G=self.G #no need to save G
        i=0
        v=root
        D[root]=0
        q=deque([root])
        while q:
            v=q.popleft()
            pv=P[v]
            dv=D[v]
            for w in G[v]:
                if w==pv:continue
                P[w]=v
                D[w]=dv+1
                q.append(w)

    def getLCA(self,u,v):
        idx_u=self.NR[u]
        idx_v=self.NR[v]
        idx_u,idx_v=min(idx_u,idx_v),max(idx_u,idx_v)
        #get dmin depth
        idx_lmin=self.rmq_lca.query(idx_u,idx_v)
        idx_nmin=self.RMQ_E[idx_lmin]
        return idx_nmin

    def calcPath(self,u,v):
        u,v=min(u,v),max(u,v)
        c=self.getLCA(u,v)
        W=self.W
        P=self.PR
        
        s=self.PTHC[u]+self.PTHC[v]-self.PTHC[c]-self.PTHC[c]
        s[self.W[c]]+=1

        slist=list(s.items())
        s=None
        slist.sort(key=lambda x: x[1]*(self.VN+1)+x[0],reverse=True)
        return slist
        #return None

    def calcCenter(self,root=0):
        N =self.VN
        W =self.W
        G=self.G 
        #pass1
        P =[-1]*N
        D =[-1]*N
        D[root]=0
        q = deque([root])
        vmax=root
        dmax=0
        while q:
            v=q.popleft()
            pv=P[v]
            dv=D[v]
            for w in G[v]:
                if w==pv:continue
                P[w]=v
                D[w]=dv+1
                q.append(w)
                if D[w]>dmax:
                    dmax=D[w]
                    vmax=w
        #pass2
        root=vmax
        P =[-1]*N
        D =[-1]*N
        D[root]=0
        q = deque([root])
        vmax=root
        dmax=0
        while q:
            v=q.popleft()
            pv=P[v]
            dv=D[v]
            for w in G[v]:
                if w==pv:continue
                P[w]=v
                D[w]=dv+1
                q.append(w)
                if D[w]>dmax:
                    dmax=D[w]
                    vmax=w
        dmax//=2
        for i in range(dmax):
            vmax=P[vmax]
        return vmax

def solve():
    N, Q = map(int, input().split())
    G = Graph(N)
    G.setVWeights(map(int,input().split()))
    for _ in range(N-1):
        u,v=map(lambda x:int(x)-1,input().split())
        G.add_edge(u,v)

    G.DFS(G.calcCenter())

    QL=[]
    for i in range(Q):
        u,v,k=map(lambda x:int(x)-1,input().split())
        u,v=min(u,v),max(u,v)
        QL.append( (u,v,k,i) )
        
    QL.sort(key=lambda x: x[0]+x[1]*(N+1))
    #print(QL)
    RES=[0]*Q
    
    sres=None
    up=vp=None
    for u,v,k,i in QL:
        res=0
        if sres and u==up and v==vp:
            res=sres[k][0]
        else:
            #if u!=up: G.DFS(u)
            sres=G.calcPath(u,v)
            if sres:
                res=sres[k][0] if k<len(sres) else 0
        RES[i]=res
        up,vp=u,v
    for r in RES:
        print(r)

solve()
