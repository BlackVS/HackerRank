#!/bin/python3

import sys

def isLucky(n):
    n,a0=divmod(n,10)
    n,a1=divmod(n,10)
    n,a2=divmod(n,10)
    n,a3=divmod(n,10)
    a5,a4=divmod(n,10)
    return a0+a1+a2==a3+a4+a5

def onceInATram(x):
    x+=1
    while not isLucky(x):
        x+=1
    return x

if __name__ == "__main__":
    x = int(input().strip())
    result = onceInATram(x)
    print(result)
