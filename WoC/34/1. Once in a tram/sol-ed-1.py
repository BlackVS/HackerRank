def formata(n):
    s = str(n)
    s = "0"*(6-len(s)) + s
    return s

def fun(n):
    s = formata(n)
    if sum(map(int,list(s[0:3]))) == sum(map(int,list(s[3:]))):
        return True
    return False

n = int(raw_input()) + 1
while(not fun(n)):
    n += 1
print formata(n)