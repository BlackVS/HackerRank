#!/bin/python3
import sys,copy
from collections import *
from itertools import *
#sys.stdin = open('G:\hackerrank\WoC\\34\\4. Recurrent on tree\sol4\\t2.txt', 'r')

MODN=1000000000+7
PIS =2000000016
MAXFS=10000000

class Graph(object):
    def __init__(self,n):
        self.VN=n
        self.G = [[] for _ in range(n)]
        self.W = [0]*n
        # DFS
        self.DFS_NR  = None
        self.DFS_PR  = None
        #fibo
        self.FMEM=dict()
        self.FS=FS=[0]*MAXFS
        FS[0]=1
        FS[1]=1
        for i in range(2,len(FS)):
            FS[i]=FS[i-1]+FS[i-2]
            if FS[i]>=MODN: FS[i]-=MODN

    def fib(self,n):
        if n<MAXFS:
            return self.FS[n]
        n+=1
        res=self.FMEM.get(n,None)
        if res:
            return res
        if n>=PIS: 
            n%=PIS
        v1, v2, v3 = 1, 1, 0    # initialise a matrix [[1,1],[1,0]]
        for rec in bin(n)[3:]:  # perform fast exponentiation of the matrix (quickly raise it to the nth power)
            calc = v2*v2
            if calc>=MODN: calc%=MODN
            v1, v2, v3 = v1*v1+calc, (v1+v3)*v2, calc+v3*v3
            if v1>=MODN: v1%=MODN
            if v2>=MODN: v2%=MODN
            if v3>=MODN: v3%=MODN
            if rec=='1':    
                v1, v2, v3 = v1+v2, v1, v2
                if v2>=MODN: v2%=MODN
        res=v2
        self.FMEM[n]=res
        return res
    
    def add_edge(self,x,y):
        self.G[x].append( y )
        self.G[y].append( x )

    def setVWeights(self,itw):
        self.W=tuple(itw)

    def DFS(self,root=0):
        self.DFS_NR  = [-1]*self.VN
        self.DFS_PR  = [None]*self.VN
        NR=self.DFS_NR
        P =self.DFS_PR
        GU=copy.deepcopy(self.G)
        N =self.VN
        W =self.W
        i=0
        v=root
        NR[v]=i
        #for each node R[v] is counter or all rooted lengts in subtree V rooted from v
        R=[Counter( {W[i]:1} ) for i in range(N)]
        #for each node store sum of all unrooted paths, F(g)!
        FU=[0]*N
        while True:
            while GU[v]:
                w=GU[v].pop()
                if w==P[v]: continue
                if NR[w]==-1:
                    P[w]=v
                    i+=1
                    NR[w]=i
                    v=w
            #we back to v
            #calculate it's rooted/unrooted
            #combine childs if exists - take all rooted childs and combine in u-1-v
            #for this check all children
            childs=[c for c in self.G[v] if P[c]==v]
            lc=len(childs)
            if lc>1:
                #combine childs rooted as pairs and remember as unrooted for v
                COMB=Counter()
                for iu in range(lc-1):
                    for iv in range(iu+1,lc):
                        cu,cv=childs[iu],childs[iv]
                        #print(cu,cv)
                        #print(R[cu],R[cv])
                        for ru,cntu in R[cu].items():
                            for rv,cntv in R[cv].items():
                                #print(ru,cntu,rv,cntv)
                                COMB[ru+W[v]+rv]+=cntu*cntv
                #U[v]+=COMB
                for w,c in COMB.items():
                    FU[v]+=self.fib(w)*c
                    if FU[v]>=MODN: FU[v]%=MODN

            #update parent
            if v!=root:
                p =P[v]
                wp=W[p]
                #all V unrooted going directly to unrooted parent 
                FU[p]+=FU[v]
                #all V rooted can be as unrooted for parent
                for w,c in R[v].items():
                    FU[p]+=self.fib(w)*c
                    if FU[p]>=MODN: FU[p]%=MODN

                #all V rooted can be as 1+rooted for parent, twice - to and back
                rp=Counter({x[0]+wp: x[1]   for x in R[v].items()})
                R[p]+=rp
            if v==root and not GU[v]:
                break;
            v=P[v]
        #R[root],U[root] counts all paths! but only once
        RESU=FU[root]
        RESR=0
        for w,c in R[root].items():
            RESR+=self.fib(w)*c
            if RESR>=MODN: RESR%=MODN
        RESW=0
        for w in W:
            RESW+=self.fib(w)
            if RESW>=MODN: RESW%=MODN
        return (RESU*2+RESR*2-RESW)%MODN

def solve():
    N = int(input())
    G = Graph(N)
            
    for _ in range(N-1):
        a,b=map(int,input().split())
        G.add_edge(a-1,b-1)
    G.setVWeights(map(int,input().split()))

    res=G.DFS()
    return res

print(solve()) 
