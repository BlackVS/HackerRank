#!/bin/python3

import sys

MINV,MAXV=1,1000000
MINN,MAXN=1, 500000

def solve():
    minA,maxA=MAXV+1,MINV-1
    minB,maxB=MAXV+1,MINV-1
    
    PA=[0]*(MAXV+1)
    for a in map(int, input().strip().split()):
        minA,maxA=min(minA,a),max(maxA,a)
        PA[a]=1
            
    PB=[0]*(MAXV+1)
    for b in map(int, input().strip().split()):
        minB,maxB=min(minB,b),max(maxB,b)
        PB[b]=1

    res=0
    maxG=min(maxA,maxB)
    for g in range(maxG,MINV-1,-1):
        ra,rb=0,0
        
        ma=(maxA//g)*g
        while ma>=minA:
            if PA[ma]:
                ra=ma
                break
            ma-=g
            
        mb=(maxB//g)*g
        while mb>=minB:
            if PB[mb]:
                rb=mb
                break
            mb-=g
            
        if ra and rb:
            res=ra+rb
            break
            
    return res

if __name__ == "__main__":
    N = int(input().strip())
    print(solve())