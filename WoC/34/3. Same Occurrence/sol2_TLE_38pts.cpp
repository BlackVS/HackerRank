#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <set>
#include <map>
#include <utility>
#include <unordered_map>

#define MAXN 8192
#define FWTYPE short
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

/*inline short getFW(FWTYPE* FW,short a, short i, short N) {
	if(a < 0) return N;
	int res = *(FW + ((int)a*MAXN) + ((int)i));
	return res ? res : N;
}
#define GETFW(a,i) getFW(FW,a,i,N)*/
#define XYKEY(x,y) ((((x)+1)<<13)+(y))

int     A [MAXN];
bool    V [MAXN];
FWTYPE  FC[MAXN];
FWTYPE  FW[MAXN][MAXN];

std::unordered_map<int,int> MEMO;

int main() {
	int N, Q;
	//scanf("%i %i", &N, &Q);
    std::ios::sync_with_stdio(false);
	std::cin>>N>>Q;	
    //
	std::set<int>     S;
	std::map<int, int> A2I;
	std::map<int, int>::iterator A2I_iter;
	int a;
	for (int i = 0; i < N; i++) {
		//scanf("%i", &a);
        std::ios::sync_with_stdio(false);
        std::cin>>a;
		A[i] = a;
		S.insert(a);
	}
	int M = S.size();
	int i = 0;
	for (auto s : S) {
		A2I[s] = i;
		i++;
	}

    //not good idea to comment - don't do in production!!!
    //memset(FW, 0, M*sizeof(FW[0]));
	//memset(FC, 0, sizeof(FC));
	for (int i = 0; i < N; i++) {
		a = A2I[A[i]];
		A[i] = a;
		int ip = FC[a];
        for (int j=ip; j < i; j++)
            FW[a][j] = i;
		FC[a] = i;
	}

	for (int q = 0; q < Q; q++) {
		int _x,_y,res=0;
		//scanf("%i %i", &x, &y);
        std::ios::sync_with_stdio(false);
        std::cin>>_x>>_y;
		A2I_iter= A2I.find(_x); 
		if (A2I_iter == A2I.end())
			_x = -1;
		else
			_x = A2I_iter->second;
		A2I_iter = A2I.find(_y);
		if (A2I_iter == A2I.end())
			_y = -1;
		else
			_y = A2I_iter->second;
		if (_y < _x) {
			std::swap(_x, _y);
		}
        short x=_x;
        short y=_y;
        //MEMO
        int xy=XYKEY(x,y);
		if (MEMO.count(xy)) {
			printf("%d\n", MEMO[xy]);
			continue;
		}
		//test boundary
		if(x==y){
			res = (N*(N + 1)) / 2;
			printf("%d\n", res);
			MEMO[xy] = res;
			continue;
		}
        if(x<0){
            res=0;
            i=0;
            while(i<N)
            {
                short ni =FW[y][i];
                if(!ni) ni=N;
                //
                short cnt=ni-i;
                if(A[i]==y)
                    cnt-=1;
                res+=(cnt*(cnt+1))/2;
                i=ni;
            }
			printf("%d\n", res);
			MEMO[xy] = res;
			continue;
        }
		memset(V, 0, sizeof(V));
		short i = 0;
		while (i < N) {
			if (V[i]) {
				i++;
				continue;
			}
			short cnt = 0;
			if (A[i] != x && A[i] != y) {
				short nix = FW[x][i]; if(!nix)nix=N;
                short niy = FW[y][i]; if(!niy)niy=N;
                short ni=MIN(nix,niy);
				cnt = ni - i;
				i = ni;
			}
			short j = i;
			while (j < N) {
				//non xy block
				if (A[j] != x && A[j] != y) {
				    //short nj = MIN(GETFW(x, j), GETFW(y, j));
       				short njx = FW[x][j]; if(!njx)njx=N;
                    short njy = FW[y][j]; if(!njy)njy=N;
                    short nj=MIN(njx,njy);
					cnt += nj - j;
                    memset(V+j,1,(nj-j)*sizeof(V[0]));
					j = nj;
					//continue;
				}
				//probably x or y block started - try find it end
				short cntx = 0, cnty = 0;
				V[j] = 1;
				while (j < N) {
					cntx += A[j] == x;
					cnty += A[j] == y;
       				short njx = FW[x][j]; if(!njx)njx=N;
                    short njy = FW[y][j]; if(!njy)njy=N;
                    short nj=MIN(njx,njy);
                    
					if (cntx == cnty) {
						cnt++;
						j++;
						break;
					}
					j = nj;
				}
			}

			res += (cnt*(cnt + 1)) / 2;
			i++;
		}
		printf("%d\n", res);
        MEMO[xy] = res;
	}

	//free(FW);
	return 0;
}
