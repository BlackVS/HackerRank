#!/bin/python3

import sys
from collections import *

def solve():
    N, Q = map(int, input().strip().split())
    MEM  = defaultdict()
    A    = list(map(int, input().strip().split()))
    S    = set(A)
    I2A  = tuple(S)
    A2I  = { v:i for i,v in enumerate(I2A) }
    #A    = tuple(map(lambda a:A2I[a],A))
    M = len(A2I)
    #��� ������� �������� ���������� ������, ����� �� ����������� � ��������� ���
    FW = [[N]*N for _ in range(M)]
    #���������������� ������ ��� ������������ FW
    #������, ����� � ��������� ��� ������ ������ �����
    Fc = [0]*M

    for i,a in enumerate(A):
        a=A2I[a]
        A[i]=a
        ip=Fc[a]
        FW[a][ip:i]=[i]*(i-ip)
        Fc[a]=i
        
    for _ in range(Q):
        x, y = map(int, input().strip().split())
        x = A2I.get(x, -1)
        y = A2I.get(y, -1)
        x,y  = min(x,y),max(x,y)
        res=0
        xy=(x,y)
        #check MEMO
        mres=MEM.get(xy,None)
        if mres!=None: 
            print(mres)
            continue
        if x==-1 and y==-1:
            res=((N+1)*N)//2
        elif x==y:
            res=((N+1)*N)//2
        elif x==-1:
            #print(FW[y])
            res=0
            i=0
            while i<N:
                ni =FW[y][i]
                cnt=ni-i
                if A[i]==y:
                    cnt-=1
                res+=(cnt*(cnt+1))//2
                #print(i,cnt,res,ni-i,A[i])
                i=ni
        else:
            V=[-1]*N
            i=0
            while i<N:
                if V[i]!=-1:
                    i+=1
                    continue
                cnt=0
                if A[i]!=x and A[i]!=y:
                    ni=min(FW[x][i],FW[y][i])
                    cnt=ni-i
                    i=ni                
                j=i
                while j<N:
                    nj=min(FW[x][j],FW[y][j])
                    #non xy block
                    if A[j]!=x and A[j]!=y:
                        cnt+=nj-j
                        V[j:nj]=[1]*(nj-j)
                        j=nj
                        continue
                    #probably x or y block started - try find it end
                    cntx=cnty=0
                    V[j]=1
                    while j<N:
                        cntx+=A[j]==x
                        cnty+=A[j]==y
                        nj=min(FW[x][j],FW[y][j])
                        #end found
                        if cntx==cnty:
                            cnt+=1
                            j+=1
                            break
                        j=nj
                res+=(cnt*(cnt+1))//2
                i+=1
        MEM[xy]=res
        print(res)
        
if __name__ == "__main__":
    solve()