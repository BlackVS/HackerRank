# Enter your code here. Read input from STDIN. Print output to STDOUT#!/bin/python3
import sys,copy
from collections import *
from itertools import *
from operator import sub
#sys.stdin = open('G:\hackerrank\WoC\\34\\3. Same Occurrence\\t1.txt', 'r')

def solve():
    N, Q = map(int, sys.stdin.readline().split())
    MEM  = defaultdict()
    A    = list(map(int, sys.stdin.readline().split()))
    S    = set(A)
    I2A  = tuple(S)
    A2I  = { v:i for i,v in enumerate(I2A) }
    M = len(A2I)
    FRQ = [[0]*N for _ in range(M)]
    for i,a in enumerate(A):
        a=A2I[a]
        A[i]=a
        if i>0:
            FRQ[a][i]=FRQ[a][i-1]
        FRQ[a][i]=1
    
    for m in range(M):
        for n in range(1,N):
            FRQ[m][n]+=FRQ[m][n-1]
        
    for _ in range(Q):
        x, y = map(int, sys.stdin.readline().split())
        x = A2I.get(x, -1)
        y = A2I.get(y, -1)
        x,y  = min(x,y),max(x,y)
        res=0
        xy=(x,y)
        #check MEMO
        mres=MEM.get(xy,None)
        if mres!=None: 
            print(mres)
            continue
        if x==y:
            res=((N+1)*N)//2
        elif x==-1:
            c=Counter(FRQ[y])
            c[0]+=1
            res=sum(map(lambda x:(x*(x-1))//2,c.values()))
        else:
            DIFF=map(sub,FRQ[x],FRQ[y])
            c=Counter(DIFF)
            c[0]+=1
            res=sum(map(lambda x:(x*(x-1))//2,c.values()))
        MEM[xy]=res
        print(res)
        
if __name__ == "__main__":
    solve()