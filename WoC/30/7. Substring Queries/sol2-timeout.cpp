#include <cmath>
#include <cstdio>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <map>
using namespace std;

class CState {

public:
	int len, link;
	map<char, int> next;
public:
	CState()
	{
		len = link = 0;
	}
};


class CSuffixAutomata {
	int				_maxlen;
	vector<CState>  _states;
	int				_size;
	int				_last;
	bool			_f_inited;
protected:	
	void Extend(char c)
	{
		int cur = _size;
		_size++;
		_states[cur].len = _states[_last].len + 1;
		int p;
		for (p = _last; p != -1 && !_states[p].next.count(c); p = _states[p].link)
			_states[p].next[c] = cur;
		if (p == -1)
			_states[cur].link = 0;
		else {
			int q = _states[p].next[c];
			if (_states[p].len + 1 == _states[q].len)
				_states[cur].link = q;
			else {
				int clone = _size;
				_size++;
				_states[clone].len = _states[p].len + 1;
				_states[clone].next = _states[q].next;
				_states[clone].link = _states[q].link;
				for (; p != -1 && _states[p].next[c] == q; p = _states[p].link)
					_states[p].next[c] = clone;
				_states[q].link = _states[cur].link = clone;
			}
		}
		_last = cur;
	}

public:
	CSuffixAutomata()
	{
		_f_inited = false;
		_maxlen = 0;
		_size = 0;
		_last = 0;
	}

	bool Init(string s)
	{
		if (_f_inited) return false;
		_maxlen = s.length();
		_states.resize(_maxlen * 2);
		_states[0].len = 0;
		_states[0].link = -1;
		_size++;
		for (int i = 0; i<(int)s.length(); i++)
			Extend(s[i]);
		_f_inited = true;
		return true;
	}

	bool IsInited()
	{
		return _f_inited;
	}

	int lcs_len(string t)
	{
		int v = 0, 
			l = 0,
			best = 0;
		for (int i = 0; i < (int)t.length(); ++i) {
			while (v && !_states[v].next.count(t[i])) {
				v = _states[v].link;
				l = _states[v].len;
			}
			if (_states[v].next.count(t[i])) {
				v = _states[v].next[t[i]];
				++l;
			}
			if (l > best)
				best = l;
		}
		return best;
	}
};

int main() {
	int n;
	int q;
	cin >> n >> q;
	vector<string> s(n);
	vector<CSuffixAutomata> tree(n);
	for (int s_i = 0; s_i < n; s_i++) 
		cin >> s[s_i];
	//
	for (int a0 = 0; a0 < q; a0++) {
		int res=0;
		//	
		int x, y;
		cin >> x >> y;
		//
		int a = max(x, y);
		int b = min(x, y);
		if (!tree[a].IsInited() && !tree[b].IsInited())
		{
			tree[a].Init(s[a]);
			res = tree[a].lcs_len(s[b]);
		} else
		if(tree[a].IsInited())
			res = tree[a].lcs_len(s[b]);
		else
			res = tree[b].lcs_len(s[a]);
		cout << res << endl;
	}
	return 0;
}
