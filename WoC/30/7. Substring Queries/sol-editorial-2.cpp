#include <bits/stdc++.h>

#define pb push_back
#define all(x) (x).begin(), (x).end()

#ifdef KAZAR
  #define eprintf(...) fprintf(stderr,__VA_ARGS__)
#else
  #define eprintf(...) 0
#endif

using namespace std;

template<class T> inline void umax(T &a,T b){if(a < b) a = b;}
template<class T> inline void umin(T &a,T b){if(a > b) a = b;}
template<class T> inline T abs(T a){return a > 0 ? a : -a;}

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

const int inf = 1e9 + 143;
const ll longinf = 1e18 + 143;

inline int read(){int x;scanf(" %d",&x);return x;}

const int N = 3e5 + 100;
const int L = N;
const int LG = 21;

int from[N], to[N];

int len = 0;
int s[L];
int seperator = 1234;

void Append(int id) {
  static char foo[L];
  scanf(" %s", foo);
  int lenFoo = strlen(foo);
  from[id] = len + 1;
  to[id] = len + lenFoo;
  for (int i = 0; i < lenFoo; i++) {
    s[++len] = foo[i] - 'a' + 1;
  }
  s[++len] = ++seperator;
}

int whose[L];
pair<ii, int> p[L];
int rnk[LG][L];
int sa[L];

inline void Pass(int g){
    sort(p + 1, p + 1 + len);
    int ptr = 1;
    for(int i = 1; i <= len; i++){
      if(i > 1 && p[i - 1].first != p[i].first)
        ++ptr;
      rnk[g][p[i].second] = ptr;
    }
}

void BuildSuffix() {
    for(int i = 1; i <= len; i++)
      p[i] = make_pair(ii(s[i], 0), i);
    Pass(0);
    for(int g = 0; g + 1 < LG; g++){
      int d = 1 << g;
      for(int i = 1; i <= len; i++)
        p[i] = make_pair(ii(rnk[g][i], i + d <= len? rnk[g][i + d] : -1), i);
      Pass(g + 1);
    }
    for(int i = 1; i <= len; i++){
      sa[rnk[LG - 1][i]] = i;
    }
}

int lcp[L];
int minLcp[LG][L];
int logTable[L];

void BuildLcp() {
  for(int i = 1; i < len; i++){
    int cnow = sa[i];
    int cnext = sa[i + 1];
    lcp[i] = 0;
    for(int j = LG - 1; j >= 0; j--){
      int d = 1 << j;
      if(cnow + d - 1 <= len && cnext + d - 1 <= len && rnk[j][cnow] == rnk[j][cnext]){
        cnow += d;
        cnext += d;
        lcp[i] += d;
      }
    }
  }
  logTable[1] = 0;
  for (int i = 2; i < L; i++) {
    logTable[i] = logTable[i - 1];
    if ((i & -i) == i) {
      logTable[i] += 1;
    }
  }
  for (int i = 1; i < len; i++) {
    minLcp[0][i] = lcp[i];
  }
  for (int k = 1; k < LG; k++) {
    for (int i = 1; i < len; i++) {
      minLcp[k][i] = min(minLcp[k - 1][i], minLcp[k - 1][i + (1 << (k - 1))]);
    }
  }
}

int GetLcp(int l, int r) { // [l, r)
  int k = logTable[r - l];
  return min(minLcp[k][l], minLcp[k][r - (1 << k)]);
}

int answer[N];
int link[N];
vector<ii> qs[N];
int last[L];

int main() {

#ifdef KAZAR
  freopen("f.input", "r", stdin);
  freopen("f.output", "w", stdout);
  freopen("error", "w", stderr);
#endif // KAZAR

  int n = read();
  int q = read();

  for (int i = 0; i < n; i++)
    Append(i);

  BuildSuffix();
  BuildLcp();

  memset(whose, -1, sizeof whose);
  for (int i = 0; i < n; i++) {
    for (int j = from[i]; j <= to[i]; j++) {
      whose[rnk[LG - 1][j]] = i;
    }
  }

  map<ii, int> firstId;
  for (int i = 0; i < q; i++) {
    int x = read() - 1;
    int y = read() - 1;
    int lenX = to[x] - from[x] + 1;
    int lenY = to[y] - from[y] + 1;
    if (lenX > lenY || (lenX == lenY && x > y)) {
      swap(x, y);
    }
    if (x == y) {
      answer[i] = lenX;
      continue;
    }
    if (!firstId.count({x, y})) {
      firstId[{x, y}] = i;
      qs[x].pb({y, i});
    }
    link[i] = firstId[{x, y}];
  }

  for (int i = 1; i < len; i++) {
    eprintf("%d %d\n", lcp[i], whose[i]);
  }

  for (int it = 0; it < 2; it++) {
    memset(last, -1, sizeof last);
    for (int i = 1; i <= len; i++) {
      if (whose[i] != -1) {
        last[whose[i]] = i;
        for (auto e : qs[whose[i]]) {
          int v = e.first;
          int qId = e.second;
          if (last[v] != -1) {
            eprintf("v = %d, lcp = %d => %d %d\n", v, GetLcp(last[v], i), last[v], i);
            if (it == 0)
                umax(answer[qId], GetLcp(last[v], i));
            else
                umax(answer[qId], GetLcp(len - i + 1, len - last[v] + 1));
          }
        }
      }
    }
    reverse(whose + 1, whose + 1 + len);
  }

  for (int i = 0; i < q; i++) {
    printf("%d\n", answer[link[i]]);
  }

  return 0;
}
