#include <bits/stdc++.h>

using namespace std;

#define dbgs(x) cerr << (#x) << " --> " << (x) << ' '
#define dbg(x) cerr << (#x) << " --> " << (x) << endl

#define foreach(i,x) for(type(x)i=x.begin();i!=x.end();i++)
#define FOR(ii,aa,bb) for(int ii=aa;ii<=bb;ii++)
#define ROF(ii,aa,bb) for(int ii=aa;ii>=bb;ii--)

#define type(x) __typeof(x.begin())

#define orta (bas + son >> 1)
#define sag (k + k + 1)
#define sol (k + k)

#define pb push_back
#define mp make_pair

#define nd second
#define st first

#define endl '\n'

typedef long long ll;

const long long linf = 1e18+5;
int mod = (int) 1e9 + 7;
const int logN = 17;
const int inf = 1e9;
const int N = 3e5 + 5;
const int SQ = 250;

int last[N], size[N], belong[N], sorted[N], suff[N], ans[N], rmq[N][logN + 1], LOG[N], finish[N];
int n, m, x, y, z, q;

pair< pair< int , int > , int > C[N];
vector< pair< int , int > > v[N];
pair< int , int > Q[N];
map< int , int > H[N];

#define min(x, y) ((x) < (y) ? (x) : (y))

int query(int x, int y) {
int l = LOG[y - x + 1];
int ans = min(rmq[x][l], rmq[y - (1 << l) + 1][l]);
int s1 = belong[sorted[x]], ind1 = sorted[x];
int s2 = belong[sorted[y + 1]], ind2 = sorted[y + 1];
ans = min(ans, finish[s1] - ind1 + 1);
ans = min(ans, finish[s2] - ind2 + 1);
return ans;
}

string sss[N];

int main() {
ios_base::sync_with_stdio(false);

cin >> n >> q;
assert(1 <= n && n <= 50000);
assert(1 <= q && q <= 100000);

string all, str;

int asd = 0;

FOR(i, 1, n) {
cin >> str;
size[i] = str.size();
asd += size[i];
sss[i] = str;
all += 'z' + 1;
int t = all.size();
all += str; finish[i] = all.size() - 1;
FOR(j, t, all.size() - 1) belong[j] = i;
}

assert(1 <= asd && asd <= 100000);

int m = all.size() - 1;

FOR(i, 1, m) suff[i] = all[i];

FOR(j, 0, logN) {
FOR(i, 1, m) C[i] = mp(mp(suff[i], suff[min(m + 1, i + (1 << j))]), i);
sort(C + 1, C + m + 1);
FOR(i, 1, m) suff[C[i].nd] = suff[C[i-1].nd] + (C[i-1].st != C[i].st);
}

FOR(i, 1, m) { sorted[suff[i]] = i; LOG[i] = log2(i); }

int j = 0;

FOR(i, 1, m) {
if(suff[i] == 1) continue;
int kk = sorted[suff[i]-1];
while(i + j <= m && sorted[suff[i]-1] + j <= m && all[i + j] == all[sorted[suff[i] - 1] + j]) j++;
rmq[suff[i] - 1][0] = j;
if(j) j--;
}

FOR(j, 1, logN) FOR(i, 1, m) rmq[i][j] = min(rmq[i][j-1], rmq[min(m, i + (1 << j - 1))][j-1]);

q = min(q, 100000);

FOR(i, 1, q) {
cin >> x >> y;
Q[i] = mp(x, y);
if(H[x][y]) continue;
H[x][y] = H[y][x] = i;
if(size[y] > size[x]) swap(x, y);
v[y].pb(mp(x, i));
}

FOR(i, 1, m) {
last[belong[sorted[i]]] = i;
foreach(it, v[belong[sorted[i]]]) {
if(!last[it->st]) continue;
ans[it->nd] = max(ans[it->nd], query(last[it->st], i - 1));
}
} memset(last, 0, sizeof last);

ROF(i, m, 1) {
last[belong[sorted[i]]] = i;
foreach(it, v[belong[sorted[i]]]) {
if(!last[it->st]) continue;
ans[it->nd] = max(ans[it->nd], query(i, last[it->st] - 1));
}
}

FOR(i, 1, q) {
x = Q[i].st, y = Q[i].nd;
cout << ans[H[x][y]] << endl;
}

return 0;
}