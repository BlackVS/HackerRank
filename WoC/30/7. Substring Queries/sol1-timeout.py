#!/bin/python3

import sys

N,Q = map(int, input().strip().split())
S = []
for _ in range(N):
    S.append(input().strip())

#DP classic
def lcs_dp(s1, s2):
    m = [[0]*(1+len(s2)) for i in range(1 + len(s1))]
    x_len = 0
    for x in range(1, 1 + len(s1)):
        for y in range(1, 1 + len(s2)):
            if s1[x - 1] == s2[y - 1]:
                m[x][y] = m[x - 1][y - 1] + 1
                if m[x][y] > x_len:
                    x_len = m[x][y]
            else:
                m[x][y] = 0
    return x_len

class SuffixAutomata():
    def __init__(self, maxlen):
        self.last = 0
        self.maxlen = maxlen
        # st[] of state. State= int len, int link, map next
        self.stlen = [0] * (maxlen * 2)
        self.stlink = [-1] + [0] * (maxlen * 2 - 1)
        self.stnext = [dict() for _ in range(maxlen * 2)]
        # empty automata first
        self.sz = 1

    def extend(self, c):
        cur = self.sz
        self.sz += 1
        self.stlen[cur] = self.stlen[self.last] + 1

        p = self.last
        while p != -1 and c not in self.stnext[p]:
            self.stnext[p][c] = cur
            p = self.stlink[p]
        if p == -1:
            self.stlink[cur] = 0
        else:
            q = self.stnext[p][c]
            if self.stlen[p] + 1 == self.stlen[q]:
                self.stlink[cur] = q
            else:
                clone = self.sz
                self.sz += 1
                self.stlen[clone]  = self.stlen[p] + 1
                self.stnext[clone] = self.stnext[q].copy()
                self.stlink[clone] = self.stlink[q]
                while p != -1 and self.stnext[p][c] == q:
                    self.stnext[p][c] = clone
                    p = self.stlink[p]
                self.stlink[q] = self.stlink[cur] = clone
        self.last = cur

    def lcs(self, s2):
        v = l = best = bestpos = 0
        for i in range(len(s2)):
            while v and s2[i] not in self.stnext[v]:
                v = self.stlink[v]
                l = self.stlen[v]
            if s2[i] in self.stnext[v]:
                v = self.stnext[v][s2[i]]
                l += 1
            if l > best:
                best = l
                bestpos = i
        return (bestpos - best + 1, best)

ST=[None]*len(S)

def lcs_sfx(is1, is2):
    sa=None
    if len(S[is1])<len(S[is2]):
        is1,is2=is2,is1
    if ST[is1]==None:
        sa = SuffixAutomata(len(S[is1]))
        for c in S[is1]:
            sa.extend(c)
        ST[is1]=sa
    else:
        sa=ST[is1]
    return sa.lcs(S[is2])[1]

for _ in range(Q):
    x,y = input().strip().split(' ')
    x,y = [int(x),int(y)]
    # your code goes here
    #print(lcs_dp(S[x],S[y]))
    print(lcs_sfx(x,y))