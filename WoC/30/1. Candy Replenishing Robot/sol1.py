#!/bin/python3

import sys


n,t = map(int, input().strip().split())
C = list(map(int, input().strip().split(' ')))
# your code goes here
cnds=0
cur =n
for c in C[:-1]:
    cur-=c
    if cur<5 :
        cnds+=n-cur
        cur=n
print(cnds)  