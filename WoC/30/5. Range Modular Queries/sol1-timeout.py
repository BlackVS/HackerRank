#!/bin/python3

import sys,random,itertools

class RMQ:
    def __init__(self, n):
        self.sz = 1 
        self.minv=0
        self.maxv=65535
        while self.sz <= n: 
            self.sz = self.sz << 1
        self.mins = [self.maxv] * (2*self.sz-1)
        self.maxs = [self.minv] * (2*self.sz-1)
    
    def update(self, idx, x):
        idx += self.sz - 1
        self.mins[idx] = x
        self.maxs[idx] = x
        while idx > 0:
            idx = (idx - 1) >> 1
            self.mins[idx] = min(self.mins[idx * 2 + 1], self.mins[idx * 2 + 2])
            self.maxs[idx] = max(self.maxs[idx * 2 + 1], self.maxs[idx * 2 + 2])
            
    def _query_minmax(self, a, b, k, l, r):
        if r <= a or b <= l:
            return (self.maxv,self.minv)
        elif a <= l and r <= b:
            return (self.mins[k],self.maxs[k])
        else:
            m1=self._query_minmax(a, b, 2 * k + 1, l, (l+r)>>1)
            m2=self._query_minmax(a, b, 2 * k + 2, (l+r)>>1, r)
            return (min(m1[0],m2[0]),max(m1[1],m2[1]))

    def query_minmax(self, a, b):
        return self._query_minmax(a, b, 0, 0, self.sz)
            
        
N,Q = map(int, input().strip().split())
A = list(map(int, input().strip().split(' ')))
rmq=RMQ(N)
for i in range(N):
    rmq.update(i,A[i])

RM=[0]*40001
for _ in range(Q):
    L,R,X,Y = map(int, input().strip().split())
    amin,amax=rmq.query_minmax(L,R+1)
    RM[amin:amax+1]=[0]*(amax-amin+1)
    for i in range((amin//X)*X+Y,amax+1,X):
        RM[i]=1
    r2=sum(RM[a] for a in A[L:R+1])
    print(r2)
