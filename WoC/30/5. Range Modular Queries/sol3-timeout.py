#!/bin/python3

import sys


N,Q = map(int, input().strip().split())
A = list(map(int, input().strip().split(' ')))
for _ in range(Q):
    L,R,X,Y = map(int, input().strip().split())
    #print(L,R,X,Y)
    print(sum(map(lambda a:a%X==Y,A[L:R+1])))