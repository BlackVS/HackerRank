#!/bin/python3

import sys,random


N,Q = map(int, input().strip().split())
#N=40000
#Q=4000
A = list(map(int, input().strip().split(' ')))
maxA=max(A)
for _ in range(Q):
    L,R,X,Y = map(int, input().strip().split())
    RM=[0]*(maxA+1)
    for i in range(Y,maxA+1,X):
        RM[i]=1
    r2=sum(RM[a] for a in A[L:R+1])
    print(r2)