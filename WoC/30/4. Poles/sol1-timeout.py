#!/bin/python3

import sys
from itertools import*



def weight1(i1,i2):
    if i1==i2: return 0
    return sum(map(lambda i:(XW[i][0]-XW[i1][0])*XW[i][1],range(i1+1,i2+1)))

def weight(i1,i2,XW,SW,SXW):
    if i1==i2: return 0
    w=SXW[i2]-XW[i1][0]*SW[i2]
    if i1>0:
        w+=XW[i1][0]*SW[i1-1]-SXW[i1-1]
    return w

#1.Brute force to compare
def solve1():

    N,K = map(int, input().strip().split())
    XW  = [(0,0)]*N
    SW  = [0]*N
    SXW = [0]*N

    sw,sxw=0,0
    for i in range(N):
        x,w=map(int, input().strip().split())
        sw+=w
        sxw+=w*x
        XW[i]=(x,w)
        SW[i]=sw
        SXW[i]=sxw
    #print(XW)
    #print(SW)
    #print(SXW)

    resW=None
    resC=[0]*K
    for c in combinations(range(1,N),K-1):
        cc=(0,)+c+(N,)
        res=sum(map(lambda i:weight(cc[i],cc[i+1]-1,XW,SW,SXW),range(K)))
        if resW==None or res<resW:
            resW=res
            resC=cc
    return resW

# i0...i2    
# res i0....ri-1 ri....i2
def split2(i0,i2):
    #print("split2>",i0,i2)
    if i0+1==i2: 
        return [(i0,0),(i2,0)]
    ri1=i1=i0+1
    rw0=w0=weight(i0,i0)
    rw1=w1=weight(i1,i2)
    while i1<i2:
        w0+=(XW[i1][0]-XW[i0][0])*XW[i1][1]
        w1+=(XW[i1][0]-XW[i1+1][0])*sum(XW[j][1] for j in range(i1+1,i2+1))
        #print(i1,weight(i0,i1)+weight(i1+1,i2),w0+w1)
        i1+=1
        if w0+w1<rw0+rw1:
            rw0,rw1=w0,w1
            ri1=i1
    return [(i0,rw0),(ri1,rw1)]

# i0... i1... i2...i3 
def split3(i0,i3):
    #print("split3>",i0,i3)
    rw=None
    w0=0
    rw0=rw1=rw2=0
    for i1 in range(i0+1,i3):
        #print(i1)
        w0+=(XW[i1-1][0]-XW[i0][0])*XW[i1-1][1]
        r=split2(i1,i3)
        if rw==None or w0+r[0][1]+r[1][1]<rw:
            rw=w0+r[0][1]+r[1][1]
            rw0,rw1,rw2=w0,r[0][1],r[1][1]
            ri1,ri2=i1,r[1][0]
    #print(rw)
    return [(i0,rw0),(ri1,rw1),(ri2,rw2)]

def solve3():
    #if K==N or N==1:
    #    return 0
    if K==1:
        return weight(0,N-1)
    if K==2:
        r=split2(0,N-1)
        return r[0][1]+r[1][1]
    if K==3:
        r=split3(0,N-1)
        return r[0][1]+r[1][1]+r[2][1]
    
    G=split3(0,N-1)+[(N,0)]
    rw=sum(g[1] for g in G)
    k=3
    while k<K:
        ri=0
        rr=None
        for i in range(0,k-1):
            if G[i+2][0]-G[i][0]<3:
                continue
            r=split3( G[i][0], G[i+2][0]-1 )
            w=sum(g[1] for g in G[:i]) + (r[0][1]+r[1][1]+r[2][1]) + sum(g[1] for g in G[i+2:])
            if rr==None or w<rw:
                rw=w
                ri=i
                rr=r
        G=G[:ri]+rr+G[ri+2:]
        k+=1
    return rw

print(solve1())
#print(solve2())
#print(split2(0,N-1))
#print(split3(0,N-1))
#print(solve3())
