#include <algorithm>
#include <cmath>
#include <iostream>
#include <limits>
#include <tuple>
#include <vector>

using namespace std;

int n;
int m[50][50];

typedef double flow_type;

const int MAXV = 50 * 50 * 50;
const int MAXE = 2 * 50 * 50 * 50 * 3;
const int MAX_DIST = numeric_limits<int>::max();
const flow_type MAX_FLOW = numeric_limits<flow_type>::max();

typedef struct struct_edge* edge;
struct struct_edge {
	int v;
	flow_type c;
	edge n, b;
} pool[MAXE];
edge top;
int S, T;
edge adj[MAXV];
void build_graph(int s, int t) {
	top = pool;
	fill(adj, adj + MAXV, nullptr);
	S = s, T = t;
}
void add_edge(int u, int v, flow_type c, flow_type bc = 0) {
	top->v = v, top->c = c, top->n = adj[u], adj[u] = top++;
	top->v = u, top->c = bc, top->n = adj[v], adj[v] = top++;
	adj[u]->b = adj[v], adj[v]->b = adj[u];
	if (u == v)
		adj[u]->n->b = adj[u], adj[v]->b = adj[v]->n;
}
int d[MAXV];
int q[MAXV];
int qh, qt;
bool relabel() {
	fill(d, d + MAXV, MAX_DIST), d[q[qh = qt = 0] = T] = 0;
	while (qh <= qt) {
		int u = q[qh++];
		for (edge i = adj[u]; i; i = i->n)
			if (i->b->c && d[i->v] > d[u] + 1) {
				d[i->v] = d[u] + 1;
				if ((q[++qt] = i->v) == S)
					return true;
			}
	}
	return false;
}
edge cur[MAXV];
flow_type augment(int u, flow_type e) {
	if (u == T)
		return e;
	flow_type f = 0;
	for (edge& i = cur[u]; i; i = i->n) {
		if (i->c > 0 && d[u] == d[i->v] + 1) {
			flow_type df = augment(i->v, min(e, i->c));
			if (df > 0)
				i->c -= df, i->b->c += df, e -= df, f += df;
		}
		if (!(e > 0))
			break;
	}
	return f;
}
flow_type dinic() {
	flow_type f = 0;
	while (relabel())
		copy(adj, adj + MAXV, cur), f += augment(S, MAX_FLOW);
	return f;
}
void dfs(int u) {
	if (!d[u]) {
		d[u] = true;
		for (edge& i = adj[u]; i; i = i->n) {
			if (i->c > 1e-6) {
				dfs(i->v);
			}
		}
	}
}

int main() {
	cin >> n;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			cin >> m[i][j];
		}
	}
	vector<tuple<int, int, int> > t;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < i; ++j) {
			for (int k = 0; k < j; ++k) {
				if (m[i][j] && m[j][k] && m[k][i]) {
					t.emplace_back(i, j, k);
				}
			}
		}
	}
	double l = 0, r = 50 * 50 * 50;
	while ((r - l) > 1e-10) {
		double m = (l + r) / 2;
		build_graph(t.size() + n, t.size() + n + 1);
		for (auto i = 0; i < n; i++) {
			add_edge(S, t.size() + i, m);
		}
		for (auto i = 0u; i < t.size(); i++) {
			add_edge(t.size() + get<0>(t[i]), i, MAX_FLOW);
			add_edge(t.size() + get<1>(t[i]), i, MAX_FLOW);
			add_edge(t.size() + get<2>(t[i]), i, MAX_FLOW);
			add_edge(i, T, 1);
		}
		auto x = dinic();
		if (t.size() - x > 0) {
			l = m;
		} else {
			r = m;
		}
	}
	fill(d, d + MAXV, false);
	dfs(S);
	vector<int> ans;
	for (int i = 0; i < n; i++) {
		if (!d[t.size() + i]) {
			ans.push_back(i);
		}
	}
	cout << ans.size() << endl;
	for (int i = 0; i < ans.size(); i++) {
		cout << ans[i] + 1 << char(i + 1 == ans.size() ? '\n' : ' ');
	}
	return 0;
}