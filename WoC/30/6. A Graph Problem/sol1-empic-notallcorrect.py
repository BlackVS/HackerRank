#!/bin/python3

import sys
from itertools import*

N = int(input().strip())
G = [[0]*N for _ in range(N)]
V = [[] for _ in range(N)]
VT= [0]*N
T = []
for i in range(N):
    G[i] = list(map(int, input().strip().split()))
    V[i] = [j for j in range(i,N) if G[i][j]]
#
#print(G)
#print(V)
for i in range(N):
    for vv in combinations(V[i],2):
        if G[vv[0]][vv[1]]:
            T.append({i,vv[0],vv[1]})
            VT[i]+=1
            VT[vv[0]]+=1
            VT[vv[1]]+=1
#
#print(T)
#print(VT)
RT=[0]*len(T)
for i in range(len(T)):
    RT[i]=sum(map(lambda j:VT[j],T[i]))
#print(RT)
Ti=sorted(range(len(T)),key=lambda i:RT[i],reverse=True)
#print(Ti)
#
resS=0
resV=set()
cntT=0
for i in range(len(T)):
    t=T[Ti[i]]
    s=(cntT+1)/len(resV|t)
    if s>=resS:
        resS=s
        resV=resV|t
        cntT+=1
#print(resS,resV,cntT)
print(len(resV))
print(" ".join(map(lambda i:str(i+1),sorted(resV))))