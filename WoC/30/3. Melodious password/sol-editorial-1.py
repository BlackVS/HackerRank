from string import ascii_lowercase
from itertools import product
vs = 'aeiou'
cs = set(ascii_lowercase) - set(vs) - {'y'}
n = int(input())
print(*(''.join(s) for pair in [(vs,cs),(cs,vs)] for s in product(*(pair*n)[:n])), sep='\n')