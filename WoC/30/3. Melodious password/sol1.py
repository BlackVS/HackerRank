#!/bin/python3
import sys
from itertools import*
n = int(input().strip())
vowels="aeiou"
cons  ="bcdfghjklmnpqrstvwxz"
n2 = n//2
n1 = n-n2

#1. vcvcv
vs=list(product(vowels,repeat=n1))
for c in product(cons,repeat=n2):
    for v in vs:
        r="".join(map(lambda i:v[i]+c[i],range(n2)))
        if n1>n2: r+=v[-1]
        print(r)
        
#2. cvcvc
vs=list(product(vowels,repeat=n2))
for c in product(cons,repeat=n1):
    for v in vs:
        r="".join(map(lambda i:c[i]+v[i],range(n2)))
        if n1>n2: r+=c[-1]
        print(r)