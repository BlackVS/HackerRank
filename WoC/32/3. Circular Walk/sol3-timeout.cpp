#include <cmath>
#include <cstdio>
#include <cstring>
#include <vector>
#include <set>
#include <iostream>
#include <algorithm>
#include <deque>
using namespace std;


int getDist(int A, int B, int N)
{
	int d1 = abs(A - B);
	int d2 = N - d1;
	return min(d1, d2);
}

int main() {
	int N, S, T, N2;
	int R0, G, SEED, MODP;
	cin >> N >> S >> T;
	N2 = N ;
	cin >> R0 >> G >> SEED >> MODP;

	if (S == T )
	{
		cout << 0;
		return 0;
	}

  	if (R0 == 0)
	{
		cout << -1;
		return 0;
	}

	int* SEQ = (int*)malloc(sizeof(*SEQ)*N);
    SEQ[0]=R0;
	for(int i=0;i<N;i++)
    {
		SEQ[i] = min(R0, N2);
		R0 = (R0*G + SEED) % MODP;
	}
    
	int szQ = sizeof(int)*N;
	int* D = (int*)malloc(szQ);
	memset(D, -1, szQ);
	D[S] = 0;

	typedef std::set<int> QT;
	QT _Q,_NQ;
	QT* pQ = &_Q;
	QT* pNQ = &_NQ;
	std::set<int>::iterator itQ;
	_Q.insert(S);


	int res = -1;
	for (int newD = 1; res == -1 && pQ->size(); newD++)
	{
		pNQ->clear();
		for (itQ=pQ->begin();itQ!=pQ->end();itQ++)
		{
			int i = *itQ;
			int Ri = SEQ[i];
            if(i==T)
			{
				res = newD-1;
				break;
			}
			if (Ri == 0) continue;

			if (getDist(i, T, N) <= Ri)
			{
				res = newD;
				break;
			}
			for (int _nb = i - Ri; _nb <= i + Ri; _nb++)
			{
				if (_nb == i) continue;
				int nb = _nb;
				while (nb < 0) nb += N;
				while (nb >= N)nb -= N;
                if(nb==T) 
                {
                    res = newD;
                    break;
                }
                int Rnb = SEQ[nb];
				if (D[nb] == -1 && Rnb>0)
				{
					D[nb] = newD;
					pNQ->insert(nb);
				}
			}
		}
		swap(pQ,pNQ);
	}
	cout << res;
	return 0;
}
