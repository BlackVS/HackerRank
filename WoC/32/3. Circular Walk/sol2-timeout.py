#!/bin/python3

import sys
from collections import*

N, S, T = map(int, input().strip().split())
N2=N//2
R0, G, SEED, MODP = map(int,input().strip().split())
    
def gen(R0,G,SEED,MODP):
    R=R0
    yield R
    for i in range(1,N):
        R=(R*G+SEED)%MODP
        yield min(R,N2)

def getDist(A,B,N):
    d1=abs(A-B)
    d2=N-d1
    return min(d1,d2)

def solve():
    if S==T: return 0
    if R0==0: return -1
    SEQ=tuple(gen(R0,G,SEED,MODP))

    Q =deque([S])
    D =[-1]*N
    D[S]=0
    for newD in range(1,N):
        if not Q: break
        NQ=deque()
        while Q:
            i=Q.pop()
            #if D[i]>=0:continue
            D[i]=newD
            Ri=SEQ[i]
            if Ri==0: continue
            dist=getDist(i,T,N);
            if dist<=Ri:
                return newD
            for nb in range(i-Ri,i+Ri+1):
                while nb<0: nb+=N
                while nb>=N:nb-=N
                if D[nb]==-1 and SEQ[nb]>0:
                    D[nb]=newD
                    NQ.append(nb)   
        Q=NQ
    return -1

print(solve())