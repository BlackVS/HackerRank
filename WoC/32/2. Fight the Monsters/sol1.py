#!/bin/python3

import sys

def solve():
    N, Hit, T = map(int, input().split())
    M = sorted(map(int, input().split()))
    i=0
    while T:
        if i>=N: break
        tmax=(M[i]+(Hit-1))//Hit
        if tmax>T: break
        T-=tmax
        i+=1
    return i


print(solve())