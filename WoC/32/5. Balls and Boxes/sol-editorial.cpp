#include <bits/stdc++.h>

using namespace std;

const int maxn = 405;

struct node
{
	int to,next,flow,cost;

	node(void){}
	node(int a,int b,int c,int d) : to(a),next(b),flow(c),cost(d){}
}e[maxn * maxn];

int final[maxn],in[maxn],A[maxn],B[maxn][maxn],C[maxn],d[maxn],tot;
int n,m,s,t,ans;

void link(int u,int v,int f,int c)
{
	e[++ tot] = node(v,final[u],f,c),final[u] = tot;
	e[++ tot] = node(u,final[v],0,-c),final[v] = tot;
}

int dfs(int now,int flow)
{
	if (now == t) return ans += flow * d[now],flow;
	int use = 0;
	in[now] = 1;
	for(int i = final[now];i;i = e[i].next)
		if (!in[e[i].to] && d[e[i].to] == d[now] + e[i].cost && e[i].flow)
		{
			int tmp = dfs(e[i].to,min(flow - use,e[i].flow));
			use += tmp,e[i].flow -= tmp,e[i ^ 1].flow += tmp;
			if (use == flow) return use;
		}
	return use;
}

void min_cost()
{
	for(;;)
	{
		static int que[maxn * maxn];
		for(int i = s;i <= t;i ++) d[i] = -(1 << 30),in[i] = 0;
		d[s] = 0;
		que[1] = s;
		int fi = 1,en = 1;
		for(;fi <= en;fi ++)
		{
			int u = que[fi];
			for(int i = final[u];i;i = e[i].next)
				if (e[i].flow && d[u] + e[i].cost > d[e[i].to])
				{
					d[e[i].to] = d[u] + e[i].cost;
					if (!in[e[i].to]) in[e[i].to] = 1,que[++ en] = e[i].to;
				}
			in[u] = 0;
		}
		if (d[t] <= 0) break;
		dfs(s,1 << 30);
	}
}

int main()
{
	tot = 1;
	scanf("%d%d", &n, &m);
	for(int i = 1;i <= n;i ++) scanf("%d", &A[i]);
	for(int i = 1;i <= m;i ++) scanf("%d", &C[i]);
	for(int i = 1;i <= n;i ++)
		for(int j = 1;j <= m;j ++) scanf("%d", &B[i][j]);
	s = 0,t = n + m + 1;
	for(int i = 1;i <= n;i ++) link(s,i,A[i],0);
	for(int i = 1;i <= n;i ++)
		for(int j = 1;j <= m;j ++)
			link(i,n + j,1,B[i][j]);
	for(int i = 1;i <= m;i ++)
	{
		link(i + n,t,C[i],0);
		link(i + n,t,1,-1);
		for(int j = C[i] + 1;j < n;j ++)
			link(i + n,t,1,-2 * (j - C[i]) - 1);
	}
	min_cost();
	printf("%d\n", ans);
	return 0;
}