#include <cmath>
#include <cstdio>
#include <cstring>
#include <vector>
#include <iostream>
#include <algorithm>
#include <unordered_map>
using namespace std;

int N=0, M=0;
int A[101];
int C[101];
int B[101][101];
char*USED = NULL;

void setUsed(int n, int m, char v)
{
	USED[n*M + m] = v;

}


char getUsed(int n, int m)
{
	return USED[n*M + m];
}

std::unordered_map<std::string, int> MEM;

int solve(int startball=0)
{
	auto search = MEM.find(USED);
	if (search != MEM.end()) {
		return search->second;
	}
    while(!A[startball]&&startball<N-1)
        startball++;
    if(startball==N)
        return 0;
    
	int rmax = 0;
    
	for (int bsk = 0; bsk < M; bsk++)
	{
		int p = C[bsk] <= 0 ? (2 * C[bsk] - 1 ): 0;
		for (int ball = startball; ball < N; ball++)
		{
			if (!A[ball]) continue;
			if (getUsed(ball,bsk)=='1') continue;
			int w=B[ball][bsk] + p;
			if (w > 0)
			{
				C[bsk] -= 1;
				setUsed(ball, bsk, '1');
				A[ball] -= 1;
				int r = w + solve();
				if (r > rmax)
					rmax = r;
				A[ball] += 1;
				setUsed(ball, bsk, '0');
				C[bsk] += 1;

			}
		}
	}
	MEM.insert({ USED, rmax });
	return rmax;
}
int main()
{
	cin >> N >> M;
	for (int i = 0; i < N; i++) 
		cin >> A[i];

	for (int i = 0; i < M; i++) 
		cin >> C[i];


	for (int n = 0; n < N; n++) 
		for (int m = 0; m < M; m++) 
			cin >> B[n][m];

	USED = (char*)malloc(N*M + 1);
	memset(USED, '0', N*M);
	USED[N*M] = 0;
	cout << solve();
	if (USED) free(USED);
	return 0;
}
