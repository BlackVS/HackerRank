#!/bin/python3

import sys
import operator
from collections import*
from itertools import*

N,M = map(int, input().strip().split())
MASK=tuple(1<<i for i in range(101))
MEM =dict()
MEMA=dict()
MEMB=dict()
USED =[0]*N


def setUsed(n,m):
    m=MASK[m]
    USED[n]|=m
    
def clearUsed(n,m):
    m=MASK[m]
    USED[n]&=~m

def getUsed(n,m):
    m=MASK[m]
    return USED[n]&m
    

def pack(USED):
    return tuple(USED)

def recurse(A,B,C,starball=0):
    h=pack(USED)
    ha=tuple(A)
    if MEMA.get(ha,False):
        r=MEM.get( h , None)
        if r!=None: 
            return r
    MEMA[ha]=True

    while(not A[starball] and starball<N-1):
        starball+=1;
        
    rmax=0
    for bsk in range(M):
        p=0
        if C[bsk]<=0: p=2*C[bsk]-1
        for ball in range(starball,N):
            if not A[ball]:
                continue
            if getUsed(ball,bsk):continue
            w=B[ball][bsk]+p;
            if w>=0:
                C[bsk]-=1
                setUsed(ball,bsk)
                A[ball]-=1
                r=w+recurse(A,B,C,starball)
                if r>rmax:rmax=r
                A[ball]+=1
                clearUsed(ball,bsk)
                C[bsk]+=1
    MEM[h]=rmax
    return rmax

def solve():
    A = list(map(int, input().strip().split()))
    C = list(map(int, input().strip().split()))

    #print(MASKS)
    B     = []
    for iBalls in range(N):
        bb=tuple(map(int, input().strip().split()))
        B.append(bb)

    return recurse(A,B,C)

print(solve())