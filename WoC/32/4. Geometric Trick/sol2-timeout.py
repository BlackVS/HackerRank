#!/bin/python3

import sys
import operator
from collections import*
from itertools import*
from functools import*

from random import*

N = int(input().strip())

def primes(n):
    ps, sieve = [], [True] * (n+1)
    for p in range(2, n):
        if sieve[p]:
            ps.append(p)
            for i in range(p*p, n, p):
                sieve[i] = False
    return ps

PRIMES=tuple(primes(500000))

def countIJKs(base,powers,J,S,iBase=0,value=1):
    res=0
    if iBase==len(base): 
        i,k=value-1,((J+1)*(J+1))//value-1
        if k<len(S):
            if S[i]=='a' and S[k]=='c':
                res+=1
    else:
        v=value
        for i in range(powers[iBase]*2+1):
            if i: v*=base[iBase]
            if v>N: break
            res+=countIJKs(base,powers,J,S,iBase+1,v)
    return res    
    
def factorize(n):
    res = defaultdict(lambda: 0)
    for p in PRIMES:
        while n % p == 0:
            n = n // p
            res[p] += 1
        if n == 1:
            return dict(res)

def solve():
    S=input().strip();
    res=0
    for i in range(N):
        if S[i]!='b': continue;
        f=factorize(i+1)
        res+=countIJKs(list(f.keys()),list(f.values()),i,S)
    return res
        
print(solve())