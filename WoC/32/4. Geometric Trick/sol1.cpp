#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


#define MAXN 512000

// stores smallest prime factor for every number
int spf[MAXN];

// Calculating SPF (Smallest Prime Factor) for every
// number till MAXN.
// Time Complexity : O(nloglogn)
void sieve()
{
	spf[1] = 1;
	for (int i = 2; i<MAXN; i++)

		// marking smallest prime factor for every
		// number to be itself.
		spf[i] = i;

	// separately marking spf for every even
	// number as 2
	for (int i = 4; i<MAXN; i += 2)
		spf[i] = 2;

	for (int i = 3; i*i<MAXN; i++)
	{
		// checking if i is prime
		if (spf[i] == i)
		{
			// marking SPF for all numbers divisible by i
			for (int j = i*i; j<MAXN; j += i)

				// marking spf[j] if it is not 
				// previously marked
				if (spf[j] == j)
					spf[j] = i;
		}
	}
}

// A O(log n) function returning primefactorization
// by dividing by smallest prime factor at every step
int getFactorization(int x,int* bases,int* pwrs)
{
	int cnt = 0;
	while (x != 1)
	{
		int spfx = spf[x];
		if (cnt == 0 || bases[cnt - 1] != spfx)
		{
			bases[cnt] = spfx;
			pwrs[cnt] = 1;
			cnt++;
		}
		else {
			pwrs[cnt - 1]++;
		}
		//ret.push_back(spf[x]);
		x /= spfx;
	}
	return cnt;
}

int countIJKs(int cntBases, int* bases, int* powers, long long J, const char* S, long long N, int iBase=0, long long value=1)
{
	int res = 0;
	if (iBase == cntBases)
	{
		int i = value - 1;
		long long k = ((J + 1)*(J + 1))/value-1;
		if (k < N && S[i] == 'a' && S[k] == 'c')
			res++;
	}
	else {
		long long v = value;
		for (int i = 0; i < powers[iBase] * 2 + 1; i++)
		{
			if (i) v *= bases[iBase];
			if (v > N) break;
			res += countIJKs(cntBases, bases, powers, J, S, N, iBase + 1, v);
		}
	}
	return res;
}

int main()
{
	// precalculating Smallest Prime Factor
	sieve();

	int N=0, res = 0;;
	int bases[1000];
	int powers[1000];
	char S[MAXN];


	cin >> N;
	cin >> S;
	
	char* ptr = S;
	for (int i = 0; *ptr!=0; i++, ptr++)
	{
		if (*ptr != 'b') continue;
		// calling getFactorization function
		int cntBases=getFactorization(i+1,bases,powers);
		res += countIJKs(cntBases, bases, powers, i, S, N);
	}
	cout << res;
	return 0;
}

