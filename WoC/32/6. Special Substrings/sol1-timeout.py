#!/bin/python3

import sys

#return array of lengths polinformes for each center
#  centers:  a   b   c ->
#          0 1 2 3 4 5 6
def Palindromes(seq):
    slen = len(seq)
    l = []
    i = 0
    plen = 0
    while i < slen:
        # try extend the current palindrome with center in i
        while i > plen and i<slen and seq[i - plen - 1] == seq[i]:
            plen += 2
            i += 1
        #and store
        l.append(plen)

        # look for a smaller left to share to the right
        s = len(l) - 2
        e = s - plen
        for j in range(s, e, -1):
            d = j - e - 1
            if l[j] == d:
                plen = d
                break

            # Otherwise just copy the value to the right
            l.append(min(d, l[j]))
        else:
            # plen == 0 or
            # unable to find a palindrome sharing the left edge with
            # the last palindrome.
            # I.e. start consider the palindrome centered at seq[i].
            plen = 1
            i += 1

    # store last longest
    l.append(plen)

    # and shared left to the right like do before, res llen = 2*slen+1
    llen = len(l)
    s = llen - 2
    e = s - (2 * slen + 1 - llen)

    for i in range(s, e, -1):
        d = i - e - 1
        l.append(min(d, l[i]))

    return l


def solve(str):
    pals =Palindromes(str)
    palsc=[1]*len(pals)
    palsp=[1]*len(str)
    pres=dict()
    pstart=0
    for i in range(len(str)):
        pres[str[i]]=1
        i20=2*i
        i21=2*i+2
        pstartn=None
        for j in range(pstart,i21+1):
            p=pals[j]
            if p>i21-j:
                p=i21-j
                if pstartn==None:
                    pstartn=j

            if p<2: continue
            while p>palsc[j]:
                s=(j-p)//2
                e=s+p
                for es in range(s+1+palsp[s],e+1):
                    pres[str[s:es]]=1
                if p>palsp[s]:
                    palsp[s]=p
                p-=2
            palsc[j]=p
        if pstartn!=None:
            pstart=pstartn
        else:
            pstart=i21
        print(len(pres))


N = int(input().strip())
S = input().strip()
#S="bccbbbbc"
solve(S)


