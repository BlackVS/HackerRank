#include <bits/stdc++.h>

using namespace std;

const int maxn = 300005,mo[2] = {int(1e9) + 7,998244353};

typedef unsigned long long ull;

struct treap_node
{
	ull rk;
	int l,r,v,siz;
}T[maxn];

struct palin_node
{
	int go[26],fail,len,fa;
}Pt[maxn];

char s[maxn];
ull ans;
int sav[maxn][2];
int has[2][maxn],pw[2][maxn],cnt,root,n;

void palin_prepare()
{
	cnt = 1;
	Pt[1].len = -1;
	Pt[1].fail = Pt[0].fail = 1;
	T[0].rk = 0,T[1].rk = -1;
}

int get(int p,int x)
{
	for(;s[n - Pt[p].len - 1] - 'a' != x;p = Pt[p].fail);
	return p;
}

bool append(int &lst,int x)
{
	int p = lst;
	++ n;
	p = get(p,x);
	if (!Pt[p].go[x])
	{
		int cur = ++ cnt;
		Pt[cur].fa = p;
		Pt[cur].len = Pt[p].len + 2;
		Pt[cur].fail = Pt[get(Pt[p].fail,x)].go[x];
		Pt[p].go[x] = cur;
		lst = cur;
		return 1;
	}
	lst = Pt[p].go[x];
	return 0;
}

void relabel(int p,ull l,ull r)
{
	if (!p) return;
	ull mid = (l + r) >> 1;
	T[p].rk = mid;
	relabel(T[p].l,l,mid),relabel(T[p].r,mid,r);
}

inline bool same(int l,int r,int x,int y)
{
	if (r - l != y - x) return 0;
	if (l > r) return 1;
	for(int i = 0;i < 2;i ++)
	{
		int a = (has[i][r] - has[i][l - 1] * 1ll * pw[i][r - l + 1]) % mo[i];
		int b = (has[i][y] - has[i][x - 1] * 1ll * pw[i][y - x + 1]) % mo[i];
		if ((a < 0 ? a + mo[i] : a) != (b < 0 ? b + mo[i] : b)) return 0;
	}
	return 1;
}

bool small(int a,int b)
{
	if (Pt[a].len < Pt[b].len) return !small(b,a);
	int la = sav[a][0],ra = sav[a][1],lb = sav[b][0],rb = sav[b][1];
	if (s[la] != s[lb]) return s[la] < s[lb];
	if (same(la + 1,la + rb - lb - 1,lb + 1,rb - 1)) return s[la + rb - lb] < s[rb];
	return T[Pt[a].fa].rk < T[Pt[b].fa].rk;
}

void update(int x)
{
	if (!x) return;
	T[x].siz = T[T[x].l].siz + T[T[x].r].siz + 1;
}

int insert(int &u,ull l,ull r)
{
	int tmp = 0;
	ull mid = (l + r) >> 1;
	if (!u) u = cnt,T[cnt].rk = mid; else
	{
		if (small(cnt,u))
		{
			tmp = insert(T[u].l,l,mid);
			if (T[T[u].l].v > T[u].v)
			{
				int p = T[u].l;
				T[u].l = T[p].r,T[p].r = u;
				update(u);
				u = p;
				relabel(p,l,r);
			}
		} else
		{
			tmp = T[T[u].l].siz + 1 + insert(T[u].r,mid,r);
			if (T[T[u].r].v > T[u].v)
			{
				int p = T[u].r;
				T[u].r = T[p].l,T[p].l = u;
				update(u);
				u = p;
				relabel(p,l,r);
			}
		}
	}
	update(u);
	return tmp;
}

int get_od(int s,int p)
{
	if (!s || s > T[p].siz) return 0;
	if (s <= T[T[p].l].siz) return get_od(s,T[p].l);
	if (s == T[T[p].l].siz + 1) return p;
	return get_od(s - T[T[p].l].siz - 1,T[p].r);
}

int get_lcp(int l,int r,int x,int y)
{
	int ll = 1,rr = min(r - l + 1,y - x + 1),tmp = 0;
	for(int mid;ll <= rr;)
	{
		mid = (ll + rr) >> 1;
		if (same(l,l + mid - 1,x,x + mid - 1)) tmp = mid,ll = mid + 1; else rr = mid - 1;
	}
	return tmp;
}

int get_lcp(int a,int b)
{
	return get_lcp(sav[a][0],sav[a][1],sav[b][0],sav[b][1]);
}

void push(int l,int r)
{
	ans += r - l + 1;
	sav[cnt][0] = l,sav[cnt][1] = r;
	T[cnt].v = rand();
	int s = insert(root,0,1ll << 60) + 1;
	int lx = get_od(s - 1,root),rx = get_od(s + 1,root);
	if (lx && rx) ans += get_lcp(lx,rx);
	if (lx) ans -= get_lcp(lx,cnt);
	if (rx) ans -= get_lcp(cnt,rx);
}

int main()
{
	int type = 0;
	scanf("%d", &type);
	scanf("%d", &n);
	scanf("%s", s + 1);
	for(int i = 0;i < 2;i ++) pw[i][0] = 1;
	palin_prepare();
	n = 0;
	for(int i = 1,lst = 0,l = strlen(s + 1);i <= l;i ++)
	{
		for(int j = 0;j < 2;j ++)
		{
			pw[j][i] = pw[j][i - 1] * 37ll % mo[j];
			has[j][i] = (has[j][i - 1] * 37ll + s[i] - 'a') % mo[j];
		}
		if (append(lst,s[i] - 'a')) push(i - Pt[lst].len + 1,i);
		printf("%llu\n", ans); 
	}
	return 0;
}